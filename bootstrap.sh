#!/bin/sh
set -e

PROMPT=$(basename $0 .sh)
NAME=$(basename $0)

COMMAND=$1
if [ -z "$COMMAND" ]; then
  COMMAND=help
fi

if [ -z "$NODE_ENV" ]; then
  NODE_ENV=development
fi

# ./bootstrap.sh config
_bootstrap_config() {
  if [ ! -e config/${NODE_ENV}.yaml ]; then
    echo "$PROMPT: copying config/default.yaml config/${NODE_ENV}.yaml"
    cp config/default.yaml config/${NODE_ENV}.yaml
  else
    echo "$PROMPT: [skip] copying config/default.yaml config/${NODE_ENV}.yaml"
  fi
}

# ./bootstrap.sh dotenv
_bootstrap_dotenv() {
  if [ ! -e .env ]; then
    echo "$PROMPT: copying .env.dist .env"
    cp .env.dist .env
  else
    echo "$PROMPT: [skip] copying .env.dist .env"
  fi
}

# ./bootstrap.sh npm
_bootstrap_npm() {
  if [ ! -e .npmrc ]; then
    echo "$PROMPT: creating .npmrc"
    cat <<'NPMRC' > .npmrc
@cortext:registry=https://lib.cortext.net
//lib.cortext.net/:_authToken=${NPM_TOKEN}
NPMRC
    if [ -n "$NPM_TOKEN" ]; then
      echo "$PROMPT: npm install"
      npm install
    else
      echo "$PROMPT: [skip] npm install"
    fi
    echo "$PROMPT: removing .npmrc"
    rm -f .npmrc
  else
    echo "$PROMPT: [skip] creating .npmrc"
    if [ -n "$NPM_TOKEN" ]; then
      echo "$PROMPT: npm install"
      npm install
    else
      echo "$PROMPT: [skip] npm install"
    fi
  fi
}

# ./bootstrap.sh all
_bootstrap_all() {
  _bootstrap_config
  _bootstrap_dotenv
  _bootstrap_npm
}

# ./bootstrap.sh help (default)
_bootstrap_help() {
  echo "$PROMPT: $NAME <COMMAND>"
  echo "$PROMPT:"
  echo "$PROMPT: COMMANDS:"
  echo "$PROMPT:"
  echo "$PROMPT: - config   create config/development.yaml (or test.yaml) etc"
  echo "$PROMPT: - dotenv   copy .env.dist to .env"
  echo "$PROMPT: - npm      create .npmrc file and run npm install"
  echo "$PROMPT: - all      run all available commands, except help"
  echo "$PROMPT: - help     this message about how to run $NAME"
}

_bootstrap_$COMMAND
