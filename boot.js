'use strict';

console.log('I: RISOR boot script started.');

// reset all jobs on RIBA as state = finished
// see: - https://gitlab.com/risis/rcf/risis-rcf-risor/-/issues/143
//      - https://gitlab.com/risis/rcf/risis-rcf-riba/-/issues/117
const riba = require('./utils/riba');
riba.resetAllJobs({ error: 'risor restarted' });
