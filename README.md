# risis-rcf-risor

RISIS Services Orchestrator (RISOR).

## Requirements

ExpressJS and NodeJS, for details about all NPM modules and its versions see
package.json file.

## RISOR settings .env file

RISOR uses [dotenv][] to load `.env` file into environment variables, `dotenv`
not overrides existing variables, that allow preserving environment variables
defined in docker or docker-compose context, as those environment variables
takes precedence over variables defined in `.env` file.

## Setup development environment

### Step 1. Create config file

    ./bootstrap.sh config

* Edit the `config/development.yaml` file:
  * Add the API Token for the Cortext and the Dataverse providers
  * And also add settings for Minio Storage in the same file

### Step 2. Configure and install NPM packages

    export NPM_TOKEN=<PASTE GITLAB NPM TOKEN HERE>
    ./bootstrap.sh npm

### Step 3. Run!

    npm run live

Runing tests:

    npm test

**Alert!** It is highly recommended to run Minio and Dataverse local when
running tests, see **Tips and Tricks** section for instructions.

Test to check conformance between OpenAPI spec and API backend:

    npm run dredd

To list [Dredd transaction names][dredd-transaction-names] run:

    npm run dredd:names

Running eslint to check coding style:

    npm run eslint .

If you want to run all tests including dredd and eslint:

    npm run test:all

See below how to run with docker or docker-compose.

### Running with docker

Build image:

    export NPM_TOKEN=put-here-the-npm-token-to-access-cortext-private-npm-registry
    docker build --build-arg NPM_TOKEN=$NPM_TOKEN -t rcf-risor .

Run container:

    docker run -p 7777:7777 rcf-risor

Run tests inside docker:

    docker run rcf-risor npm run test:all

### Running with docker-compose

Build image:

    export NPM_TOKEN=put-here-the-npm-token-to-access-cortext-private-npm-registry
    docker-compose build

Run container:

    docker-compose up

Run tests under docker-compose:

    docker-compose exec -e NODE_ENV=test rcf-risor npm run test

If you need to run a single testfile run as:

    docker-compose exec -e NODE_ENV=test rcf-risor npm run test <file>

Example running the file `test/scenarios.test.js`:

    docker-compose exec -e NODE_ENV=test rcf-risor npm run test test/scenarios.test.js

To run tests in case of the container non-started by `docker-compose up` run:

    docker-compose run -e NODE_ENV=test rcf-risor npm run test

## Releasing a new RISOR version

* Go to main branch, merge dev branch into main
* Make sure the new version is declared in package.json and package-lock.json file
  * See the [Semantic Versioning](https://semver.org) specification on how to increment version numbers
* Make sure all automated tests are ok
* Push main branch to Gitlab
* Create a git tag for the new version and push to Gitlab
* Merge main branch back to dev branch and push
* Create a new Release on Gitlab from the new tag
  * https://gitlab.com/risis/rcf/risis-rcf-risor/-/releases

Gitlab CI/CD will push docker images tagging it with the new git tag pushed to
the repo.

## Publishing docker images

Docker images are being built automatically on every commit by Gitlab CI/CD
pipeline, check the [.gitlab-ci.yaml](.gitlab-ci.yaml) file for details, published
images on Gitlab is tagged with:

* commit short sha
* branch name OR tag name
* latest

## Setup production environment

Everything specific-related to production deployment is managed into
[risis-rcf-stack][] repository in `orchestration` folder, follow instructions
on link below on how to deploy on production.

* See https://gitlab.com/risis/rcf/risis-rcf-stack/-/blob/master/orchestration/README.md

## Coding style guide and naming conventions

Please use the following style and conventions on the risor project, and also
in any other RCF's project.

### JavaScript style guide hints

- always use semicolons
- single-quotes (') are preferred to double-quotes (")
- Use string concatenation for multiline string literals
- Use const for references
- Use let if you must reassign references
- BE CONSISTENT

### Naming conventions

- constant values, use CONSTANT_VALUES_LIKE_THIS
- functions, use functionNamesLikeThis
- variables, use variableNamesLikeThis
- classes, use ClassNamesLikeThis
- enums, use EnumNamesLikeThis
- methods, use methodNamesLikeThis
- scopes, use foo.namespaceNamesLikeThis.bar
- filenames, use filenameslikethis.js

## Tips and Tricks

### Opening shell into a running container

    docker exec -it $(docker ps -q -f ancestor=rcf-risor) sh

### Running Min.io storage for local development/testing

**Alert:** It's better to run minio and dataverse by running test environment on
[risis-rcf-stack/datastore/dataverse5](https://gitlab.com/risis/rcf/risis-rcf-stack/-/tree/master/datastore/dataverse5).

But if you need to run by-hand see the instructions below:

    docker run -p 9000:9000 \
      -e MINIO_ACCESS_KEY=minioadmin \
      -e MINIO_SECRET_KEY=minioadmin \
      -v ${PWD}/data/minio:/data minio/minio server /data

See http://localhost:9000
* access key = minioadmin
* secret key = minioadmin

### Running Dataverse datastore for local development/testing

**Alert:** It's better to run minio and dataverse by running test environment on
[risis-rcf-stack/datastore/dataverse5](https://gitlab.com/risis/rcf/risis-rcf-stack/-/tree/master/datastore/dataverse5).

But if you need to run by-hand see the instructions below:

    git clone https://github.com/IQSS/dataverse-docker.git
    cd dataverse-docker/
    export traefikhost=localhost
    docker network create traefik
    docker-compose -f docker-compose-local.yml up

See http://dataverse-dev.localhost
* admin user = dataverseAdmin
* password = admin

To see Dataverse admin token run:

    docker-compose -f docker-compose-local.yml exec postgres psql -t dvndb dvnuser -c "select tokenstring from apitoken where authenticateduser_id = (select id from authenticateduser where useridentifier = 'dataverseAdmin');"

### Generate user access_token

    curl -s --location --request POST 'https://auth.rcf/auth/realms/master/protocol/openid-connect/token' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'username=admin' --data-urlencode 'password=admin' --data-urlencode 'grant_type=password' --data-urlencode 'client_secret=d0b8122f-8dfb-46b7-b68a-f5cc4e25d077' --data-urlencode 'client_id=RISOR' | jq .access_token

### Upload scenarios to local storage m4.risis.localhost

Install minio client or download binary from https://min.io/download#/linux

    mc alias set rcf-local https://m4.risis.localhost minioadmin minioadmin
    mc mb rcf-local/scenarios/dev
    mc cp --recursive scenarios/models/ rcf-local/scenarios/dev

**ALERT** Don't forget the slash **/** at the end of **scenarios/models/**.

# License

- Copyright (c) 2019 Philippe Breucker
- Copyright (c) 2020-2023 Université Gustave Eiffel
- Copyright (c) 2020-2023 INRAE

Licensed under the EUPL.

The full text of the EUPL licence can be found at
https://opensource.org/licenses/EUPL-1.2.

[dotenv]: https://www.npmjs.com/package/dotenv
[risis-rcf-stack]: https://gitlab.com/risis/rcf/risis-rcf-stack
[dredd-transaction-names]: https://dredd.org/en/latest/hooks/index.html#transaction-names
[cortext-team]: https://www.cortext.net/members
