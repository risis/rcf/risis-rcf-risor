'use strict';

const fs = require('fs-extra');
const path = require('path');
const Minio = require('minio');
const log4js = require('log4js');
const errors = require('../../utils/errors');
const archiver = require('archiver');
const walk = require('walkdir');
const Url = require('url-parse');
const logger = log4js.getLogger('default');
const urljoin = require('url-join');

const config = global.config;

const minioClient = new Minio.Client({
  endPoint: config.providers.storage.uri.hostname,
  port: parseInt(config.providers.storage.uri.port),
  useSSL: config.providers.storage.uri.ssl,
  accessKey: config.providers.storage.keys.access,
  secretKey: config.providers.storage.keys.secret
});

// In case we want to work with disk instead of memory (large folder downloads)
function init () {
  try {
    fs.ensureDirSync(config.tmpRoot);
    logger.info('tmp path ' + config.tmpRoot + ' exists');
  } catch (e) {
    throw new errors.RuntimeError('Error ensuring tmp path ' + config.tmpRoot, e);
  }
  try {
    fs.emptyDirSync(config.tmpRoot);
    logger.info('tmp path ' + config.tmpRoot + ' emptied');
  } catch (e) {
    throw new errors.RuntimeError('Error emptying tmp path ' + config.tmpRoot, e);
  }
}

function getPathAsZip (bucket, minioPath, objectName) {
  logger.info(`get object ${objectName} on minio path ${minioPath} inside bucket ${bucket}`);
  return new Promise((resolve, reject) => {
    const infoStream = minioClient.listObjects(
      bucket,
      minioPath,
      true
    );
    const archive = archiver('zip');
    archive.on('warning', (e) => {
      reject(new errors.RuntimeError('Archiver warning ', e));
    });
    archive.on('error', (e) => {
      reject(new errors.RuntimeError('Archiver error ', e));
    });
    const objects = [];
    infoStream.on('data', (obj) => {
      objects.push({
        stream: minioClient.getObject(bucket, obj.name),
        fileName: obj.name.substring(obj.name.indexOf(objectName)) // remove 'dev/' or 'prod/' from filenames
      });
    });
    infoStream.on('error', (e) => {
      reject(new errors.RuntimeError('Error getting scenario files metadata', e));
    });
    infoStream.on('end', async () => {
      logger.debug('objects to zip');
      logger.debug(objects.map((obj) => obj.fileName));
      for (const object of objects) {
        archive.append(await object.stream, { name: object.fileName });
      }
      archive.finalize();
      resolve(archive);
    });
  });
}

init();

module.exports = {
  getPathAsZip,
  uploadFiles: async (dir, uri) => {
    const url = new Url(uri);
    const bucket = url.hostname;
    const minioPath = url.pathname.substring(1);
    logger.info(`uploading dir '${path.relative(config.appRoot, dir)}' to min.io bucket '${bucket}'`);
    const exists = await minioClient.bucketExists(bucket);
    if (!exists) {
      try {
        await minioClient.makeBucket(bucket);
        logger.info(`created min.io bucket '${bucket}'`);
      } catch (err) {
        logger.error(err.stack);
        throw new Error(err.message);
      }
    }
    const dirWalk = await walk.async(dir, { return_object: true });
    const filePaths = Object.keys(dirWalk).filter(key => fs.statSync(key).isFile());
    return Promise.all(filePaths.map((fullFilename) => {
      const relativeFilename = path.relative(dir, fullFilename);
      return minioClient.fPutObject(bucket, urljoin(minioPath, relativeFilename), fullFilename);
    }));
  },
  uploadFile: async (file, uri) => {
    const url = new Url(uri);
    const bucket = url.hostname;
    const minioPath = url.pathname.substring(1);
    logger.info(`uploading file '${path.relative(config.appRoot, file)}' to min.io bucket '${bucket}' at path '${minioPath}'`);
    const exists = await minioClient.bucketExists(bucket);
    if (!exists) {
      try {
        await minioClient.makeBucket(bucket);
        logger.info(`created min.io bucket '${bucket}'`);
      } catch (err) {
        logger.error(err.stack);
        throw new Error(err.message);
      }
    }
    return await minioClient.fPutObject(bucket, minioPath, file);
  },
  workspaceUriToPublicUri: (workspaceUri) => {
    // workspaceUri has the following format:
    // m4://users-workspace/fc8cae85-c0d2-4c62-b383-dc11fec39cf8/projects/LnLGLG6nXNT/scenarios/report-generator/RfscLUOFcN5
    const w = workspaceUri.match(/\/\/(?<bucket>[^/]+)\/(?<workspace>[^/]+)\/projects\/(?<project>[^/]+)\/scenarios\/(?<scenario>[^/]+)\/(?<job>[^/]+)/);
    return urljoin(config.providers.storage.public_uri, w.groups.workspace, w.groups.project, w.groups.scenario, w.groups.job);
  }
};
