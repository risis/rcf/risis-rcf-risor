'use strict';

const { datastore: datastoreConfig } = global.config.providers;

const RcfDatastoreClient = require('@cortext/rcf-datastore-client');
const url = require('url');
const fs = require('fs-extra');
const path = require('path');
const log4js = require('log4js');
const logger = log4js.getLogger('default');
const adminDatastoreClient = new RcfDatastoreClient(url.format(datastoreConfig.uri), datastoreConfig.token, datastoreConfig.blocked_api_key);

module.exports = {
  createUserClient: (datastoreApiToken) => {
    logger.debug('initializing RcfDatastoreClient with token %o', datastoreApiToken);
    const datastoreApiUrl = url.format(datastoreConfig.uri);
    return new RcfDatastoreClient(datastoreApiUrl, datastoreApiToken);
  },
  searchDatasets: (req, searchOptions) => {
    const { context: { datastoreClient: client } } = req;
    return client.searchDatasets(searchOptions);
  },
  createDataset: (req, dataverseId, datasetInformation) => {
    const { context: { datastoreClient: client } } = req;
    return client.createDataset(dataverseId, datasetInformation);
  },
  deleteDataset: (req, datasetId) => {
    const { context: { datastoreClient: client } } = req;
    return client.deleteDataset(datasetId);
  },
  publishDataset: (req, datasetId) => {
    const { context: { datastoreClient: client } } = req;
    return client.publishDataset(datasetId);
  },
  replaceDataset: (req, datasetId, datasetInformation) => {
    const { context: { datastoreClient: client } } = req;
    return client.updateDataset(datasetId, datasetInformation);
  },
  editDataset: (req, datasetId, field, value) => {
    const { context: { datastoreClient: client } } = req;
    return client.editDataset(datasetId, field, value);
  },
  uploadFile: async (req, datasetId, filepath, filename) => {
    const { context: { datastoreClient: client } } = req;
    // FIXME the loop below is a HOTFIX for the demo presentation on 10 sept 2021 on LISIS lab and must be refactored ASAP
    const resDatasetInfo = await client.getDatasetInformation(datasetId);
    logger.debug('dataset id = %o', resDatasetInfo.data.data.id);
    let datasetLocks = 'unknown';
    while (datasetLocks !== 'free') {
      const res = await client.locks(resDatasetInfo.data.data.id);
      const resJson = await res.json();
      console.debug('res_json = %o', JSON.stringify(resJson));
      if (resJson.data.length === 0) {
        datasetLocks = 'free';
      } else {
        datasetLocks = 'lock';
      }
    }
    return client.uploadFile(datasetId, filepath, filename);
  },
  deleteFile: (req, fileId) => {
    const { context: { datastoreClient: client } } = req;
    return client.deleteFile(fileId);
  },
  replaceFile: (req, fileId, filepath, filename) => {
    const { context: { datastoreClient: client } } = req;
    return client.replaceFile(fileId, filepath, filename);
  },
  downloadFile: (req, fileId) => {
    const { context: { datastoreClient: client } } = req;
    return client.downloadFile(fileId);
  },
  downloadDatasetFiles: (req, datasetId) => {
    const { context: { datastoreClient: client } } = req;
    return client.downloadDatasetFiles(datasetId);
  },
  getDatasetInformation: (req, datasetId) => {
    const { context: { datastoreClient: client } } = req;
    return client.getDatasetInformation(datasetId);
  },
  listDatasets: (req, dataverseId) => {
    const { context: { datastoreClient: client } } = req;
    return client.listDatasets(dataverseId);
  },
  getDatasetsInformation: (req, ids) => {
    const { context: { datastoreClient: client } } = req;
    return client.getDatasetsInformation(ids);
  },
  downloadFiles: (req, ids) => {
    const { context: { datastoreClient: client } } = req;
    return client.downloadFiles(ids);
  },
  importFiles: async (req, datasetId, outputFolder) => {
    const { context: { datastoreClient: client } } = req;
    const datasetInformation = await client.getDatasetInformation(datasetId);
    const files = datasetInformation.data.data.latestVersion.files;
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      logger.info(`import file ${file.dataFile.filename}`);
      const result = await client.downloadFile(file.dataFile.id);
      const data = result.data.toString();
      fs.writeFileSync(path.join(outputFolder, file.dataFile.filename), data);
    }
    return datasetInformation;
  },
  importFile: async (req, datasetId, fileId, outputFolder) => {
    const { context: { datastoreClient: client } } = req;
    const result = await client.downloadFile(fileId);
    const metadata = await client.getFileMetadata(datasetId, fileId);
    fs.writeFileSync(path.join(outputFolder, metadata.data.label), result.data);
    return metadata;
  },
  assignRole: async (req, email, role, datasetId) => {
    const { context: { datastoreClient: client } } = req;
    logger.info(`grant role '${role}' to user '${email}' on dataset '${datasetId}'`);
    // return client.assignUserRole(email, role, datasetId);
    let listDatasetRole = await adminDatastoreClient.listDatasetRoleAssignments(datasetId);
    listDatasetRole = await listDatasetRole.json();
    const resUser = await adminDatastoreClient.listUsers(email);
    logger.debug('response', resUser);
    const jsonUser = await resUser.json();
    if (jsonUser.data.users.length > 0) {
      const user = jsonUser.data.users[0];
      const roleAlreadyAssigned = listDatasetRole.data.find(roleAssigned => {
        return roleAssigned._roleAlias === role && roleAssigned.assignee === `@${user.userIdentifier}`;
      });
      if (!roleAlreadyAssigned) {
        const roleObject = {
          assignee: `@${user.userIdentifier}`,
          role
        };
        return client.assignDatasetRole(datasetId, roleObject);
      }
      return true;
    } else {
      throw new Error(`no user found with email '${email}'`);
    }
  }
};
