'use strict';

const request = require('request');
const url = require('url');

const config = global.config;

const cortextApiUrl = url.format(config.providers.cortext.uri) || process.env.CORTEXT_API_URL || '';
const cortextApiKey = config.providers.cortext.token || process.env.CORTEXT_API_KEY || '';

const cortext = {
  handleRequest: (req, res, callback) => {
    const { context: { logger } } = req;
    const resourceId = req.params.id || '';
    const options = {
      url: cortextApiUrl + '/' + req.params.resource + '/' + resourceId,
      headers: {
        Authorization: 'Bearer ' + cortextApiKey
      },
      method: req.method.toLowerCase()
    };
    logger.info('proxying request to cortext api');
    logger.debug('options: %o', options);
    logger.debug('request headers: %o, params: %o, query: %o', req.headers, req.params, req.query);
    req.pipe(request(options)).pipe(res);
  },
  FilesUrlResults: (req, res) => {
    const { context: { logger } } = req;
    const options = {
      url: cortextApiUrl + `/documents/${req.params.hash}`,
      headers: {
        Authorization: 'Bearer ' + cortextApiKey,
        'User-Agent': 'request'
      },
      method: 'GET'
    };
    request(options, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        const documents = JSON.parse(body);

        const newObj = {};
        let folder = '';
        Object.entries(documents.files).forEach(([key, obj]) => {
          // Builds telling key names to be used as the task outputs
          let keyName = obj.filename.slice(1).replace(/[/ ._-]/g, '_');

          if (req.params.script_name === 'network_mapping') {
            folder = obj.filename.split('/')[1];
            const networkMappingFolders = ['maps', 'mapexplorer'];
            if (networkMappingFolders.includes(folder)) {
              keyName = folder + '_' + obj.extension;
            }
          } else if (req.params.script_name === 'terms_extraction') {
            folder = obj.filename.split('/')[1];
            const termsExtractionFolders = ['lexical_analysis', 'indexed_list'];
            if (!termsExtractionFolders.includes(folder)) {
              keyName = 'extracted_terms_' + obj.extension;
            }
          } else if (req.params.script_name === 'contingency_matrix') {
            keyName = 'contingency_matrix_' + obj.extension;
          }
          // Builds a url according to the kind of information the user needs
          let url = '';
          switch (obj.extension) {
            case 'csv':
            case 'json':
            case 'tsv':
            case 'zip':
              url = `https://assets.cortext.net/docs/${obj.id}`;
              break;
            case 'geocortext':
              url = `https://geomapping.cortext.net/#/map/rsml-${documents.hash}`;
              break;
            case 'pdf':
              url = `https://assets.cortext.net/docs/${obj.id}/view`;
              break;
            case 'svg':
              url = `https://labeleditor.cortext.net/?svg=https%3A%2F%2Fassets.cortext.net%2Fdocs%2F${obj.id}`;
              break;
            case 'gexf':
              url = `https://retina.cortext.net/#/graph/?r=d&url=https://assets.cortext.net/docs/${obj.id}`;
              break;
            case 'html':
              url = `https://assets.cortext.net/docs/${obj.id}/view`;
              break;
          }
          newObj[keyName] = url;
        });
        const newObjJson = { files: newObj };
        res.status(200).json(newObjJson);
      } else {
        logger.error(error);
      }
    });
  }
};

// module exports
module.exports = cortext;
