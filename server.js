'use strict';

const app = require('./app');
const port = global.config.node_port;

const server = app.listen(port, () => {
  console.info(`I: Server on ${app.locals.settings.env} mode started on port ${port}`);
  require('./boot');
});

// disable socket timeouts (nodejs default = 2 min)
server.setTimeout(0);
