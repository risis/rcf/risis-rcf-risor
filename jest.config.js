const config = require('./config');

const jestConfig = {
  testEnvironment: 'node',
  globals: {
    config
  },
  moduleNameMapper: {
    '^csv-parse/sync': '<rootDir>/node_modules/csv-parse/dist/cjs/sync.cjs'
  }
};

module.exports = jestConfig;
