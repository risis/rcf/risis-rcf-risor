'use strict';

module.exports = function (datastoreController) {
  return async function (req, res, next) {
    const { context: { logger } } = req;
    const datastoreToken = req.query.service_provider_key;
    if (datastoreToken) {
      logger.debug('datastore user token = %o', datastoreToken);
      req.context.datastoreClient = datastoreController.datastoreService.createUserClient(datastoreToken);
      next();
    } else {
      logger.warn("datastore token missing, query param 'service_provider_key' is null");
      res.status(400).send({ error: "datastore token missing, please provide a valid token on query param 'service_provider_key'" });
    }
  };
};
