'use strict';

const jwt = require('jsonwebtoken');
const keycloak = require('../utils/keycloak');

module.exports = async function (req, res, next) {
  const { context: { logger } } = req;
  const publicKey = await keycloak.getPublicKey();
  logger.debug(publicKey);
  if (!req.token) {
    res.sendStatus(401);
  } else {
    try {
      const tokenDecoded = jwt.verify(req.token, publicKey, { algorithms: ['RS256'] });
      req.tokenDecoded = tokenDecoded;
      next();
    } catch (err) {
      logger.warn(err);
      res.sendStatus(401);
    }
  }
};
