'use strict';

function requestLogger (req, res, next) {
  const { method, url, context: { logger } } = req;

  res.on('finish', () => {
    // Log response status, request url and method
    logger.info(res.statusCode, method, url);
  });

  next();
}

module.exports = requestLogger;
