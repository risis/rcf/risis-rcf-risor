'use strict';

const log4js = require('log4js');
const { v4: uuid } = require('uuid');

function contextHandler (req, res, next) {
  const context = {};
  // Check if request comes with an UUID
  if (req.header('X-Request-Id')) {
    context.traceid = req.header('X-Request-Id');
  } else {
    context.traceid = uuid();
  }
  // Add default logger instance to context
  const defaultLogger = log4js.getLogger('default');
  defaultLogger.addContext('traceid', context.traceid);
  context.logger = defaultLogger;

  // Add uuid as response header
  res.set('X-Request-Id', context.traceid);

  req.context = context;
  next();
}

module.exports = contextHandler;
