'use strict';

const openApiValidator = require('express-openapi-validator');
const path = require('path');
const _ = require('lodash');
const config = global.config;
const authorizationConfig = config.authorization;

// regex with named groups. see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Groups_and_Ranges
const scopeTemplateRegex = /{(?<paramname>\w+)}/mg;

function renderAuthorizationScope (scopeTemplate, replaceDict) {
  // don't change parameters. make copy of string
  let scopeCopy = scopeTemplate.slice();
  // Look for templates to replace by path args
  // match stuff that comes inside brackets {likethis}
  let match;
  const matches = [];
  while ((match = scopeTemplateRegex.exec(scopeTemplate)) != null) {
    // check if there is a path param for the thing in brackets
    if (replaceDict[match.groups.paramname]) {
      matches.push(match);
    }
  }
  // replace each 'replaceable' template tag with value from route
  matches.forEach((match) => {
    const key = match.groups.paramname; // {something}
    const inScope = match[0]; // something
    scopeCopy = scopeCopy.replace(inScope, replaceDict[key]);
  });
  return scopeCopy;
}

// This function is very coupled with keycloak / other auth provider jwt formatting
// The idea is to give a more semantically meaningful object to risor
function parseUserAuthorizations (userToken) {
  const result = {};
  if (userToken.realm_access && userToken.realm_access.roles) {
    result.global = userToken.realm_access.roles;
  }
  if (userToken.resource_access && userToken.resource_access.RISOR && userToken.resource_access.RISOR.roles) {
    userToken.resource_access.RISOR.roles.forEach(scope => {
      // This comes from our convention RISOR:resourceGroup:resource:permission
      const [, resourceGroup, resource, permission] = scope.split(':');
      if (!result[resourceGroup]) {
        result[resourceGroup] = [];
      }
      if (!result[resourceGroup][resource]) {
        result[resourceGroup][resource] = [];
      }
      result[resourceGroup][resource].push(permission);
    });
  }
  return result;
}

function securityBearerAuthHandler (req, scopes) {
  const { context: { logger } } = req;
  logger.debug('openapi-validator security handler = BearerAuth');
  const tokenDecoded = req.tokenDecoded;
  const pathParams = req.openapi.pathParams;

  // If the endpoint has scopes, check the authorization
  if (scopes.length > 0) {
    // Custom authorization logic.
    // Check the scopes declared in the openapi
    const scopesMap = scopes.reduce(
      (scopeMap, current) => {
        // render scope template with path param replacements
        const renderedScope = renderAuthorizationScope(current, pathParams);
        // by default, group key is realm_access
        let groupKey = 'realm_access';
        // look for hierarchies (separated by :)
        // the convention we chose for scopes is CLIENT:resource_group:resource:action
        // keycloak sends the client roles as part of the attribute 'resource_access'
        const tokens = renderedScope.split(':');
        if (tokens.length > 1) {
          // check if resource is protected
          const [, resourceGroup, resource] = tokens;
          if (!authorizationConfig[resourceGroup] ||
              !authorizationConfig[resourceGroup].protected.includes(resource)) {
            return scopeMap;
          }
          // make attribute key for get nested entry with resource_access.CLIENT
          groupKey = 'resource_access.' + tokens[0].toUpperCase(); // Fix later
        }
        if (scopeMap[groupKey] !== undefined) {
          scopeMap[groupKey].push(renderedScope);
        } else {
          scopeMap[groupKey] = [renderedScope];
        }
        return scopeMap;
      },
      {}
    );

    const authorized = Object.entries(scopesMap).every(([groupKey, requiredScopes]) => {
      // get nested entry with lodash _.get
      const userScopes = _.get(tokenDecoded, groupKey);
      if (userScopes) {
        const requiredScopesLower = requiredScopes.map(s => s.toLowerCase());
        const userScopesLower = userScopes.roles.map(s => s.toLowerCase());
        return requiredScopesLower.some(scope => userScopesLower.includes(scope));
      } else {
        return false;
      }
    });

    if (authorized) {
      req.context.userAuthorizations = parseUserAuthorizations(tokenDecoded);
      return true;
    }
    throw new Error('Forbidden');
  }

  // If the endpoint has no scopes, forbid by default?
  throw new Error('Forbidden');
}

module.exports = openApiValidator.middleware({
  apiSpec: path.join(config.appRoot, 'openapi.yaml'),
  fileUploader: {
    dest: config.tmpRoot
  },
  ignorePaths: /\/+providers\/+(?:cortext|riba|gatecloud\/.+|patents-explorer|knowmak|d4science|eupro|risis-datasets\/.+|builtins\/test\/)/,
  unknownFormats: ['slug', 'semver', 'spec', 'doi', 'uri'],
  validateRequests: true,
  validateResponses: true,
  validateSecurity: {
    handlers: {
      BearerAuth: securityBearerAuthHandler
    }
  }
});
