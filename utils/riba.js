'use strict';

const log4js = require('log4js');
const unirest = require('unirest');
const logger = log4js.getLogger('default');
const url = require('url');
const keycloak = require('./keycloak');
const urljoin = require('url-join');

const config = global.config;
const ribaUrl = url.format(config.providers.riba.uri);

async function resetAllJobs (response) {
  const updateAllJobsUrl = urljoin(ribaUrl, '/jobs/reset');
  const body = { response };
  try {
    const token = await keycloak.getToken();
    logger.debug('access-token = ', token);
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    };
    logger.info('resetting riba jobs, riba url = %o', updateAllJobsUrl);
    logger.debug('body payload sent on updating state', JSON.stringify(body));
    const updateResponse = await unirest.post(updateAllJobsUrl).headers(headers).send(body);
    if (updateResponse.status === 200) {
      logger.debug('success resetting riba jobs');
    } else {
      logger.error('error resetting riba jobs, body = %o, status = %o', updateResponse.body, updateResponse.status);
    }
  } catch (error) {
    logger.error('error resetting riba jobs');
    logger.error(error);
  }
}

module.exports = {
  resetAllJobs
};
