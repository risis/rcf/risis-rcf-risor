const util = require('util');

const { logging } = global.config;

const loggingConfig = {
  appenders: {
    stdout: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        // pattern: "%d level=%p %x{traceid}%x{message} at=%f{3}.%l%x{context}%n",
        pattern: '%d level=%p %x{traceid}%x{flatten} at=%f{3}.%l%n',
        tokens: {
          traceid: ({ context }) => context.traceid ? 'traceid=' + context.traceid + ' ' : '',
          flatten: ({ data }) => util.format.apply(this, data).replace(/\n/g, ' ')
        }
      }
    },
    stderr: {
      type: 'stderr',
      layout: {
        type: 'coloured'
      }
    },
    errors: {
      type: 'logLevelFilter',
      level: 'ERROR',
      appender: 'stderr'
    }
  },
  categories: {
    default: {
      appenders: [
        'stdout',
        'errors'
      ],
      level: logging.level,
      enableCallStack: true
    }
  }
};

module.exports = {
  config: loggingConfig
};
