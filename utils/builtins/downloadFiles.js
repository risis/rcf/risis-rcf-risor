const fetch = require('node-fetch');
const fs = require('fs');
const path = require('path');
const faker = require('faker');

const config = global.config;

async function downloadFileToTemp (url) {
  const fileName = faker.random.alphaNumeric(10);
  const urlPath = new URL(url).pathname;

  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Failed to download file from ${url}`);
  }

  const contentDisposition = response.headers.get('content-disposition');
  let fileExtension = path.extname(contentDisposition) || path.extname(urlPath) || '';
  fileExtension = fileExtension.replace(/"/g, '');

  const folder = config.tmpRoot;
  const fileNameWithExtension = `${fileName}${fileExtension}`;
  const filePath = path.join(folder, fileNameWithExtension);

  const fileStream = fs.createWriteStream(filePath);
  await new Promise((resolve, reject) => {
    response.body.pipe(fileStream);
    response.body.on('error', reject);
    fileStream.on('finish', resolve);
  });

  return filePath;
}

async function downloadFiles (req, res) {
  const { filesUri } = req.body;
  const { context: { logger } } = req;

  if (!filesUri || !Array.isArray(filesUri) || filesUri.length === 0) {
    throw new Error('Invalid filesUri array provided');
  }

  try {
    const downloadedFiles = await Promise.all(filesUri.map(async (fileUri) => {
      if (!fileUri) {
        throw new Error('Each file URI should be provided');
      }

      return await downloadFileToTemp(fileUri);
    }));

    return downloadedFiles;
  } catch (e) {
    logger.error(e);
    res.status(e.errorCode || 500).send({ error: e.message });
  }
}

module.exports = downloadFiles;
