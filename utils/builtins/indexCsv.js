'use strict';
const fs = require('fs');
const path = require('path');
const { Transform, pipeline } = require('stream');
const { promisify } = require('util');
const { parse: csvParse } = require('csv-parse');
const { stringify: csvStringify } = require('csv-stringify');
const _get = require('lodash.get');

const { detectCsvDelimiter, extractFileName } = require('./utils');

const pipelinePromise = promisify(pipeline);

const config = global.config;

async function indexCsv ({ file, delimiter = 'detect', idColumnName, outfile }, outputPath, logger) {
  if (delimiter === 'detect') {
    delimiter = await detectCsvDelimiter(file);
    if (!delimiter) {
      throw new Error('Could not detect delimiter');
    }
  }

  if (!outfile) {
    outfile = `${extractFileName(file)}_indexed${path.extname(file)}`;
  }

  const outputFilePath = `${outputPath}/${outfile}`;

  let rowCounter = 0;
  const columnAdder = new Transform({
    objectMode: true,
    transform (row, encoding, done) {
      this.push({ [idColumnName]: rowCounter, ...row });
      rowCounter++;
      done();
    }
  });

  await pipelinePromise(
    fs.createReadStream(file),
    csvParse({ delimiter, columns: true }),
    columnAdder,
    csvStringify({ delimiter, header: true }),
    fs.createWriteStream(outputFilePath, 'utf-8')
  );
  return ({ outputFilename: outfile });
}

async function indexCsvHandler (options, logger) {
  const requiredOptions = ['file', 'idColumnName'];
  const validations = requiredOptions.map((val) => _get(options, val));
  if (validations.some(v => !v)) {
    throw new Error(`Options [${requiredOptions}] must be provided`);
  }

  const folder = config.tmpRoot;

  try {
    const result = await indexCsv(options, folder, logger);
    return { folder, filename: result.outputFilename };
  } catch (e) {
    throw new Error(`Error indexing csv ${e.message}`);
  }
}

module.exports = indexCsvHandler;
