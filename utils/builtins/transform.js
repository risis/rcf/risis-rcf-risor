'use strict';
const fs = require('fs');
const path = require('path');
const { Parser, Transform, transforms: { unwind } } = require('json2csv');
const faker = require('faker');
const _get = require('lodash.get');
const config = global.config;

/**
 * Creates a json2csv transform that flattens an array of key-value pairs within an input object.
 *
 * @param {Object} options - Configuration options.
 * @param {string} options.path - The path to the array of key-value pairs within the input object.
 * @param {Object} options.keyColumnMap - A mapping of key names to target column names.
 * @param {string} options.separator - The separator used to concatenate values with the same key within the key-value pairs array.
 * @returns {Function} A json2csv transform function that flattens a key-value array property based on the provided options.
 */
function flattenKeyValueArray ({ path, keyColumnMap, separator }) {
  /**
   * Transforms an input object by flattening the specified key-value array property.
   *
   * @param {Object} row - The input object containing the key-value array.
   * @returns {Object} A new object with flattened properties.
   */
  return (row) => {
    const result = {};

    // initialize empty arrays for each target column.
    for (const csvColumn of Object.values(keyColumnMap)) {
      result[csvColumn] = [];
    }

    // iterate through the key-value array and populate the result object.
    for (const item of _get(row, path)) {
      const columnName = keyColumnMap[item.key];
      if (columnName !== undefined) {
        result[columnName].push(item.value);
      }
    };

    // join values within each column using the separator
    // in case there are multiple elements with the same key within the key-value pairs array.
    for (const key in result) {
      result[key] = result[key].join(separator);
    };

    // merge the flattened properties with the original input object.
    return { ...row, ...result };
  };
}

const transformsMap = {
  unwind,
  flattenKeyValueArray
};

async function transform (res, body, logger) {
  const { type, from, to, data, file, options, filename } = body;
  const filesList = file;
  const optionsList = options;
  let filenames = filename;
  if (!Array.isArray(filenames)) {
    filenames = [filenames];
  }
  logger.debug('type = %o, from = %o, to = %o, data = %o, optionsList = %o, filesList = %o', type, from, to, data, optionsList, filesList);
  let i = 0;
  const resultItems = [];
  filenames.forEach(async (filename) => {
    filename = filename.replace(/[^0-9A-Za-z-_]/g, '-').toLowerCase();
    filename = `${filename || faker.random.alphaNumeric(10)}.${to || 'csv'}`;
    const folder = config.tmpRoot;
    const dirFile = `${folder}/${filename}`;
    const options = Array.isArray(optionsList) ? optionsList[i] : optionsList;
    const file = Array.isArray(filesList) ? filesList[i] : filesList;
    if (data && type === 'raw' && (to === 'csv' || to === 'tsv') && from === 'json') {
      const parser = new Parser(options);
      const dataConverted = parser.parse(data);
      fs.createWriteStream(dirFile);
      fs.writeFileSync(dirFile, dataConverted);
      resultItems.push({ folder, filename });
    } else if (file && type === 'file' && (to === 'csv' || to === 'tsv') && from === 'json') {
      const input = fs.createReadStream(file, { encoding: 'utf8' });
      input.on('error', (e) => { res.status(500).send({ error: e.message }); });
      const output = fs.createWriteStream(dirFile, { encoding: 'utf8' });
      output.on('error', (e) => { res.status(500).send({ error: e.message }); });
      resultItems.push(new Promise((resolve, reject) => {
        input.on('open', () => {
          if ('transforms' in options) {
            const transforms = [];
            for (const key in options.transforms) {
              if (key in transformsMap) {
                const transformFunction = transformsMap[key](options.transforms[key]);
                transforms.push(transformFunction);
              }
            }
            if (transforms.length > 0) {
              options.transforms = transforms;
            }
          }
          const json2csv = new Transform(options, { encoding: 'utf8' });
          input.pipe(json2csv).pipe(output);
          json2csv.on('error', (e) => res.status(500).send({ error: e.message }));
          json2csv.on('end', () => {
            logger.info('transform finished and file saved at %o', path.join(folder, filename));
            resolve({ folder, filename });
          });
        });
      }));
    } else {
      res.status(400).send({ error: 'Invalid data' });
    }
    i += 1;
  });
  const resultPromises = await Promise.all(resultItems);
  return resultPromises && resultPromises.length === 1 ? resultPromises[0] : { result: resultPromises };
}

module.exports = transform;
