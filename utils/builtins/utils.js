const fs = require('fs');
const { parse: csvParseSync } = require('csv-parse/sync');

function extractFileName (filePath) {
  const pathSegments = filePath.split('/');
  const fileNameWithExtension = pathSegments[pathSegments.length - 1];
  const fileName = fileNameWithExtension.split('.')[0];
  return fileName;
}

async function readLines (file, nLines) {
  const input = fs.createReadStream(file, { flags: 'r' });
  return new Promise((resolve, reject) => {
    let fileContent = '';
    let linesRead = 0;
    input.on('data', (chunk) => {
      fileContent += chunk;
      if (chunk.indexOf('\n') !== -1) {
        linesRead += 1;
      }
      if (linesRead > nLines) {
        input.destroy();
        resolve(fileContent.slice(0, fileContent.lastIndexOf('\n')));
      }
    });
    input.on('error', (error) => reject(error));
    input.on('end', () => resolve(fileContent));
  });
}

async function detectDelimiter (file) {
  const lines = await (readLines(file, 3));
  const candidates = ['\t', ';', ','];
  const delimiter = candidates.find((candidate) => {
    try {
      const parsedLines = csvParseSync(lines, { delimiter: candidate });
      return parsedLines.some(row => row.length);
    } catch {
      return false;
    }
  });
  return delimiter;
}

module.exports = {
  detectCsvDelimiter: detectDelimiter,
  extractFileName
};
