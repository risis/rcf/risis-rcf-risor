'use strict';
const fs = require('fs');
const path = require('path');
const { Transform } = require('stream');
const { parse: csvParse } = require('csv-parse');
const { stringify: csvStringify } = require('csv-stringify');
const { pipeline } = require('stream');
const util = require('util');

const pipelineAsync = util.promisify(pipeline);

const config = global.config;

function extractFileName (filePath) {
  const pathSegments = filePath.split('/');
  const fileNameWithExtension = pathSegments[pathSegments.length - 1];
  const fileName = fileNameWithExtension.split('.')[0];
  return fileName;
}

class CsvConcatenator extends Transform {
  constructor (concatenateColumns, separator, concatenatedColumnName) {
    super({ objectMode: true });
    this.concatenateColumns = concatenateColumns;
    this.separator = separator;
    this.concatenatedColumnName = concatenatedColumnName;
  }

  _transform (row, encoding, done) {
    const concatenatedValues = this.concatenateColumns.map(column => row[column].split('***'));
    const groupedValues = concatenatedValues[0].map((_, i) => concatenatedValues.map(arr => arr[i]).join(this.separator));
    row[this.concatenatedColumnName] = groupedValues.join('***');
    this.push(row);
    done();
  }
}

async function concatenateCsvColumns ({ inputFilePath, concatenateColumns, concatSeparator, fileDelimiter, concatenatedColumnName }, logger) {
  const tempDir = config.tmpRoot;
  const baseFileName = extractFileName(inputFilePath);
  const tempFileName = `${baseFileName}_concat_${concatenateColumns.join('_')}.csv`;
  const outputFilePath = path.join(tempDir, tempFileName);

  const source = fs.createReadStream(inputFilePath).pipe(csvParse({ delimiter: fileDelimiter, columns: true, relax_quotes: true }));
  const transformer = new CsvConcatenator(concatenateColumns, concatSeparator, concatenatedColumnName);
  const destination = fs.createWriteStream(outputFilePath);

  await pipelineAsync(source, transformer, csvStringify({ delimiter: fileDelimiter, header: true }), destination);
  return { folder: tempDir, filename: tempFileName };
}

module.exports = concatenateCsvColumns;
