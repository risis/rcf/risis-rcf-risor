'use strict';
const fs = require('fs');
const path = require('path');
const { Transform, pipeline } = require('stream');
const { promisify } = require('util');
const { parse: csvParse } = require('csv-parse');
const { stringify: csvStringify } = require('csv-stringify');
const _get = require('lodash.get');
const _has = require('lodash.has');

const { detectCsvDelimiter } = require('./utils');

const pipelinePromise = promisify(pipeline);

const config = global.config;

async function alterCsv ({ file, changes, outfile }, outputPath, logger) {
  const delimiter = await detectCsvDelimiter(file);

  const outputFilename = `${outfile}${path.extname(file)}`;
  const outputFilePath = `${outputPath}/${outputFilename}`;

  const columnTransformer = new Transform({
    objectMode: true,
    transform (row, encoding, done) {
      let result = row;
      for (const change of changes) {
        result = change.op(result);
      }
      console.log(result);
      this.push(result);
      done();
    }
  });

  await pipelinePromise(
    fs.createReadStream(file),
    csvParse({ delimiter, columns: true }),
    columnTransformer,
    csvStringify({ delimiter, header: true }),
    fs.createWriteStream(outputFilePath, 'utf-8')
  );

  return ({ outputFilename });
}

const removeColumn = {
  fn: ({ colNames }) => {
    const toRemove = new Set(colNames);
    return function (record) {
      return Object.fromEntries(
        Object.entries(record).filter(([k, _]) => !toRemove.has(k))
      );
    };
  },
  req: ['colNames']
};

const renameColumn = {
  fn: ({ from: renameFrom, to: renameTo }) => {
    return function (record) {
      return Object.fromEntries(
        Object.entries(record).map(([k, v]) => {
          return k === renameFrom ? [renameTo, v] : [k, v];
        })
      );
    };
  },
  req: ['from', 'to']
};

const changeMap = {
  removeColumn,
  renameColumn
};

async function alterCsvHandler (options, logger) {
  const requiredOptions = ['file', 'changes', 'outfile'];
  const validations = requiredOptions.map((val) => _get(options, val));
  if (validations.some(v => !v)) {
    throw new Error(`Options [${requiredOptions}] must be provided`);
  }

  for (const change of options.changes) {
    const [op, param] = Object.entries(change)[0];
    if (!_has(changeMap, op)) {
      throw new Error(`'${op}' operation not supported`);
    }
    for (const req of changeMap[op].req) {
      if (!_has(param, req)) {
        throw new Error(`'${param}' must be specified for ${op}`);
      }
    }
  }
  const changes = options.changes
    .map((change) => Object.entries(change)[0])
    .map(([opKey, params]) => ({ op: changeMap[opKey].fn(params) }));

  const folder = config.tmpRoot;

  try {
    const result = await alterCsv(
      {
        ...options,
        changes
      },
      folder,
      logger
    );
    return { folder, filename: result.outputFilename };
  } catch (e) {
    throw new Error(`Error indexing csv ${e.message}`);
  }
}

module.exports = alterCsvHandler;
