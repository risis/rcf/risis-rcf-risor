'use strict';
const fs = require('fs');
const path = require('path');
const faker = require('faker');
const { parse: csvParse } = require('csv-parse');
const config = global.config;

async function transformCsvToCortextFormat (res, body, logger) {
  const { fileToTransform, delimiter, resultFileName, fieldToGroupBy } = body;
  let filesList = fileToTransform;
  if (!Array.isArray(filesList)) {
    filesList = [filesList];
  }
  logger.debug('fileToTransform = %o, delimiter = %o, resultFileName = %o', fileToTransform, delimiter, resultFileName);
  const filename = `${resultFileName || faker.random.alphaNumeric(10)}.csv`;
  const folder = config.tmpRoot;
  const outputFilePath = `${folder}/${filename}`;
  const data = {};

  return new Promise((resolve, reject) => {
    // Read the CSV file and group data by the specified field
    fs.createReadStream(fileToTransform)
      .pipe(csvParse({ delimiter, columns: true }))
      .on('data', (row) => {
        const key = row[fieldToGroupBy];
        if (!data[key]) {
          data[key] = {};
        }
        // Store all fields except the field to group by
        for (const field in row) {
          if (field !== fieldToGroupBy) {
            if (!data[key][field]) {
              data[key][field] = new Set();
            }
            data[key][field].add(row[field]);
          }
        }
      })
      .on('end', () => {
        // Write the grouped data to the output CSV file
        const outputStream = fs.createWriteStream(outputFilePath);
        if (Object.keys(data).length) {
          const headerRow = Object.keys(data[Object.keys(data)[0]]).map(value => `"${value}"`);
          outputStream.write(`"${fieldToGroupBy}"\t${headerRow.join(delimiter)}\n`);
        }

        for (const key in data) {
          const values = Object.values(data[key]).map((fieldValues) => [...fieldValues].filter(value => value !== '').join('***'));
          values.unshift(key);
          const formattedRow = values.map(value => `"${value}"`).join('\t');
          outputStream.write(formattedRow + '\n');
        }

        outputStream.end();
        outputStream.on('finish', () => {
          logger.info('CSV file transformed to Cortext format and file saved at %o', path.join(folder, filename));
          resolve({ folder, filename });
        });
      });
  });
}

module.exports = transformCsvToCortextFormat;
