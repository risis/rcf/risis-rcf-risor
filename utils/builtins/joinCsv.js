'use strict';
const fs = require('fs');
const path = require('path');
const { Transform } = require('stream');
const { parse: csvParse } = require('csv-parse');
const { stringify: csvStringify } = require('csv-stringify');
const _has = require('lodash.has');

const { detectCsvDelimiter, extractFileName } = require('./utils');

const config = global.config;

class CsvJoiner extends Transform {
  constructor (baseRowsIndex, joinBy, onNonUnique) {
    super({ objectMode: true });
    this.baseRowsIndex = baseRowsIndex;
    this.joinBy = joinBy;
    this.recordGen = nonUniqueDoMap[onNonUnique.do](onNonUnique);
    this.unmatchedRowsBuffer = [];
    this.hasMatched = false;
  }

  async _transform (targetRow, encoding, done) {
    const targetId = targetRow[this.joinBy];
    const baseRows = this.baseRowsIndex.get(targetId);
    if (!baseRows) {
      if (this.hasMatched) {
        this.push(targetRow);
      } else {
        this.unmatchedRowsBuffer.push(targetRow);
      }
    } else {
      for await (const record of this.recordGen(baseRows, targetRow)) {
        this.push(record);
      }
      if (!this.hasMatched) {
        this.hasMatched = true;
        for (const row of this.unmatchedRowsBuffer) {
          this.push(row);
        }
        this.unmatchedRowsBuffer = [];
      }
    }
    done();
  }
}

const nonUniqueDoMap = {
  join: joinRecords,
  concatenate: joinConcatenateRecords
};

function joinRecords () {
  return function * joinGenerator (baseRecords, targetRecord) {
    for (const baseRow of baseRecords) {
      const newCols = {};
      Object.entries(baseRow).forEach(([baseColumnName, baseColumnValue]) => {
        let targetKey = baseColumnName;
        for (let i = 1; targetRecord[targetKey]; i++) {
          targetKey = `${baseColumnName}_${i}`;
        }
        newCols[targetKey] = baseColumnValue;
      });
      yield { ...targetRecord, ...newCols };
    }
  };
}

function joinConcatenateRecords ({ separator = '***' } = {}) {
  return function * joinConcatenateRecords (baseRecords, targetRecord) {
    const newCols = {};
    for (const baseRow of baseRecords) {
      Object.entries(baseRow).forEach(([baseColumnName, baseColumnValue]) => {
        let targetKey = baseColumnName;
        for (let i = 1; targetRecord[targetKey]; i++) {
          targetKey = `${baseColumnName}_${i}`;
        }
        if (_has(newCols, targetKey)) {
          newCols[targetKey].push(baseColumnValue);
        } else {
          newCols[targetKey] = [baseColumnValue];
        }
      });
    }
    for (const [colName, colValue] of Object.entries(newCols)) {
      newCols[colName] = colValue.join(separator);
    }
    yield { ...targetRecord, ...newCols };
  };
}

async function createCsvJoiner (csvA, csvB, { firstColumn, secondColumn, onNonUnique }) {
  if (!onNonUnique.do) {
    throw new Error('onNonUnique.do is not specified');
  }
  if (!_has(nonUniqueDoMap, onNonUnique.do)) {
    throw new Error(`Unsupported non-unique operation ${onNonUnique.do}`);
  }
  const baseRowsIndex = new Map();
  for await (const row of csvA) {
    if (baseRowsIndex.has(row[firstColumn])) {
      baseRowsIndex.get(row[firstColumn]).push(row);
    } else {
      baseRowsIndex.set(row[firstColumn], [row]);
    }
  }
  const joiner = new CsvJoiner(baseRowsIndex, secondColumn, onNonUnique);
  csvB.pipe(joiner);
  return joiner;
}

async function joinTwoCsv (filesConfig, outputPath, { delimiter = '\t', onNonUnique = { do: 'join' } } = {}) {
  if (filesConfig.length !== 2) {
    throw new Error('Only two files can be joined');
  }
  const [configB, configA] = filesConfig;

  const outputFilename = `${extractFileName(configB.filePath)}_${extractFileName(configA.filePath)}${path.extname(configA.filePath)}`;
  const outputFilePath = `${outputPath}/${outputFilename}`;

  const joinOptions = {
    firstColumn: configA.joinBy,
    secondColumn: configB.joinBy,
    onNonUnique
  };

  const source = fs.createReadStream(configA.filePath)
    .pipe(csvParse({ delimiter: configA.delimiter, columns: true }));
  const target = fs.createReadStream(configB.filePath)
    .pipe(csvParse({ delimiter: configB.delimiter, columns: true }));
  const joinedData = await createCsvJoiner(source, target, joinOptions);

  return new Promise((resolve, reject) => {
    joinedData
      .pipe(csvStringify({ delimiter, header: true }))
      .pipe(fs.createWriteStream(outputFilePath));

    joinedData.on('finish', () => {
      resolve({ outputFilename, outputFilePath });
    });
  });
}

async function joinCsvFiles ({ files, options }, logger) {
  if (files.length < 2) {
    throw new Error('At least two files are required to do the join');
  }

  for (const config of files) {
    if (!_has(config, 'filePath') || !_has(config, 'joinBy')) {
      throw new Error('Each fileConfig item must have the filePath and joinBy attributes');
    }
    if (!_has(config, 'delimiter')) {
      config.delimiter = await detectCsvDelimiter(config.filePath);
    }
  };

  const folder = config.tmpRoot;

  const tempfiles = [];
  let pathResultFile = '';
  let joinResult = {};
  let currentBaseFile = files[0];
  for (let i = 1; i < files.length; i++) {
    joinResult = await joinTwoCsv([currentBaseFile, files[i]], folder, options);
    pathResultFile = joinResult.outputFilePath;
    currentBaseFile = {
      filePath: pathResultFile,
      joinBy: currentBaseFile.joinBy,
      delimiter: options.delimiter || files[0].delimiter
    };
    if (i < files.length - 1) {
      tempfiles.push(pathResultFile);
    }
  }
  await Promise.all(tempfiles.map(fs.promises.unlink));
  let resultFilename;
  if (options.outfile) {
    resultFilename = `${options.outfile}${path.extname(files[0].filePath)}`;
    await fs.promises.rename(joinResult.outputFilePath, `${folder}/${resultFilename}`);
  } else {
    resultFilename = joinResult.outputFilename;
  }
  return { folder, filename: resultFilename };
}

module.exports = joinCsvFiles;
