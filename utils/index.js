'use strict';

function filterJSON (filteredFields, json) {
  return Object.fromEntries(Object.entries(json).filter(i => filteredFields.includes(i[0])));
}

function listProviders () {
  const publicFields = ['name', 'uri', 'spec'];
  return Object.entries(global.config.providers)
    .map(i => { return { ...{ id: i[0] }, ...filterJSON(publicFields, i[1]) }; });
}

module.exports = {
  filterJSON,
  listProviders
};
