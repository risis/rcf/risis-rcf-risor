const http = require('http');
const messageMap = Object.assign({}, http.STATUS_CODES); // see https://nodejs.org/docs/latest/api/http.html#http_http_status_codes

/*
 * Inspired by https://github.com/shutterstock/node-common-errors
 */

const codeMap = {
  /*
   * Map custom error names to status codes here
   *
   * ArgumentError: 400
   * UnauthorizedError: 403
   */

};

class HttpMappedError extends Error {
  constructor (error) {
    super();
    this.name = this.constructor.name;
    this.statusCode = error.status || codeMap[error.name] || 500;
    this.stack = error.stack;
    this.message = messageMap[this.statusCode];
  }

  static codeMap () { return codeMap; }
  static messageMap () { return messageMap; }
}

function errorHandler (error, req, res, next) {
  if (!error) {
    if (next) {
      return next();
    }
    return res.end();
  }
  const { context: { logger } } = req;
  const errorToSend = new HttpMappedError(error);
  if (errorToSend.statusCode >= 500) {
    logger.error(error);
  } else {
    logger.info(error.name + ': ' + error.message);
  }
  res.status(errorToSend.statusCode).send(errorToSend.message);
}

module.exports = {
  errorHandler
};
