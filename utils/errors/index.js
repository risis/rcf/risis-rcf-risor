const middleware = require('./middleware');

class BaseError extends Error {
  constructor (message, extError, extraAttributes) {
    super(message);
    // Ensure the error name is the initial thrown error
    this.name = this.constructor.name;
    // Make the stack nicer?
    Error.captureStackTrace(this, this.constructor);
    if (extError && extError.stack) {
      this.stack += '\n - inner error - \n' + extError.stack;
    }
    // Add extra attrs to first level
    if (extraAttributes) {
      Object.keys(extraAttributes).forEach((attributeName) => {
        this[attributeName] = extraAttributes[attributeName];
      });
    }
  }
}

/*
 * Add custom error classes and export them from this module, then use them across all modules. e.g.
 * class ArgumentError extends BaseError {
 *   constructor (argName, extError, extraAttributes) {
 *     super('Invalid argument provided: ' + argName, extError, extraAttributes);
 *   }
 * }
 */

class RuntimeError extends BaseError {
  constructor (message, extError, extraAttributes) {
    super('Unexpected runtime error: ' + message, extError, extraAttributes);
  }
}

module.exports = {
  middleware,
  RuntimeError
};
