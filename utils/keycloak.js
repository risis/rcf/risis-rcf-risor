const jwt = require('jsonwebtoken');
const log4js = require('log4js');
const unirest = require('unirest');
const logger = log4js.getLogger('default');
const url = require('url');

const config = global.config.keycloak;

async function getPublicKey () {
  try {
    if (config.publicKey) {
      logger.debug('keycloak public_key found in config file', config.publicKey);
      return config.publicKey;
    } else if (!global._keycloakPublicKey) {
      const keycloakUri = url.format(Object.assign(config.uri, { pathname: '/auth/realms/rcf' }));
      logger.debug('keycloak public_key not found');
      logger.debug('retrieving keycloak public_key at', keycloakUri);
      const res = await unirest.get(keycloakUri);
      global._keycloakPublicKey = res.body.public_key;
      logger.debug('keycloak public_key =', global._keycloakPublicKey);
    }
    return `-----BEGIN PUBLIC KEY-----\n${global._keycloakPublicKey}\n-----END PUBLIC KEY-----`;
  } catch (err) {
    logger.error('Unexpected error getting keycloak public key');
    logger.error(err);
  }
}

async function getToken () {
  try {
    const keycloakPublicKey = await getPublicKey();
    jwt.verify(global._rcf_token, keycloakPublicKey, { algorithms: ['RS256'] });
    logger.debug('token is valid, ok!');
    return global._rcf_token;
  } catch (err) {
    logger.debug('token invalid, refreshing token...', err.message);
    logger.debug(err.stack);
    const params = {
      grant_type: 'client_credentials',
      client_id: config.clientId,
      client_secret: config.clientSecret
    };
    logger.info(`requesting token from ${config.endpoints.token}`);
    const tokenResponse = await unirest.post(config.endpoints.token).form(params);
    logger.debug('token response status', tokenResponse.status);
    if (tokenResponse.body && tokenResponse.body.access_token) {
      global._rcf_token = tokenResponse.body.access_token;
      return global._rcf_token;
    } else {
      logger.error('error requesting token %s', tokenResponse.body);
      if (tokenResponse.error) {
        logger.error(tokenResponse.error);
      }
      throw new Error('error requesting token');
    }
  }
}

module.exports = {
  getToken,
  getPublicKey
};
