/*!
 * RSML Engine
 * Copyright(c) 2018-2023 Cogniteva SAS
 */
'use strict';

/*!
 * Error for Nucleus
 * Represents custom errors that occurs when dealing with nucleus flows.
 */

/**
 * Module dependencies.
 * @private
 */
const stringify = require('json-stable-stringify');
const utils = require('../utils');

/**
 * CustomError
 * @class CustomError
 * @extends {Error}
 * @constructor
 * @param {string} options.title - A short message describing the error.
 * @param {string} options.message - A detailed message describing the error.
 * @param {Object} options.context - Context object related to the error.
 */
class CustomError extends Error {
  constructor (options, defaults = { }) {
    super();
    Error.captureStackTrace && Error.captureStackTrace(this, this.constructor);
    options = utils.setDefaults(options, defaults);
    this.kind = this.constructor.kind;
    this.name = this.constructor.name;
    this.type = this.constructor.type;
    this.title = options.title || utils.toSentenceCase(this.name);
    this.message = options.message;
    this.context = options.context;
    this.isFriendly = options.isFriendly || false;
  }

  /**
   * Getter method for the `type` property.
   * @return {string} The `type` value.
   */
  get type () {
    return '/' + 'error' +
           '/' + this.kind.toLowerCase().replace(/\s/g, '') +
           '/' + this.name.toLowerCase().replace(/\s/g, '') +
           '/' + this._type.toLowerCase().replace(/\s/g, '-');
  }

  /**
   * Setter for the type property.
   * @param {string} type - The new value for the type property.
   */
  set type (type) {
    this._type = type;
  }

  /**
   * Getter for the context property of the class.
   * @returns {any} The value of the _context property.
   */
  get context () {
    return this._context;
  }

  /**
   * Setter for the context property of the class. Assigns the value
   * passed to the function to the _context property.
   * @param {any} context - The value to be assigned to the _context property.
   */
  set context (context) {
    if (context && stringify(context) !== '{}') {
      let contextError;
      if (context instanceof Error && !(context instanceof CustomError)) {
        contextError = {
          code: context.code ? context.code : -1,
          type: context.type ? context.type : 'js/' + context.constructor.name,
          name: context.constructor.name,
          title: context.title ? context.title : utils.toSentenceCase(context.name),
          message: context.message
        };
        // add context under the host.error key
        this._context = { js: { error: contextError } };
      } else if (context instanceof CustomError) {
        // add context under the parent key
        this._context = { parent: context.toJSON() };
      } else if (context && Object.prototype.hasOwnProperty.call(context, 'error')) {
        // add context under the host.error key
        this._context = { js: { error: context.error } };
      } else {
        // add context under the extra key
        this._context = { extra: context };
      }
    } else {
      this._context = {};
    }
  }

  /**
    * Returns an object representation of the error, with target  properties
    * and their values.
    * @returns {Object} An object with the properties and values of the error.
    */
  toJSON () {
    return {
      type: this.type,
      name: this.name,
      title: this.title,
      message: this.message,
      context: this.context,
      isFriendly: this.isFriendly
    };
  }

  /**
   * Returns a string representation of the Error, using first the toJSON
   * function to convert the Error to an object first.
   * @returns {string} A string representation of the class.
  */
  serialize () {
    return stringify(this.toJSON(), {
      space: '  '
    });
  }
};

/**
 * Module exports.
 * @public
 */
module.exports = CustomError;
