/*!
 * RSML Engine
 * Copyright(c) 2018-2023 Cogniteva SAS
 */
'use strict';

/*!
 * Error for Nucleus
 * FileError represents error that occurs when dealing with files.
 */

/**
 * Module dependencies.
 * @private
 */

const CustomError = require('./custom-error');

const createFileError = (type, defaults = {}) => {
  /**
   * @class FileError
   * @extends {RSMLError}
   * @constructor
   * @param {string} options.title - A short message describing the error.
   * @param {string} options.message - A detailed message describing the error.
   * @param {Object} options.context - Context object related to the error.
   */
  class FileError extends CustomError {
    constructor (options) {
      super(options, defaults);

      this.kind = 'general';
      this.name = 'file-error';
      this.type = type;
    }
  }
  return FileError;
};

/**
 * Module exports.
 * @public
*/
module.exports = {
  /**
    NotFound is a generic error class that represents a error that
    occurs when a file is not found.
    @param {string} options.title - A short message describing the error.
    @param {string} options.message - A detailed message describing the error.
    @param {string} options.context - A error context.
    */
  Analysis: createFileError('analysis'),
  Append: createFileError('append'),
  Backup: createFileError('backup'),
  BadEncoding: createFileError('bad-encoding'),
  ChecksumMismatch: createFileError('checksum-mismatch'),
  Comparison: createFileError('comparison'),
  Compression: createFileError('compression'),
  Concatenation: createFileError('concatenation'),
  Conversion: createFileError('conversion'),
  Copy: createFileError('copy'),
  Corrupted: createFileError('corrupted'),
  Decoding: createFileError('decoding'),
  Decompression: createFileError('decompression'),
  Decryption: createFileError('decryption'),
  Deletion: createFileError('deletion'),
  Download: createFileError('download'),
  Empty: createFileError('empty'),
  Encoding: createFileError('encoding', {
    title: 'Missing encoding',
    message: 'Unable to identify the file encoding'
  }),
  Encryption: createFileError('encryption'),
  FormatConversion: createFileError('format-conversion'),
  Integration: createFileError('integration'),
  InvalidName: createFileError('invalid-name'),
  InvalidVersion: createFileError('invalid-version'),
  InvalidType: createFileError('invalid-type'),
  Lock: createFileError('lock'),
  Merge: createFileError('merge'),
  Metadata: createFileError('metadata'),
  MetadataUpdate: createFileError('metadata-update'),
  Move: createFileError('move'),
  NotFound: createFileError('not-found', {
    title: 'File not found',
    message: 'The request file does not exist'
  }),
  NotSupported: createFileError('not-supported'),
  OutOfSync: createFileError('out-of-sync'),
  PermissionDenied: createFileError('permission-denied'),
  Read: createFileError('read'),
  Rename: createFileError('rename'),
  Restore: createFileError('restore'),
  Search: createFileError('search'),
  Sign: createFileError('sign'),
  Split: createFileError('split'),
  Sync: createFileError('sync'),
  TooLarge: createFileError('too-large'),
  TypeDetection: createFileError('type-detection'),
  UnsupportedFormat: createFileError('unsupported-format'),
  UnsupportedVersion: createFileError('unsupported-version'),
  Upload: createFileError('upload'),
  Validation: createFileError('validation'),
  Verify: createFileError('verify'),
  Write: createFileError('write')
};
