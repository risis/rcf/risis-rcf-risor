/*!
 * RSML Engine
 * Copyright(c) 2018-2023 Cogniteva SAS
 */
'use strict';

/*!
 * Error for RSML
 */

/**
 * Module dependencies.
 * @private
 */

const CustomError = require('./custom-error');
const utils = require('../utils');
const constants = require('../constants');
const status = require('statuses').STATUS_CODES;

/**
 * RSMLError
 * @class RSMLError
 * @extends {CustomError}
 * @constructor
 * @param {string} options.title - A short message describing the error.
 * @param {string} options.message - A detailed message describing the error.
 * @param {string} options.taskName - Name of the task that caused the error.
 * @param {Object} options.context - Context object related to the error.
 */
class RSMLError extends CustomError {
  constructor (options, defaults = {}) {
    super(options, defaults);

    this.kind = 'rsml';
    this.taskname = options.taskName;
    this.rsml = {
      scenario: { spec: '?', name: '?', version: '?' },
      engine: { spec: constants.RSML_ENGINE_SPEC_VERSION },
      error: { task: { name: this.taskname } }
    };
  }

  /**
   * Returns the RSML object associated with this error.
   * @return {Object} the RSML object
   */
  get rsml () {
    return this._rsml;
  }

  /**
   * Setter method for the `rsml` property.
   * @param {Object} obj The new value for the `rsml` object.
   */
  set rsml (obj) {
    this._rsml = obj;
  }

  /**
   * Getter method for the `taskname` property.
   * @return {string} The `taskname` value.
   */
  get taskname () {
    return this._taskName;
  }

  /**
   * Setter for the taskname property.
   * @param {string} taskName - The new value for the taskname property.
   */
  set taskname (taskName) {
    this._taskName = utils.stripChar(taskName, '/');
    if ('_rsml' in this) {
      this.rsml.error.task.name = this._taskName;
    }
  }

  /**
    * Returns an object representation of the error, with target  properties
    * and their values.
    * @returns {Object} An object with the properties and values of the error.
    */
  toJSON () {
    return utils._merge(super.toJSON(), {
      context: { rsml: this._rsml }
    });
  }
}

/**
 * createRSMLCustomError
 * @param {string} name - The name of the custom error.
 * @param {string} type - A string identifier for the type of error.
 * @returns {function} - A new custom error class that inherits from the RSMLError class.
 */
const createRSMLCustomError = (name, type, defaults) => {
  /**
   * @class RSMLCustomError
   * @extends {RSMLError}
   * @constructor
   * @param {string} options.title - A short message describing the error.
   * @param {string} options.message - A detailed message describing the error.
   * @param {string} options.taskName - Name of the task that caused the error.
   * @param {Object} options.context - Context object related to the error.
   */
  class RSMLCustomError extends RSMLError {
    constructor (options) {
      super(options, defaults);

      this.name = name;
      this.type = type;
    }
  }
  return RSMLCustomError;
};

/**
 * createRSMLHTTPError
 * @param {string} name - The name of the custom error.
 * @param {string} type - A string identifier for the type of error.
 * @returns {function} - A new custom error class that inherits from the RSMLError class.
 */
const createRSMLHTTPError = (name, type, defaults) => {
  /**
   * @class RSMLHTTPError
   * @extends {RSMLError}
   * @constructor
   * @param {string} options.title - A short message describing the error.
   * @param {string} options.message - A detailed message describing the error.
   * @param {string} options.taskName - Name of the task that caused the error.
   * @param {Object} options.response - The HTTP response object returned by the request.
   */
  class RSMLHTTPError extends RSMLError {
    constructor (options) {
      super(options, utils.setDefaults(defaults, {
        title: `HTTP ${options.response.status} ${status[options.response.status]}`,
        context: {
          http: {
            method: options.response.req.method,
            path: options.response.req.path.split('?')[0],
            status: options.response.status,
            body: options.response.raw_body
              ? utils.stringifyNonString(options.response.raw_body).slice(0, 255)
              : ''
          }
        }
      }));

      this.name = name;
      this.type = type;
    }
  }
  return RSMLHTTPError;
};

/**
 * Module exports.
 * @public
 */
module.exports = {
  /**
    ExecutionError represents errors that occurs when executing RSML.
   */
  ExecutionError: {
    /**
      Engine is a generic error class that represents a generic error from
      the RSML engine.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.context - Context object related to the error.
      */
    Engine: createRSMLCustomError('execution-error', 'engine', {
      title: 'RSML execution error',
      taskName: ''
    }),
    /**
      Response is a generic error class that represents a error that
      occurs when prepaparing the RSML response.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.context - Context object related to the error.
      */
    Response: createRSMLCustomError('execution-error', 'response', {
      title: 'RSML response processing error',
      taskName: ''
    })
  },

  /**
    ParseError represents parsing errors that occurs when interpreting RSML.
    */
  ParseError: {
    /**
      Yaml is a custom error class that represents a parsing error that
      occurs when parsing a YAML document.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.taskName - The name of the RSML task that triggered the error.
      @param {string} options.context - Context object related to the error.
      */
    Yaml: createRSMLCustomError('parse-error', 'yaml', {
      title: 'YAML parsing error'
    }),
    /**
      Json is a custom error class that represents a parsing error that
      occurs when parsing a JSON document.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.taskName - The name of the RSML task that triggered the error.
      @param {string} options.context - Context object related to the error.
      */
    Json: createRSMLCustomError('parse-error', 'json', {
      title: 'JSON parsing error'
    }),
    /**
      Rsml is a custom error class that represents a parsing error that
      occurs when parsing a RSML document.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.taskName - The name of the RSML task that triggered the error.
      @param {string} options.context - Context object related to the error.
      */
    Rsml: createRSMLCustomError('parse-error', 'rsml', {
      title: 'RSML parsing error'
    }),
    /**
      StringLiterals is a custom error class that represents a parsing error that
      occurs when rendering a string literal.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.taskName - The name of the RSML task that triggered the error.
      @param {string} options.context - Context object related to the error.
      */
    StringLiterals: createRSMLCustomError('parse-error', 'string-literals', {
      title: 'String-literals parsing error'
    }),
    /**
      RenderString is a custom error class that represents a parsing error that
      occurs when rendering a template string.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.taskName - The name of the RSML task that triggered the error.
      @param {string} options.context - Context object related to the error.
      */
    RenderString: createRSMLCustomError('parse-error', 'render-string', {
      title: 'Render template parsing error'
    })
  },

  /**
    TaskError represents generic errors that occurs when executing a RSML task.
    */
  TaskError: {
    /**
      Failure is a error class that represents a failure that occurs when
      executing a RSML task.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.taskName - The name of the RSML task that triggered the error.
      @param {string} options.context - Context object related to the error.
      */
    Failure: createRSMLCustomError('task-error', 'failure', {
      title: 'Task failure'
    })
  },

  /**
    TriggerError represents errors that occurs when interpreting the trigger
    of a task.
    */
  TriggerError: {
    /**
      FailRequested is a custom error class that represents a error that
      occurs when interpreting the trigger of a task.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.taskName - The name of the RSML task that triggered the error.
      @param {string} options.context - Context object related to the error.
      */
    FailRequested: createRSMLCustomError('trigger-error', 'fail-requested', {
      title: 'Trigger error'
    })
  },

  /**
    HTTPError represents errors that occurs when making HTTP requests.
    */
  HTTPError: {
    /**
      TaskRequest is a custom error class that represents an HTTP error that
      occurs when making a request to an RSML endpoint.
      @param {string} options.title - A short message describing the error.
      @param {string} options.message - A detailed message describing the error.
      @param {string} options.taskName - The name of the RSML task that triggered the error.
      @param {Object} options.response - The HTTP response object returned by the request.
      */
    TaskRequest: createRSMLHTTPError('http-error', 'task-request', {
      /* do not put a default title */
      message: 'An error occurs while making a request to an RSML endpoint',
      taskName: ''
    })
  }
};
