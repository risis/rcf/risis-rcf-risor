const { logging } = global.config;
const util = require('util');
const log4js = require('log4js');

const rsmlLoggingConfig = {
  appenders: {
    stdout: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        pattern: '%d level=%p %x{traceid}%x{flatten} at=%f{3}.%l%n',
        tokens: {
          traceid: ({ context }) => context.traceid ? 'traceid=' + context.traceid + ' ' : '',
          flatten: ({ data }) => util.format.apply(this, data).replace(/\n/g, ' ')
        }
      }
    },
    stderr: {
      type: 'stderr',
      layout: {
        type: 'coloured'
      }
    },
    errors: {
      type: 'logLevelFilter',
      level: 'ERROR',
      appender: 'stderr'
    },
    rsml_console: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        pattern: ' %[[%x{levelSymbol}] %c %X{scenarioId}% %X{taskId}%]: %m',
        tokens: {
          levelSymbol: function (logEvent) {
            switch (logEvent.level.levelStr) {
              case 'TRACE': return '..';
              case 'DEBUG': return '%%';
              case 'INFO': return 'II';
              case 'MARK': return '!!';
              case 'WARN': return 'WW';
              case 'ERROR': return 'EE';
              case 'FATAL': return 'CC';
              default: return '';
            }
          }
        }
      }
    }
  },
  categories: {
    default: {
      appenders: [
        'stdout',
        'errors'
      ],
      level: logging.level,
      enableCallStack: true
    },
    'rsml-engine': {
      appenders: ['rsml_console'],
      level: logging.level
    }
  }
};

log4js.configure(rsmlLoggingConfig);
const logger = log4js.getLogger('rsml-engine');

module.exports = logger;
