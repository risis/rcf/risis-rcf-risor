/*!
 * RSML Engine
 * Copyright(c) 2018-2023 Cogniteva SAS
 */
'use strict';

// constants is a library of constants specific to the RSML Engine

const rsml = require('./package.json');
const utils = require('./utils');

module.exports = {
  RSML_ENGINE_SPEC_VERSION: utils.semver.coerce(rsml.version).version,
  RSML_ENGINE_SPEC_SUPPORTED_VERSION_RANGE: '>= 0.6.0',
  RSML_FILE_HEADER_SHARP_PERCENT: '#%RSML',
  RSML_FILE_EXTENSION: 'rsml'
};
