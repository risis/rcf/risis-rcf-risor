'use strict';

const yaml = require('yaml');
const turtl = require('./turtl');
const querystring = require('querystring');
const url = require('url');
const extend = require('extend');
const _isEqual = require('lodash/isEqual');
const nunjucks = require('nunjucks');
const filters = require('./filters');
const urljoin = require('url-join');
const path = require('path');
const hash = require('object-hash');
const shortid = require('shortid');
const fileerror = require('./custom-error/file-error');
const rsmlerror = require('./custom-error/rsml-error');
const utils = require('./utils');
const constants = require('./constants');
const JSON5 = require('json5');
const filechecker = require('./custom-checker/file-checker');
const mime = require('mime');
const fs = require('fs');
const fetch = require('node-fetch');
const FormData = require('form-data');

const config = global.config;

const logger = require('./config/logging');

// templates live at path 'scenarios/models/{scenario-folder}/include/'
// turn off autoescaping when render templates
const mainIncludePath = path.join(__dirname, 'models');
const env = nunjucks.configure([mainIncludePath], { autoescape: false });

// installs experimental support for more consistent Jinja compatibility
// by adding Pythonic APIs to the environment
// @see https://github.com/mozilla/nunjucks/issues/530
nunjucks.installJinjaCompat();

// on nunjucks, allow to access the global context using 'this()'
env.addGlobal('this', function () {
  return this.getVariables();
});

// Add filters to the nunjucks environment
Object.keys(filters).forEach(filterName => {
  env.addFilter(filterName, filters[filterName]);
});

// returns a promise that resolves after a delay
// @see https://stackoverflow.com/a/50092576
function delay (t, val) {
  return new Promise(function (resolve) {
    setTimeout(function () {
      resolve(val);
    }, t);
  });
}

// renders a string template using nunjucks
function rsmlRenderTemplate (template, context, taskName, env, callback) {
  // try to render a raw template as string
  let renderedString;
  try {
    renderedString = env.renderString(template, context, callback);
  } catch (error) {
    const parseError = new rsmlerror.ParseError.RenderString({
      message: `An error found while expanding the expression: ${template}`,
      taskName,
      context: error
    });
    throw parseError;
  }

  return renderedString;
}

// renders a template with string literals (${foo.bar.baz}) using turtl
function rsmlRenderLiterals (template, context, taskName) {
  // try to render a raw string
  let renderedString;
  try {
    renderedString = turtl.render(template, context);
  } catch (error) {
    const parseError = new rsmlerror.ParseError.RenderString({
      message: `An error found while expanding the expression: ${template}`,
      taskName,
      context: error
    });
    throw parseError;
  }

  return renderedString;
}

// render a template string and then try to interpret the result as an object
function rsmlParseStringAsObject (template, context, taskName) {
  // try to render a raw string
  let parsedString;
  try {
    parsedString = turtl.parse(template, context);
  } catch (parseError) {
    parseError.taskname = taskName;
    throw parseError;
  }

  return parsedString;
}

// first, expand {{}} template variables in input using map as context
// then,  expand ${}  scenario variables in input using map as context
const expandObjectVariables = function (input, map, taskName) {
  map = map || input;
  turtl.explore(input, {
    filter: (attributes) => {
      return typeof attributes.value === 'string';
    },
    update: (attributes) => {
      const currentValue = attributes.value;
      const expandedValue = rsmlRenderTemplate(currentValue, map, taskName, env);
      attributes.value = rsmlParseStringAsObject(expandedValue, map, taskName);
      return !_isEqual(currentValue, attributes.value);
    }
  });
  return true;
};

class RsmlEngine {
  static #supportedCommands = ['STOP']; // jshint ignore:line
  constructor () {
    this.executors = new Map();
  }

  async runScenario (scenarioId, instanceId, context, params) {
    const executor = new RsmlExecutor(scenarioId, instanceId, context, params);
    this.executors.set(instanceId, executor);
    const response = await executor.run();
    this.executors.delete(instanceId);
    return response;
  }

  get runningInstances () {
    return Array.from(this.executors.keys());
  }

  processCommand (instanceId, command) {
    if (!RsmlEngine.#supportedCommands.includes(command)) { // jshint ignore:line
      throw new Error('Unsupported command');
    }
    if (this.executors.has(instanceId)) {
      return this.executors.get(instanceId).stop();
    }
  }
}

class Stopper {
  constructor () {
    const p = this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
    this.then = this.promise.then.bind(p);
    this.catch = this.promise.catch.bind(p);
    if (this.promise.finally) {
      this.finally = this.promise.finally.bind(p);
    }
    this.abortController = new AbortController();
  }

  stopExecution () {
    this.resolve({ stopped: true });
    this.abortController.abort();
  }

  get signal () {
    return this.abortController.signal;
  }
}

class RsmlExecutor {
  constructor (scenarioId, instanceId, context, params) {
    this.context = context;
    this.scenarioId = scenarioId;

    logger.addContext('scenarioId', '/' + scenarioId);
    logger.addContext('taskId', '');

    let scenarioPath;
    // download scenario files if it is not in cache
    if (process.env.NODE_ENV.trim() === 'development') {
      scenarioPath = path.join(config.appRoot, 'scenarios', 'models', `${scenarioId}`);
    } else {
      scenarioPath = path.join(config.appRoot, config.scenarios.path, scenarioId);
    }

    // Get parameters directly from the input parameters.
    this.inputParams = {
      params
    };

    // create a short non-sequential url-friendly unique id for the current running instance
    this.uidInstance = instanceId;

    // raw scenario definition
    this.scenarioFileName = `${scenarioId}.${constants.RSML_FILE_EXTENSION}`;
    this.scenarioFilePath = path.join(scenarioPath, this.scenarioFileName);

    // stopper
    this.stopper = new Stopper();
  }

  makeStoppable (promise) {
    return Promise.race([promise, this.stopper]);
  }

  stop () {
    this.stopper.stopExecution('Execution stopped');
  }

  async run () {
    const rsml = await this.loadRsml();

    // the main task is equal to scenario name
    const mainTask = '/' + rsml.name;
    logger.debug('%o', utils.stringifyVariable('mainTask', mainTask));

    // merge the parsed scenario with the input params and then generate a MD5 hash
    const hashScenario = hash.MD5({ ...rsml, ...this.inputParams });

    // create a context accesible via ${self.foo}
    const inputContext = {
      self: {
        name: this.scenarioId,
        hash: hashScenario,
        uid: this.uidInstance
      }
    };

    logger.debug('initial %s', utils.stringifyVariable('inputContext', inputContext));

    // extend the input context with the input params
    extend(true, inputContext, this.inputParams);
    extend(true, inputContext, { context: this.context });

    // inject rsml options inside input context
    extend(true, inputContext, { options: rsml.options });

    logger.debug('extended %s', utils.stringifyVariable('inputContext', inputContext));

    // process the scenario starting from the main task
    const responseRsml = await this.makeStoppable(this.rsmlProcess(rsml, mainTask, inputContext));

    logger.debug('%s', utils.stringifyVariable('responseRsml', responseRsml));

    // once the execution is finished
    let parsedResponse = responseRsml;

    // only if the RSML defines an expected response template
    if (rsml.response) {
      try {
        // try to render the RSML response key
        expandObjectVariables(rsml.response, responseRsml, mainTask);
        // now responseRsml becomes a context and the object
        // with string templates on rsml.response the result
        parsedResponse = {
          context: responseRsml,
          result: rsml.response
        };
      } catch (error) {
        // if an error occurs, throw an execution error
        throw (new rsmlerror.ExecutionError.Response({
          message: `An error occurs while rendering the RSML response for /${mainTask}`,
          context: error
        }));
      }
    }

    logger.debug('%s', utils.stringifyVariable('parsedResponse', parsedResponse));

    return parsedResponse;
  }

  async loadRsml () {
    // [ rsmlExist, rsmlReadable, rsmlIsString,
    // rsmlNotEmpty,
    // rsmlValidHeader, rsmlSupportedVersion,
    // rsmlValidYaml, rsmlValidJson ]

    // test if the scenario file exits
    const scenarioFileExist = await filechecker.fileExists(this.scenarioFilePath, {
      title: 'The requested scenario has not been found',
      message: `Unable to open ${this.scenarioFileName}`
    }).run();

    utils.throwIfError(scenarioFileExist);

    // try to load the scenario definition
    const scenarioFileContents = await filechecker.fileReadSync(this.scenarioFilePath, {
      title: 'Unable to read the requested scenario',
      message: `Unable to read ${this.scenarioFileName}`
    }).run();

    utils.throwIfError(scenarioFileContents);

    // test if the content is a string
    // test if the content is not empty

    // get file header
    const scenarioFileLines = scenarioFileContents.split('\n');
    const scenarioFileHeader = scenarioFileLines.find((line) => line.trim() !== '');

    // check if we have a valid header
    if (!scenarioFileHeader.startsWith(constants.RSML_FILE_HEADER_SHARP_PERCENT)) {
      throw (new fileerror.InvalidType({
        title: 'Invalid RSML file',
        message: `Unable to found a valid RSML header: ${constants.RSML_FILE_HEADER_SHARP_PERCENT}`,
        context: {
          'invalid-type': scenarioFileHeader,
          'expected-type': constants.RSML_FILE_HEADER_SHARP_PERCENT
        }
      }));
    }

    // check if we have a valid version number
    const scenarioFileVersion = utils.trim(scenarioFileHeader.slice(
      constants.RSML_FILE_HEADER_SHARP_PERCENT.length));
    const scenarioFileCoersedVersion = utils.semver.coerce(scenarioFileVersion);
    if (!utils.semver.valid(scenarioFileCoersedVersion)) {
      throw (new fileerror.InvalidVersion({
        title: 'Invalid RSML version number',
        message: 'Unable to found a valid RSML version number',
        context: {
          'invalid-version': scenarioFileVersion
        }
      }));
    }

    // check it the current version number satisfies the engine implementation
    if (!utils.semver.satisfies(scenarioFileCoersedVersion,
      constants.RSML_ENGINE_SPEC_SUPPORTED_VERSION_RANGE)) {
      throw (new fileerror.UnsupportedVersion({
        title: 'Unsupported RSML version',
        message: 'Unable to found a supported RSML version',
        context: {
          'unsupported-version': scenarioFileVersion,
          'expected-version-range': constants.RSML_ENGINE_SPEC_SUPPORTED_VERSION_RANGE
        }
      }));
    }

    // this will contains the parsed scenario
    let rsml;

    // try to parse the scenario definition
    try {
      rsml = yaml.parse(scenarioFileContents);
    } catch (error) {
      throw (new rsmlerror.ParseError.Yaml({
        title: 'Bad scenario definition',
        message: `Make sure that your RSML '${this.scenarioId}' scenario is a valid YAML file`,
        context: error
      }));
    }

    return rsml;
  }

  async rsmlProcess (rsml, task, context) {
    const taskList = rsml[task];

    if (taskList === undefined) {
      throw new rsmlerror.ExecutionError.Engine({
        taskname: task,
        message: `${task} is not one of the declared RSML tasks`
      });
    }

    for (const rsmlNode of Object.values(taskList)) {
      const taskName = Object.keys(rsmlNode)[0];
      if (taskName.startsWith('/')) {
        logger.addContext('taskId', taskName);

        let inputParams = {};
        if ('params' in rsmlNode) {
          // process task params
          inputParams = JSON5.parse(rsmlParseStringAsObject(
            JSON.stringify(rsmlNode.params),
            context,
            taskName));

          logger.debug('%s', utils.stringifyVariable('inputParams', inputParams));
        }
        // task '/foo' becomes 'foo'
        const idTask = taskName.substring(1);
        // create a context accesible via ${self.foo}
        const inputContext = {
          self: {
            name: idTask
          }
        };

        // only if there are parameters
        if (Object.keys(inputParams).length) {
          // extend the input context with the input params
          inputContext.params = inputParams;
        }

        logger.debug('%s', utils.stringifyVariable('inputContext', inputContext));

        // initialize task context
        context[idTask] = inputContext;
        // process the task
        const response = await this.makeStoppable(this.rsmlProcess(rsml, taskName, context));

        // exit early if anything fails
        if ('status' in response && !(response.status >= 200 && response.status < 300)) {
          throw ((new rsmlerror.HTTPError.TaskRequest({
            taskName,
            response
          })));
        } else if (response instanceof Error) {
          throw (new rsmlerror.TaskError.Failure({
            message: response.message,
            taskName,
            context: response
          }));
        } // if ('status' in response && ...

        logger.debug('%s', utils.stringifyVariable('response.status', response.status));
        logger.addContext('taskId', '');
      } else {
        // we have a task body, process it
        return await this.makeStoppable(this.rsmlTaskRepeatExecute(rsml, task, taskList, context));
      }
    }

    return context;
  }

  async rsmlTaskRepeatExecute (rsml, task, taskList, context) {
    let expandedOutputContext = {};
    const response = await this.rsmlTaskExecute(rsml, task, taskList, context);
    logger.addContext('taskId', task);

    // keep response status as part of the self context
    if (typeof (context.self) !== 'undefined' && 'status' in response) {
      context.self.response = { status: response.status };
    }

    // render output template
    if ('status' in response && (response.status >= 200 && response.status < 300)) {
      const httpMethod = Object.keys(taskList)[1];

      let outputTemplate = taskList[httpMethod].output;
      logger.debug('%s', utils.stringifyVariable('outputTemplate', outputTemplate));

      const outputKey = task.substring(1);

      // only render task output if there is a template
      if (typeof outputTemplate !== 'undefined') {
        // convert an array to a json template
        if (Array.isArray(outputTemplate)) {
          // [ {a:1} {b:2} {c:3} ] to '{ a:1, b:2, c3 }'
          outputTemplate = JSON.stringify((Object.assign({}, ...outputTemplate)));
        }
        logger.debug('asArray %s', utils.stringifyVariable('outputTemplate', outputTemplate));

        // try to update the current context using the output template
        if (typeof outputTemplate === 'string') {
          const output = rsmlRenderTemplate(outputTemplate, response.parsed_body, task, env);
          const outputContext = `{ ${outputKey}  : ${output} }`;
          // resolve output context variables
          expandedOutputContext = rsmlParseStringAsObject(outputContext, context, task);
          // preserve taks attributes like params
          extend(expandedOutputContext[outputKey], context[outputKey]);
        }
      } else {
        // otherwise use the raw body as output
        expandedOutputContext[outputKey] = response.parsed_body;
      }

      // process triggers
      if ('triggers' in taskList[httpMethod]) {
        // create a copy of the triggers to avoid update the object itself
        const triggers = JSON.parse(JSON.stringify(taskList[httpMethod].triggers));
        // create a trigger context equal to the global context extended with the expandedOutputContext
        const triggerContext = extend({}, context, expandedOutputContext);
        // loop over triggers
        // eslint-disable-next-line no-unused-vars
        for (const [index, trigger] of triggers.entries()) {
          // resolve condition as string
          const triggerCondition = rsmlRenderLiterals(utils.stringifyNonString(trigger.condition), triggerContext, task);
          logger.debug('%s', utils.stringifyVariable('triggerCondition', triggerCondition));
          // resolve params as object
          const triggerParams = rsmlParseStringAsObject(trigger.params, triggerContext, task);
          logger.debug('%s', utils.stringifyVariable('triggerParams', triggerParams));
          // evaluate the condition using template logic
          const triggerResult = rsmlRenderTemplate(triggerCondition, triggerContext, task, env);
          logger.debug('%s', utils.stringifyVariable('triggerResult', triggerResult));

          // process trigger afterOutput repeat and checks if triggerResult is resolved as 'true'
          if (trigger.when === 'afterOutput' && triggerResult === 'true') {
            // execute the requested action
            switch (trigger.action) {
              // repeat
              case 'repeat':
                await delay(Number(triggerParams.delay) * 1000);
                return await this.makeStoppable(this.rsmlTaskRepeatExecute(rsml, task, taskList, context));
              // fail
              case 'fail':
                throw (new rsmlerror.TriggerError.FailRequested({
                  message: triggerParams.message,
                  taskName: task
                }));
            } // switch (trigger.action)
          } // if (trigger.when === 'afterOutput' && triggerResult === 'true')
        } // for (const trigger of triggers)
      } // if ('triggers' in taskList[httpMethod])

      if (expandedOutputContext) {
        // update the global context with the task context
        extend(context, expandedOutputContext);
        logger.debug('%s', utils.stringifyVariable('context', context));
      }
    } else {
      // TODO() trigger management needs to be refactored in an independent function
      const httpMethod = Object.keys(taskList)[1];
      // process triggers
      if ('triggers' in taskList[httpMethod]) {
        // create a copy of the triggers to avoid update the object itself
        const triggers = JSON.parse(JSON.stringify(taskList[httpMethod].triggers));
        // create a trigger context equal to the global context extended with the expandedOutputContext
        const triggerContext = extend({}, context, expandedOutputContext);
        // loop over triggers
        // eslint-disable-next-line no-unused-vars
        for (const [index, trigger] of triggers.entries()) {
          // resolve condition as string
          const triggerCondition = rsmlRenderLiterals(utils.stringifyNonString(trigger.condition), triggerContext, task);
          logger.debug('%s', utils.stringifyVariable('triggerCondition', triggerCondition));
          // resolve params as object
          const triggerParams = rsmlParseStringAsObject(trigger.params, triggerContext, task);
          logger.debug('%s', utils.stringifyVariable('triggerParams', triggerParams));
          // evaluate the condition using template logic
          const triggerResult = rsmlRenderTemplate(triggerCondition, triggerContext, task, env);
          logger.debug('%s', utils.stringifyVariable('triggerResult', triggerResult));
          // process trigger afterOutput repeat and checks if triggerResult is resolved as 'true'
          if (trigger.when === 'beforeOutput' && triggerResult === 'true') {
            // execute the requested action
            switch (trigger.action) {
              // repeat
              case 'repeat':
                await delay(Number(triggerParams.delay) * 1000);
                return await this.makeStoppable(this.rsmlTaskRepeatExecute(rsml, task, taskList, context));
              // fail
              case 'fail':
                throw (new rsmlerror.TriggerError.FailRequested({
                  message: triggerParams.message,
                  taskName: task
                }));
            } // switch (trigger.action)
          } // if (trigger.when === 'afterOutput' && triggerResult === 'true')
        } // for (const trigger of triggers)
      } // if ('triggers' in taskList[httpMethod]
    }

    logger.addContext('taskId', '');
    // avoid to explore other keys inside the task definition
    return response;
  }

  async rsmlTaskExecute (rsml, taskName, taskNode, context) {
    logger.addContext('taskId', taskName);
    // http://, web+risor://
    let taskUrl = taskNode.url;
    logger.debug('initial %s', utils.stringifyVariable('taskUrl', taskUrl));

    // protocol, host, pathname, ...
    const taskParsedUrl = url.parse(taskNode.url); //eslint-disable-line

    if ('protocol' in taskParsedUrl && taskParsedUrl.protocol.startsWith('web+')) {
      // get 'foo' from 'web+foo'
      const providerName = (taskParsedUrl.protocol.match(/^web\+([^:]*):?$/))[1];
      // get the provider url
      const providerUri = config.providers[providerName].uri;
      // get the task url without protocol
      const taskUrlWithoutProtocol = taskUrl.slice((taskParsedUrl.protocol + '//').length);
      // finally resolve the url: web+foo://bar/zaz becomes http://foo-uri/bar/zaz
      taskUrl = urljoin(url.format(providerUri), taskUrlWithoutProtocol);
    }

    logger.debug('resolved %s', utils.stringifyVariable('taskUrl', taskUrl));

    // second item MUST always be the http method
    const httpMethod = Object.keys(taskNode)[1];
    logger.debug('%s', utils.stringifyVariable('httpMethod', httpMethod));

    // the current httpResource
    const httpResource = taskNode[httpMethod];
    logger.debug('%s', utils.stringifyVariable('httpResource', httpResource));

    // request data to pass to the underline rest library
    const httpRequest = httpResource.request;
    logger.debug('initial %s', utils.stringifyVariable('httpRequest', httpRequest));

    // update request with context's variables
    expandObjectVariables(httpRequest, context, taskName);
    logger.debug('expanded %s', utils.stringifyVariable('httpRequest', httpRequest));

    // format the target task url
    let resourceUrl = rsmlRenderLiterals(querystring.unescape(taskUrl), context, taskName);
    logger.debug('renderLiterals %s', utils.stringifyVariable('resourceUrl', resourceUrl));

    // expand "{{ }}" variables on url task (eg {{ params.inputs.datasetId }})
    resourceUrl = rsmlRenderTemplate(resourceUrl, context, taskName, env);
    logger.debug('renderTemplate %s', utils.stringifyVariable('resourceUrl', resourceUrl));

    // task '/foo' becomes 'foo'
    const idTask = taskName.substring(1);

    // create a short non-sequential url-friendly unique id for this execution
    context[idTask].self.uid = shortid.generate();

    // merge the task defintion and then generate a MD5 hash
    context[idTask].self.hash = hash.MD5({ ...rsml[taskName], ...httpRequest, ...httpMethod, ...resourceUrl });

    // build base request(method, uri)
    const Request = RsmlRequest(httpMethod, resourceUrl, { signal: this.stopper.signal });

    // update Request according to the httpRequest definition
    Object.keys(httpRequest).forEach(function (f) {
      // this looks for any property (`f`) in httpRequest (`r`), like
      // `send`, which is also a unirest method (`Request[f]`), and
      // assigns its value (`r[f]`) to the unirest instance
      if (Object.prototype.hasOwnProperty.call(Request, f)) {
        const value = httpRequest[f];
        // if value is an array, convert it to an argument list
        if (Array.isArray(value)) {
          Request[f](...value);
        } else {
          Request[f](value);
        }
      }
    });

    logger.addContext('taskId', '');

    // return a promise
    return Request.build();
  }
}

// Builder pattern for creating request objects with fluent interface
// Inspired by https://github.com/Kong/unirest-nodejs/blob/master/index.js
function RsmlRequest (method, url, options) {
  const $this = {
    _url: url,
    _options: { method, headers: {}, ...options },
    type: function (value) {
      const typeVal = value.includes('/') ? value : mime.getType(value);
      $this._options.headers['Content-Type'] = typeVal;
      return $this;
    },
    send: function (data) {
      const type = $this._options.headers['Content-Type'];
      if (is(data).a(Object) || is(data).a(Array)) { // do json by default, support only form and json
        if (!type) {
          $this.type('json');
          $this._options.body = JSON.stringify(data);
        } else if (!type.includes('json')) {
          $this._options.body = new URLSearchParams(data);
        } else {
          $this._options.body = JSON.stringify(data);
        }
      } else if (is(data).a(String)) { //
        if (!type) {
          $this.type('form');
        }
        $this._options.body = $this._options.body ? $this._options.body + '&' + data : data;
      } else {
        $this._options.body = data;
      }
      return $this;
    },
    query: function (value) {
      $this._query = value;
      return $this;
    },
    headers (value) {
      $this._options.headers = { ...$this._options.headers, ...value };
      return $this;
    },
    attach (name, value) {
      if (!$this._options.body || !is($this._options.body).a(FormData)) {
        $this._options.body = new FormData();
      }
      $this._options.body.append(name, fs.createReadStream(path.resolve(value)));
      $this._options.headers['Content-Type'] = 'multipart/form-data; boundary=' + $this._options.body.getBoundary();
      return $this;
    },
    build: function () {
      const url = new URL($this._url);
      if ($this._query) {
        Object.entries($this._query).forEach(([key, value]) => {
          url.searchParams.append(key, value);
        });
      }
      const _fetch = fetch(
        url,
        $this._options
      );
      return _fetch.then(async (response) => {
        response.req = {
          method: $this._options.method,
          path: url.toString()
        };
        response.parsed_body = await response.json();
        response.raw_body = JSON.stringify(response.parsed_body);
        return response;
      });
    }
  };
  return $this;
}

function is (value) {
  return {
    a: function (check) {
      if (check.prototype) check = check.prototype.constructor.name;
      const type = Object.prototype.toString.call(value).slice(8, -1).toLowerCase();
      return value != null && type === check.toLowerCase();
    }
  };
}

module.exports = {
  models_path: path.join(__dirname, 'models'),
  RsmlEngine
};
