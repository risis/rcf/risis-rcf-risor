/*!
 * RSML Engine
 * Copyright(c) 2018-2023 Cogniteva SAS
 */
'use strict';

/*!
 * Checks for RSML
 */

/**
 * Module dependencies.
 * @private
 */
const utils = require('../utils');

/**
 * Class representing a check. A check is used to define and perform checks on an
 * arbitrary object
 *
 * Operators for checks and groups of checks
 *
 * `oneOf` – requires exactly one of checks passed
 * `allOf` – requires all the checks passed
 * `anyOf` – requires one or more of the checks passed
 * `not`  - negate operator
 *
 * @class
 */
class CustomChecker {
  /**
   * Create a Check.
   * @param {Object} options - The options object.
   * @param {string} options.title - The title of the check.
   * @param {string} options.message - The message of the check.
   * @param {string} options.context - The context of the check.
   * @param {Object} params - The params object.
   * @param {Object} params.dependencies - The dependencies of the check.
   * @param {Object} params.groups - The groups of the check.
   */
  constructor (options, params, paramsDefaults = {}) {
    this.options = options;
    this.params = utils.setDefaults(params,
      utils.setDefaults(paramsDefaults, {
        checkDependencies: false,
        dependencies: [],
        groups: []
      }));
  }

  /**
   * Run the check.
   * @returns {Promise<boolean>} - A promise that resolve with a boolean or Error
   *                               indicating the result of the check.
   */
  async run () {
    // Check dependencies
    if (this.params.dependencies.length > 0) {
      let result;
      for (const { dependency, operator } of this.params.dependencies) {
        switch (operator) {
          case 'allOf':
            result = await dependency.run();
            if (utils.isFalseOrError(result)) {
              return result;
            }
            break;
          case 'oneOf':
            result = await dependency.run();
            if (utils.isTrueAndNotError(result)) {
              let onePassed = true;
              for (const { dep, op } of this.params.dependencies) {
                if (dep !== dependency && op === 'oneOf' &&
                  (utils.isTrueAndNotError(await dep.run()))) {
                  onePassed = false;
                }
              }
              if (onePassed) {
                return result;
              }
            }
            break;
          case 'anyOf':
            result = await dependency.run();
            if (utils.isTrueAndNotError(result)) {
              return result;
            }
            break;
          case 'not':
            result = await dependency.run();
            if (!(utils.isTrueAndNotError(result))) {
              return true;
            }
            break;
          default:
            throw new Error(`Invalid operator: ${operator}`);
        }
      }
    }

    // Check groups
    if (this.params.groups.length > 0) {
      for (const { group, operator } of this.params.groups) {
        let passedGroupChecks = 0;
        for (const dependency of group) {
          if (utils.isTrueAndNotError(await dependency.run())) {
            passedGroupChecks++;
          }
        }
        switch (operator) {
          case 'allOf':
            if (passedGroupChecks !== group.length) {
              return false;
            }
            break;
          case 'oneOf':
            if (passedGroupChecks !== 1) {
              return false;
            }
            break;
          case 'anyOf':
            if (passedGroupChecks === 0) {
              return false;
            }
            break;
          case 'not':
            if (passedGroupChecks === group.length) {
              return false;
            }
            break;
          default:
            throw new Error(`Invalid operator: ${operator}`);
        }
      }
    }

    // Perform the check
    return this._performCheck();
  }

  /**
   * Add a dependency to the check.
   * @param {CustomChecker} dependency - The check that this check depends on.
   * @param {string} operator - The operator that defines the relationship
   *                            between the checks (e.g. 'oneOf', 'allOf',
   *                            'anyOf', 'not'). Default is 'allOf'.
   * @return {CustomChecker} - The check.
   */
  addDependency (dependency, operator = 'allOf') {
    this.params.dependencies.push({ dependency, operator });
  }

  /**
   * Add a group of dependencies for this check.
   * @param {Array<CustomChecker>} dependencies - The checks that this check depends on.
   * @param {string} operator - The operator that defines the relationship
   *                             between the checks (e.g. 'oneOf', 'allOf',
   *                             'anyOf', 'not'). Default is 'allOf'.
   * @return {CustomChecker} - The check.
   */
  groupDependencies (dependencies, operator = 'allOf') {
    this.params.groups.push({ dependencies, operator });
  }

  /**
    The _performCheck method is intended to be overridden by child classes to
    provide the specific implementation of the check. It should return a
    boolean value indicating whether the check passed or failed.
    @abstract
    @return {boolean} - Result of the check
    @throws {Error} - This method should throw an error if not overridden by
                      a child class.
    */
  _performCheck () {
    throw new Error('_performCheck method MUST be implemented by a child class');
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = CustomChecker;
