/*!
 * RSML Engine
 * Copyright(c) 2018-2023 Cogniteva SAS
 */
'use strict';
const fs = require('fs');
const CustomChecker = require('./custom-checker');
const fileerror = require('../custom-error/file-error');
const utils = require('../utils');

/*!
 * Check for Files
 */

/**
 * Class representing a checker for checking if a file exists
 * @extends CustomChecker
 */
class FileExistsChecker extends CustomChecker {
  /**
   * Creates an instance of FileExistsChecker
   * @param {string} filePath - The file path to check
   * @param {object} options - options object
   * @param {string} options.title - title for error
   * @param {string} options.message - message for error
   * @param {string} options.error - error instance to throw
   * @param {Object} [params={}] - Additional params for the checker
   * @param {Object} [paramsDefaults={}] - Default params for the checker
   */
  constructor (filePath, options = {}, params = {}, paramsDefaults = {}) {
    super(options, params, paramsDefaults);
    this._filePath = filePath;
  }

  /**
   * Perform the check
   * @private
   * @return {Promise<boolean>} - Result of the check
   */
  async _performCheck () {
    try {
      const stats = await fs.promises.stat(this._filePath);
      return stats.isFile();
    } catch (err) {
      if (err.code === 'ENOENT') {
        this.options.context = err;
        return new fileerror.NotFound(this.options);
      }
      throw err;
    }
  }
}

/**
 * Check if a file exists
 *
 * @param {string} filePath - The file path to check if exists
 * @param {object} options - options object
 * @param {string} options.title - title for error
 * @param {string} options.message - message for error
 * @param {string} options.error - error instance to throw
 * @param {Object} [params={}] - Optional params for the checker
 * @param {Object} [paramsDefaults={}] - Optional defaults for the params
 *
 * @return {FileExistsChecker} - A new instance of the FileExistsChecker class
 */
const fileExists = (filePath, options = {}, params = {}, paramsDefaults = {}) => {
  return new FileExistsChecker(filePath, options, params, paramsDefaults);
};

/**
 * Class representing a checker to read a file synchronously and check for errors.
 * @extends CustomChecker
 */
class FileReadSyncChecker extends CustomChecker {
  /**
   * @param {string} filePath - The path to the file that should be read.
   * @param {object} options - options object
   * @param {string} options.title - title for error
   * @param {string} options.message - message for error
   * @param {string} options.error - error instance to throw
   */
  constructor (filePath, options, params = {}, paramsDefaults = {}) {
    super(options, params, paramsDefaults);
    this._filePath = filePath;
    if (this.params.checkDependencies) {
      this.fileExistsChecker = new FileExistsChecker(filePath);
      this.addDependency(this.fileExistsChecker, 'allOf');
    }
  }

  /**
   * Perform the check
   * @private
   * @return {Promise<boolean>} - Result of the check
   */
  async _performCheck () {
    try {
      const result = await fs.promises.readFile(this._filePath, this.params);
      if (utils.isNotString(result)) {
        return new fileerror.Encoding();
      }
      return result;
    } catch (err) {
      this.options.context = err;
      return new fileerror.Read(this.options);
    }
  }
}

/**
 * Read a file synchronously and check for errors
 *
 * @param {string} filePath - The file path to check if exists
 * @param {object} options - options object
 * @param {string} options.title - title for error
 * @param {string} options.message - message for error
 * @param {string} options.error - error instance to throw
 * @param {Object} [params={}] - Optional params for the checker
 * @param {Object} [paramsDefaults={}] - Optional defaults for the params
 *
 * @return {FileReadSyncChecker} - A new instance of the FileReadSyncChecker class
 */
const fileReadSync = (filePath, options = {},
  params, paramsDefaults = { encoding: 'utf8' }) => {
  return new FileReadSyncChecker(filePath, options, params, paramsDefaults);
};

/**
 * Module exports.
 * @public
 */
module.exports = { fileExists, fileReadSync };
