/*!
 * RSML Engine
 * Copyright(c) 2018-2023 Cogniteva SAS
 */
'use strict';

// utils is a library of generic helper functions non-specific to Nucleus

/**
 * Module dependencies.
 * @private
 */
const _extend = require('extend');
const _merge = require('lodash.merge');
const _defaults = require('lodash.defaults');
const semver = require('semver');
const _clone = require('lodash.clone');
const log4js = require('log4js');
const logger = log4js.getLogger('rsml-engine');

/**
 * Convert a string in CamelCase to a sentence case string separated by spaces
 * @param {string} [str=''] - The string to convert.
 * @returns {string} The converted string.
 */
function toSentenceCase (str = '') {
  if (!str) return '';

  const textcase = String(str)
    .replace(/^[^A-Za-z0-9]*|[^A-Za-z0-9]*$/g, '')
    .replace(/([a-z])([A-Z])/g, (m, a, b) => `${a}_${b.toLowerCase()}`)
    .replace(/[^A-Za-z0-9]+|_+/g, ' ')
    .toLowerCase();

  return textcase.charAt(0).toUpperCase() + textcase.slice(1);
}

/**
 * Removes the first occurrence of a character from the start of a string.
 * @param {string} str The string to be modified.
 * @param {string} chr The character to remove.
 * @return {string} The modified string.
 */
function stripChar (str, chr) {
  if (!str) return '';
  // ensure str doesn't begin with '/'
  while (str.charAt(0) === chr) {
    str = str.substring(1);
  }
  return str;
}

/**
 * Removes whitespace from the beginning and end of a string.
 * @param {string} str - The string to trim.
 * @returns {string} The trimmed string.
 */
function trim (str) {
  if (typeof str.trim === 'function') {
    return str.trim();
  } else {
    return str.replace(/^\s+|\s+$/g, '');
  }
}

/**
 * setDefaults - merges the properties of two objects, with the properties of
 * the second object taking precedence over the first.
 * @param {Object} options - The object whose properties will be merged.
 * @param {Object} defaults - The object whose properties will be used as
 *                            the default values
 * @returns {Object} - A new object with the properties of the first object
 *                     merged with the properties of the second object.
*/
function setDefaults (options, defaults) {
  return _defaults({}, _clone(options), defaults);
}

/**
 * Handle the result of a check.
 * @param {Object} result - The result of a check.
 * @throws {Error} - If the result is an instance of Error.
 * @returns {Object} - The result passed as argument.
 */
function throwIfError (result) {
  if (result instanceof Error) {
    if (typeof result.toJSON === 'function') {
      logger.error(result.toJSON());
    } else if (result.message !== undefined) {
      logger.error(result.message);
    } else {
      logger.error(result);
    }
    throw result;
  }

  return result;
}

/**
 * isError - check if an object is an instance of the Error class
 * @param {*} result - The object to check
 * @returns {Boolean} - true if the object is an instance of the Error class,
 *                      false otherwise.
 */
function isError (result) {
  return result instanceof Error;
}

/**
 * isNotError - check if an object is not an instance of the Error class
 * @param {*} result - The object to check
 * @returns {Boolean} - true if the object is not an instance of the Error class,
 *                      false otherwise.
 */
function isNotError (result) {
  return !(isError(result));
}

/**
 * isFalseOrError - check if an object is either false or an instance of the
 * Error class
 * @param {*} result - The object to check
 * @returns {Boolean} - true if the object is either false or an instance of
 *                      the Error class, false otherwise.
 */
function isFalseOrError (result) {
  return !result || isError(result);
}

/**
 * isTrueAndNotError - check if an object is both true and not an instance of
 * the Error class
 * @param {*} result - The object to check
 * @returns {Boolean} - true if the object is both true and not an instance of
 *                      the Error class, false otherwise.
 */
function isTrueAndNotError (result) {
  return result && isNotError(result);
}

/**
 * isString - check if an object is a string
 * @param {*} str - The object to check
 * @returns {Boolean} - true if the object is a string, false otherwise.
 * @see https://stackoverflow.com/a/9436948
 */
function isString (str) {
  return (typeof str === 'string' || str instanceof String);
}

/**
 * isNotString - check if an object is not a string
 * @param {*} str - The object to check
 * @returns {Boolean} - true if the object is not a string, false otherwise.
 */
function isNotString (str) {
  return !isString(str);
}

/**
 * Converts any input that is not a string to a string representation.
 * @param {any} obj - The input value to be converted.
 * @param {Function} [replacer = null] - A replacer function that alters the
 * behavior of the stringification.
 * @return {string} - The string representation of the input value,
 * or the input value if it is a string.
 */
function stringifyNonString (obj, replacer = null) {
  if (!(isString(obj))) {
    return JSON.stringify(obj, replacer);
  } else {
    return obj;
  }
}

/**
 * Logs the variable name and its value
 * @function
 * @param {string} variableName - The name of the variable
 * @param {any} variableValue - The value of the variable
 * @returns {string} - A string representing the variable name and its value in the format 'variableName = variableValue'
 */
function stringifyVariable (variableName, variableValue) {
  return `${variableName} = ${stringifyNonString(variableValue)}`;
}

/**
 * Module exports.
 * @public
 */
/* eslint object-shorthand: [2, "consistent"] */
module.exports = {
  // deepMerge: require('lodash.merge'),
  _extend: _extend,
  // forEach: axiosUtils.forEach,
  // isArray: axiosUtils.isArray,
  // isBlob: axiosUtils.isBlob,
  // isDate: axiosUtils.isDate,
  // isFile: axiosUtils.isFile,
  // isFunction: axiosUtils.isFunction,
  // isNumber: axiosUtils.isNumber,
  // isObject: axiosUtils.isObject,
  // isString: axiosUtils.isString,
  // isUndefined: axiosUtils.isUndefined,
  _merge: _merge,
  semver: semver,
  _clone: _clone,
  setDefaults: setDefaults,
  stripChar: stripChar,
  toSentenceCase: toSentenceCase,
  throwIfError: throwIfError,
  isError: isError,
  isNotError: isNotError,
  isFalseOrError: isFalseOrError,
  isTrueAndNotError: isTrueAndNotError,
  isString: isString,
  isNotString: isNotString,
  stringifyNonString: stringifyNonString,
  trim: trim,
  stringifyVariable: stringifyVariable
};
