#%RSML 0.7

title: CInnoB extraction
name: cinnob-extraction
version : 0.1.0
description: CInnoB scenario for testing risis2 CInnoB dataset API.

date:
  creation: '2023-01-26'

owners:
  - rcf-team

options:
  followRedirect: true

response:
  outputs:
  - type: apps
    title: CInnoB extraction
    description: "CInnoB extraction"
    main: false
    uri: '{{ params.datastore_public_uri }}/loginpage.xhtml?redirectPage=/dataset.xhtml?persistentId={{createDataset.datasetId}}&silentAutoLogin=true'

inputs:
  cinnobInfo:
    title: 'Cinnob info'
    members:
      firm_register_id:
        title: 'Firm register id'
        description: 'Firm register id'
        hint: 'Firm register id with structure F_XX000000000'
        type: string+select+multiple+text
        validate:
          - $each:
              regex: '/^F_[A-Z]{2}[0-9]{9}$/'

      year:
        title: 'Year'
        description: 'Year interval to analyse'
        hint: 'Select the range of years that you want to analyze'
        type: string+select+multiple+int
        validate:
          - $each:
              between:
                - 2000
                - 2020

      firm_country:
        title: 'ISO2 country'
        description: 'ISO2 country'
        default: ''
        type: string+select+multiple+text
        hint: 'Add multiples iso countries like FR'
        validate:
          - $each:
              regex: '/^[A-Z]{2}$/'

      firm_sector_nace_3digits:
        title: 'Firm sector nace3'
        description: 'Firm sector nace3'
        hint: '0-99 is the range of all the sectors existing in the database'
        type: string+range
        mode: simple
        options:
          min: 0
          max: 99
          step: 0.1
        default:
          min: 0
          max: 99

      cinnob_query_limit:
        title: 'Maximal number of records to return'
        type: string+inputmask+int
        mode: simple
        hint: 'Use this field to constrains the number of records returned'
        description: >
          Upper limit on the number of records returned by the search criteria
        docs: '#cinnob-query-limit'
        default: '1000'

      dataset_title:
        title: 'Dataset title'
        type: string+inputmask+text
        mode: simple
        hint: 'Enter the title for the new dataset'
        description: >
          Enter a title for the dataset to be created. This title will help to
          identify and use the data in projects
        docs: '#dataset-title'
        default: 'My CinnoB Dataset'
        required: true

      datastore_user_email:
        title: 'User e-mail'
        type: string+inputmask+text
        mode: simple
        placeholder: 'example@domain.com'
        hint: "Enter user's email to share the extraction with"
        description: >
          Enter the email address of the user to grant access to the generated
          extraction. Leave blank if you don't want to share it.
        docs: '#user-email'
        default: ''
        required: false
  domainInfo:
    title: 'Domain info'
    members:
      trademarks_patents:
        title: 'Trademarks and Patents'
        description: 'Domain'
        hint: 'Filtering only on trademarks and patents table'
        type: string+select+multiple
        options:
          values:
            - 'Electrical engineering'
            - 'Instruments'
            - 'Chemistry'
            - 'Mechanical engineering'
            - 'Other fields'
            - 'UNKNOWN'
        default: ''
      cwts:
        title: 'cwts'
        description: 'Domain'
        hint: 'Filtering only on cwts table'
        type: string+select+multiple
        options:
          values:
            - 'Eng_Tech'
            - 'Nat_Sci'
            - 'Soc_Sci'
            - 'Hum'
            - 'Med_Hlth_Sci'
            - 'Agric_Sci'
        default: ''
      top_fos_hierarchy:
        title: 'Top fos hierarchy'
        description: 'Domain'
        hint: 'Filtering only on Eupro table'
        type: string+select+multiple
        options:
          values:
            - '21'
            - '23'
            - '25'
            - '27'
            - '29'
            - '31'
        default: ''


/cinnob-extraction:
  - /extractFirms:
  - /extractFirmsCwtsYear:
  - /extractFirmsCwtsYearDomain:
  - /extractFirmsEuproYear:
  - /extractFirmsEuproYearDomain:
  - /extractFirmsFinancialYear:
  - /extractFirmsTrademarksYear:
  - /extractFirmsTrademarksYearDomain:
  - /extractFirmsPatentsYear:
  - /extractFirmsPatentsYearDomain:
  - /transformCinnobInfoToCsv:
  - /createDataset:
  - /uploadCinnobInfoFilesToDataset:
  - /importDatasetIntoRibaProject:
  - /assignUserRole:

/extractFirms:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firms",
            "query": {
              "firm_register_id.in": "{{ params.inputs.cinnobInfo.firm_register_id }}",
              "firm_country.in": "{{ params.inputs.cinnobInfo.firm_country }}",
              "firm_sector_nace_3digits.min": '{{ params.inputs.cinnobInfo.firm_sector_nace_3digits.min }}',
              "firm_sector_nace_3digits.max": '{{ params.inputs.cinnobInfo.firm_sector_nace_3digits.max }}',
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsCwtsYear:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firms_cwts_data_per_year",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsCwtsYearDomain:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firms_cwts_data_per_year_per_domain",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "fos_domain.in": "{{params.inputs.domainInfo.cwts}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsEuproYear:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firms_eupro_data_per_year",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsEuproYearDomain:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firms_eupro_data_per_year_per_domain",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "top_fos_hierarchy.in": "{{params.inputs.domainInfo.top_fos_hierarchy}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsFinancialYear:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firms_financial_data_per_year",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsTrademarksYear:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firms_trademarks_data_per_year",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsTrademarksYearDomain:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firms_trademarks_data_per_year_per_domain",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "tms_domain_label.in": "{{params.inputs.domainInfo.trademarks_patents}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsPatentsYear:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firm_patents_per_year",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/extractFirmsPatentsYearDomain:
  url: web+risor://risis-datasets/cinnob/v1.1/fetch
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "api": {
            "endpoint": "/entities/firm_patents_per_year_per_domain",
            "query": {
              "firm_register_id.in": "{{ extractFirms.firmsExtraction }}",
              "year.in": "{{params.inputs.cinnobInfo.year}}",
              "patent_technology_domain_label.in": "{{params.inputs.domainInfo.trademarks_patents}}",
              "limit": "{{ params.inputs.cinnobInfo.cinnob_query_limit }}"
            }
          }
        }

/transformCinnobInfoToCsv:
  url: web+risor://risis-datasets/cinnob/transform
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
         "filename": [
          "cinnob_firms",
          "cinnob_firms_cwts_data_per_year",
          "cinnob_firms_cwts_data_per_year_per_domain",
          "cinnob_firms_eupro_data_per_year",
          "cinnob_firms_eupro_data_per_year_per_domain",
          "cinnob_firms_financial_data_per_year",
          "cinnob_firms_trademarks_data_per_year",
          "cinnob_firms_trademarks_data_per_year_per_domain",
          "cinnob_firm_patents_per_year",
          "cinnob_firm_patents_per_year_per_domain",
         ],
          "type": "file",
          "from": "json",
          "to": "tsv",
          "options": [
            {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "firm_name",
                "firm_simplified_name",
                "firm_incorporation_year",
                "firm_country",
                "firm_sector_nace_2digits",
                "firm_sector_nace_3digits",
                "firm_sector_nace_4digits",
                "firm_quotation"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "nb_publications",
                "nb_collaboration",
                "nb_int_collaboration",
                "nb_higher_education_partners",
                "nb_pro_partners",
                "nb_hcp",
                "mean_ncs",
                "nb_not_cited",
                "nb_open_access",
                "nb_eu_partner",
                "nb_same_co_parent"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "fos_domain",
                "nb_publications",
                "nb_collaboration",
                "nb_int_collaboration",
                "nb_higher_education_partners",
                "nb_pro_partners",
                "nb_hcp",
                "mean_ncs",
                "nb_not_cited",
                "nb_open_access",
                "nb_eu_partner",
                "nb_same_co_parent"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "received_funding_volume",
                "nb_active_projects",
                "nb_coordinator_projects",
                "nb_projects_with_higher_education_partners",
                "nb_projects_with_pro_partners",
                "nb_projects_with_public_research_partners"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "top_fos_hierarchy",
                "received_funding_volume",
                "nb_active_projects",
                "nb_coordinator_projects",
                "nb_projects_with_higher_education_partners",
                "nb_projects_with_pro_partners",
                "nb_projects_with_public_research_partners"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "firm_turnover",
                "firm_total_assets",
                "firm_number_employees",
                "firm_return_on_equity",
                "firm_return_on_assets",
                "firm_debt_to_equity",
                "firm_liquidity_ratio",
                "firm_RD_investment",
                "firm_market_capitalisation"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "nb_active_euipo_tms",
                "nb_euipo_tms_all",
                "nb_euipo_tms_goods",
                "nb_euipo_tms_services",
                "nb_euipo_tms_mixed",
                "avg_tm_breadth",
                "nb_applns_firm_home_based",
                "nb_applns_eu_based"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "tms_domain_code",
                "tms_domain_label",
                "nb_active_euipo_tms",
                "nb_euipo_tms_all",
                "nb_euipo_tms_goods",
                "nb_euipo_tms_services",
                "nb_euipo_tms_mixed",
                "avg_tm_breadth",
                "nb_applns_firm_home_based",
                "nb_applns_eu_based"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "nb_priority_patents",
                "nb_granted_priority_patents",
                "nb_priority_patents_with_ep_family",
                "nb_priority_patents_with_us_family",
                "nb_priority_patents_with_pct_family",
                "nb_priority_patents_with_2ip5_family",
                "nb_top_10_cited_patents",
                "nb_academic_patents",
                "nb_firm_home_based_patents",
                "nb_eu_based_patents",
                "nb_patents_with_int_collab",
                "nb_patents_with_industry_collab",
                "nb_patents_with_univ_collab",
                "nb_patents_with_ror_collab",
                "nb_patents_with_orgreg_collab"
              ]
            }, {
              "delimiter": "\t",
              "fields": [
                "firm_register_id",
                "year",
                "patent_technology_domain_code",
                "patent_technology_domain_label",
                "nb_priority_patents",
                "nb_granted_priority_patents",
                "nb_priority_patents_with_ep_family",
                "nb_priority_patents_with_us_family",
                "nb_priority_patents_with_pct_family",
                "nb_priority_patents_with_2ip5_family",
                "nb_top_10_cited_patents",
                "nb_academic_patents",
                "nb_firm_home_based_patents",
                "nb_eu_based_patents",
                "nb_patents_with_int_collab",
                "nb_patents_with_industry_collab",
                "nb_patents_with_univ_collab",
                "nb_patents_with_ror_collab",
              ]
            }
          ],
          "file": [
            "{{extractFirms.folder}}/{{extractFirms.filename}}",
            "{{extractFirmsCwtsYear.folder}}/{{extractFirmsCwtsYear.filename}}",
            "{{extractFirmsCwtsYearDomain.folder}}/{{extractFirmsCwtsYearDomain.filename}}",
            "{{extractFirmsEuproYear.folder}}/{{extractFirmsEuproYear.filename}}",
            "{{extractFirmsEuproYearDomain.folder}}/{{extractFirmsEuproYearDomain.filename}}",
            "{{extractFirmsFinancialYear.folder}}/{{extractFirmsFinancialYear.filename}}",
            "{{extractFirmsTrademarksYear.folder}}/{{extractFirmsTrademarksYear.filename}}",
            "{{extractFirmsTrademarksYearDomain.folder}}/{{extractFirmsTrademarksYearDomain.filename}}",
            "{{extractFirmsPatentsYear.folder}}/{{extractFirmsPatentsYear.filename}}",
            "{{extractFirmsPatentsYearDomain.folder}}/{{extractFirmsPatentsYearDomain.filename}}"
          ]
        }

/createDataset:
  url: "web+risor://datastore/datasets?service_provider_key=${context.datastore.token}"
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query:  '{ "dataverseId": "users-workspace" }'
      type: 'json'
      send: '{% include "cinnob-extraction/include/dataset-info.json" %}'
    output: '{ "datasetId": "{{data.persistentId}}" }'

/uploadCinnobInfoFilesToDataset:
  url: "web+risor://datastore/datasets/files?service_provider_key=${context.datastore.token}"
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "datasetId": "${createDataset.datasetId}",
          "files": "{{transformCinnobInfoToCsv}}"
        }

/importDatasetIntoRibaProject:
  url: "web+risor://riba/projects/${params.inputs.project-id}/datasets/{{ createDataset.datasetId | urlEncode }}"
  PUT:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query:  ''

/assignUserRole:
  url: web+risor://datastore/admin/roles/assign?service_provider_key=${context.datastore.token}
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "email": "{{params.inputs.cinnobInfo.datastore_user_email}}",
          "role": "member",
          "datasetId": "${createDataset.datasetId}"
        }
