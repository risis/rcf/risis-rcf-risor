#/bin/sh

# script for monitoring and auto upload scenarios files do m4 storage

echo "[autodeploy-scenarios.sh] start"

trap "echo '[autodeploy-scenarios.sh] exit' && exit" SIGINT

echo "[autodeploy-scenarios.sh] monitoring changes on scenarios files"
mc watch --recursive --events put,delete /scenarios > /mc-watch.log &

echo "[autodeploy-scenarios.sh] setting RCF minio credentials"
mc alias set rcf-storage $MINIO_URL $MINIO_ACCESS_KEY $MINIO_SECRET_KEY

echo "[autodeploy-scenarios.sh] upload the scenarios files on startup to $MINIO_URL/$SCENARIOS_PATH"
mc cp --recursive /scenarios/ rcf-storage/$SCENARIOS_PATH

while true; do
  NLINES=$(wc -c /mc-watch.log | awk '{ print $1 }')
  if [ "$NLINES" -gt "0" ]; then
    echo "[autodeploy-scenarios.sh] file changes detected, uploading files to $MINIO_URL/$SCENARIOS_PATH"
    mc cp --recursive /scenarios/ rcf-storage/$SCENARIOS_PATH
    truncate -s 0 /mc-watch.log
  fi
  sleep 2
done

echo "[autodeploy-scenarios.sh] finish"
