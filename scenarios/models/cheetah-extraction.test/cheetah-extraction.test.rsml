#%RSML 0.6

title: TEST CHEETAH extraction
name: cheetah-extraction.test
version : 0.1.0
description: CHEETAH scenario for testing risis2 cheetah dataset API. It fetches the first 10 instances for each endpoint called, filtering the input year.

date:
  creation: '2022-03-29'

owners:
  - rcf-team

options:
  followRedirect: true

inputs:
  country_code:
    title: 'Country code for all entities'
    description: 'Filter results by country code'
    type: string+inputmask+text
    default: 'IT'
  announced_year:
    title: 'Announced year for entity Main'
    description: 'Filter announced year'
    type: string+inputmask+text
    default: '2010'
  completed_year:
    title: 'Completed year for entity Main'
    description: 'Filter by completed year'
    type: string+inputmask+text
    default: '2010'

/cheetah-extraction.test:
  - /risisDatasetsCheetahMetadata:
  - /risisDatasetsCheetahEntityTypes:
  - /risisDatasetsCheetahEntitiesMain:
  - /risisDatasetsCheetahEntitiesM_A:
  - /risisDatasetsCheetahEntitiesOwnership:

/risisDatasetsCheetahMetadata:
  url: web+risor://risis-datasets/cheetah/v1.1/metadata
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query: ''
    output: '{ }'

/risisDatasetsCheetahEntityTypes:
  url: web+risor://risis-datasets/cheetah/v1.1/entityTypes
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query: ''
    output: '{ }'

/risisDatasetsCheetahEntitiesMain:
  url: web+risor://risis-datasets/cheetah/v1.1/entities/Main
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query: >
        {
          "country_code": "{{ params.inputs.country_code }}",
          "limit": "10"
        }
    output: '{ }'

/risisDatasetsCheetahEntitiesM_A:
  url: web+risor://risis-datasets/cheetah/v1.1/entities/M_A
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query: >
        {
          "t_country_code": "{{ params.inputs.country_code }}",
          "announced_year": "{{ params.inputs.announced_year }}",
          "completed_year": "{{ params.inputs.completed_year }}",
          "limit": "10"
        }
    output: '{ }'

/risisDatasetsCheetahEntitiesOwnership:
  url: web+risor://risis-datasets/cheetah/v1.1/entities/Ownership
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query: >
        {
          "s_country_code": "{{ params.inputs.country_code }}",
          "limit": "10"
        }
    output: '{ }'
