#%RSML 0.6

title: Transform CSV file to Cortext format
name: transform-csv-to-cortext-format
version : 0.1.0
description: >
  Transform a CSV file that may contain multiple rows for a specific field value into grouped values 
  based on that field, using the format accepted by Cortext. (https://docs.cortext.net/data-formats/#csv)

date:
  creation: '2023-09-26'

owners:
  - rcf-team

inputs:
  dataset-id:
    title: 'Dataset'
    type: string+dataset-id
    mode: simple
    default: ''
    required: true
  dataset-file-id:
    title: 'File'
    type: string+dataset-file-id
    mode: simple
    default: ''
    file-types:
      - csv
      - tsv
    required: true
  field-to-group-by:
    title: 'Field to group by'
    description: 'Name of the field within your data that will be used to group data based on the values within this field.'
    type: string+dataset-file-field
    source: dataset-file-id
    required: true

response:
  outputs:
    - type: apps
      title: Result file with its format transformed to be compatible with Cortext
      description: "Result file with its format transformed to be compatible with Cortext"
      main: false
      uri: '{{params.datastore_public_uri}}/loginpage.xhtml?redirectPage=/file.xhtml?fileId={{uploadResultFileToDataset.fileId}}&silentAutoLogin=true'
    - type: file-text
      title: "File parsed in Cortext"
      description: "Using the format accepted by Cortext: https://docs.cortext.net/data-formats/#csv"
      main: false
      uri: 'https://assets.cortext.net/docs/{{getCorpusCorText.corpusHash}}'

      
/transform-csv-to-cortext-format:
  - /downloadFileFromDataset:
  - /transformCsvToCortextFormat:
  - /uploadResultFileToDataset: 
  - /uploadToCorText:
  - /parseOnCorText:

/downloadFileFromDataset:
  url: web+risor://datastore/datasets/files/${params.inputs.dataset-file-id}/import?service_provider_key=${context.datastore.token}
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query: '{ "datasetId": "${params.inputs.dataset-id}" }'
    output:
      - folder: "{{folder}}"
      - filename: "{{data.label}}"

/transformCsvToCortextFormat:
  url: web+risor://builtins/transform-csv-to-cortext-format
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: >
        {
          "fileToTransform": "{{downloadFileFromDataset.folder}}/{{downloadFileFromDataset.filename}}",
          "delimiter": "\t", 
          "resultFileName": "{{downloadFileFromDataset.filename.split('.')[0]}}_formatted_for_cortext",
          "fieldToGroupBy": "${params.inputs.field-to-group-by}"
        }

/uploadResultFileToDataset:
  url: "web+risor://datastore/datasets/files?service_provider_key=${context.datastore.token}"
  POST:
    request:
      query:  '{ "datasetId": "${params.inputs.dataset-id}" }'
      headers: '{"Content-Type": "multipart/form-data", "Authorization": "Bearer ${params.inputs.token}"}'
      attach:  [ 'file', '${transformCsvToCortextFormat.folder}/${transformCsvToCortextFormat.filename}' ]
    output: '{ "fileId": "{{data.files[0].dataFile.id}}" }'

/uploadToCorText:
  - /uploadFile:
  - /setMetainfo:

/uploadFile:
  url: web+risor://cortext/files
  POST:
    request:
      headers: '{"Content-Type": "multipart/form-data", "Authorization": "Bearer ${params.inputs.token}"}'
      attach:  [ 'qqfile', '${transformCsvToCortextFormat.folder}/${transformCsvToCortextFormat.filename}' ]
    output: '{ "id": "{{id}}" }'

/setMetainfo:
  url: web+risor://cortext/documents/${uploadFile.id}
  PATCH:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: '{ "datastore-dataset-title": "${transformCsvToCortextFormat.filename}" }'
    output: '{ "id": "{{id}}", "filename":"{{filename}}", "title":"${transformCsvToCortextFormat.filename}"}'

/parseOnCorText:
  - /doParseOnCorText:
  - /getStatusCorText:
    params: '{ "jobid" : "${doParseOnCorText.jobid}" }'
  - /getCorpusCorText:
    params: '{ "resultHash": "${doParseOnCorText.resultsHash}" }'

/doParseOnCorText:
  url: web+risor://cortext/jobs
  POST:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
      send: '{% include "transform-csv-to-cortext-format/include/cortext-parsing-job.json" %}'
    output: '{ "jobid": "{{job_id}}", "resultsHash": "{{resultsHash}}" }'

/getCorpusCorText:
  url: web+risor://cortext/documents/${getCorpusCorText.params.resultHash}
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
    output:  '{ "corpusHash": "{{ files[files.keys()[0]].id }}" }'

/getStatusCorText:
  url: web+risor://cortext/jobs/${getStatusCorText.params.jobid}
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      type: 'json'
    triggers:
      -
        when: 'afterOutput'
        action: 'repeat'
        params: '{ "delay": 5, "maxtries": -1 }'
        condition: '{{ false if ${getStatusCorText.state} in [8,9] else true }}'
      -
        when: 'afterOutput'
        action: 'fail'
        params: '{ "message": "job ${getStatusCorText.params.jobid} ended with the state ${getStatusCorText.state}"  }'
        condition: '{{ true if ${getStatusCorText.state} != 9 else false }}'
    output: '{ "state": "{{this()[0].state}}", "result_url": "{{this()[0].result_url}}" }'

