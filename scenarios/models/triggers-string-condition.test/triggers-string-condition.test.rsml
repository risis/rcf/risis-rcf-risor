#%RSML 0.6

title: TRIGGERS STRING CONDITION test
name: triggers-string-condition.test
version : 0.1.0
description: >
    TRIGGERS STRING CONDITION test to validate RSML engine triggers by string
    condition.
    .
    This scenario must runs successfully, if it fails means RSML engine has a
    bug on triggers conditionals with strings. If it runs on a infinite loop
    also means a bug on RSML engine.
    .
    The expected behavior is: The scenario runs for 5 seconds and finishes
    with success after that time, if it runs in a infinite loop it means a bug
    on the RSML trigger condition below:
    .
    '{{ true if "${wait5SecondsJob.status}" == "running" else false }}'

date:
  creation: '2023-01-19'

owners:
  - rcf-team

options:
  followRedirect: true

/triggers-string-condition.test:
  - /getJobInitialTimestamp:
  - /wait5SecondsJob:

/getJobInitialTimestamp:
  url: web+risor://builtins/test/job
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query: '{ }'

/wait5SecondsJob:
  url: web+risor://builtins/test/job
  GET:
    request:
      headers: '{"Authorization": "Bearer ${params.inputs.token}"}'
      query: '{ "initialTimestamp": "${getJobInitialTimestamp.now}" }'
    triggers:
      -
        when: 'afterOutput'
        action: 'repeat'
        params: '{ "delay": 1, "maxtries": -1 }'
        condition: '{{ true if "${wait5SecondsJob.status}" == "running" else false }}'
