/*!
 * RSML Engine
 * Copyright(c) 2018-2023 Cogniteva SAS
 */

'use strict';

/* eslint-disable no-unused-vars */

/**
 * Module dependencies.
 * @private
 */
const nunjucks = require('nunjucks');
const path = require('path');
const utils = require('./utils');

/**
 * Function filters.
 */

// Converts an object or array to a JSON string and returns a
// SafeString instance. In this case, the resulting JSON string
// will be marked as safe and will not be escaped when it is
// rendered in the template
// Note that {{ foo | json }} is a shorthand for {{ foo | dump | safe }}
const json = (obj) => {
  const jsonString = JSON.stringify(obj, null);
  return new nunjucks.runtime.SafeString(jsonString);
};

// Converts any special characters in str to their corresponding
// percent-encoded representations, so the string can be safely
// included in a URL without being interpreted as special characters
// the list of encoded characters is:
// !,#,$,&,',(,),*,+,/,:,;,=,?,@,[,],,
const urlEncode = (str) => {
  return encodeURIComponent(str);
};

// Filter out from arr the items that match at least one item in
// the list
const pruneList = (arr, items) => {
  const filteredArr = arr.filter(item => !items.includes(item));
  return filteredArr;
};

const extractFileName = (str) => {
  return path.parse(str).name;
};

/**
 * Module exports.
 * @public
 */
// make the custom filters defined in the this 'filters' file available
// for import by other files when they use the require function.
module.exports = {
  json,
  urlEncode,
  pruneList,
  extractFileName
};
