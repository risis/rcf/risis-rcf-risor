'use strict';
const JSON5 = require('json5');
const yaml = require('yaml');
/**
 * Turtle
 *
 * A set of functions to render string templates embedding ES6 template literals
 * This file is part of Nucleus
 */
const getOwnEnumPropSymbols = require('get-own-enumerable-property-symbols').default;
const ostringify = require('stringify-object');
const _get = require('lodash/get');
const _set = require('lodash/set');
const isObj = require('is-obj');

// renders a template using a map object
// template variables are expressed as ES6 template literals, e.g. "${foo.bar.baz}"
exports.render = function (template, map, get = _get, seen = []) {
  return typeof map === 'undefined' || typeof template !== 'string'
    ? template
    : template.replace(/(["']?)\$\{(.+?)\}(["']?)/g, (match, q1, path, q2) => {
    // use the matched template literal as fallback value
      const fallback = match;
      // check if foo is in map using lodash get
      const obj = get(map, path);
      // test if the returned string match a nested template like {foo: '${bar}'}
      const isTemplateString = (typeof obj === 'string') && (obj.match(/\$\{.+?}/) !== null);
      // if the path was not found then return the matched template literal
      if (typeof obj === 'undefined') {
        return fallback;
        // if the path was found and the resulting string doesn't have nested templates
      } else if (typeof obj === 'string' && !isTemplateString) {
      // return the interpreted path
        return q1 + obj + q2;
        // if the path was found but it is an object or contains nested templates
      } else {
      // converts the object path to a string
        const objTemplateString = isTemplateString
          ? obj
          : ostringify(obj, {
            indent: ' '
          });
        // circular references will be replaced with the template literal itself
        if (seen.indexOf(objTemplateString) !== -1) {
          return fallback;
          // try to expand any nested template
        } else {
          seen.push(objTemplateString);
          return q1 + this.render(objTemplateString, map, get, seen) + q2;
        }
      }
    });
};

// renders a template and try to interprets the resolved output as an object
exports.parse = function (template, map, get = _get) {
  // render the template
  const objectString = this.render(template, map, get);

  if (objectString.startsWith('---')) {
    return yaml.parse(objectString);
  }

  // try to convert the resolved template back to an object
  // JSON5 allows to parse a "relaxed" JSON
  let parsedString;

  try {
    parsedString = JSON5.parse(objectString);
  } catch (error) {
    return objectString;
  }

  return parsedString;
};

// CRUD operations on object properties
exports.explore = (input, options) => {
  const base = input;
  options.filter = options.filter || function () { return true; };
  return (function explore (input, options = {}, path = []) {
    if (isObj(input)) {
      const objKeys = Object.keys(input).concat(getOwnEnumPropSymbols(input));
      objKeys.some((el, i) => {
        const attributes = { prray: path.slice(0), path: path.join('.'), el, i, value: input[el] };
        // only process filter elements
        if (options.filter(attributes)) {
          // [first]next
          if (typeof options.next !== 'function' || !options.next(attributes)) {
            // update
            if (typeof options.update === 'function' && options.update(attributes)) {
              _set(base, attributes.prray.concat(el), attributes.value);
            }
            // upgrade
            if (typeof options.upgrade === 'function' && options.upgrade(attributes)) {
              _set(base, attributes.prray, attributes.value);
            }
            // visit
            if (typeof options.visit === 'function') {
              options.visit(attributes);
            }
            // [last] break
            if (typeof options.break === 'function' && options.break(attributes)) {
              return true;
            }
          }
        }
        // explore next
        return explore(input[el], options, path.concat(el));
      });
    }
  })(input, options);
};
