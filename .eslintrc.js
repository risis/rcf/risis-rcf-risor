module.exports = {
  extends: 'standard',
  root: true,
  env: {
    jest: true
  },
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2023
  },
  rules: {
    semi: ['error', 'always']
  },
  ignorePatterns: ['scenarios/models/report-generator/report-template']
};
