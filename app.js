'use strict';

require('dotenv').config();
const express = require('express');
const helmet = require('helmet');
const log4js = require('log4js');
const bearerToken = require('express-bearer-token');

/* Make config available everywhere */

const config = require('./config');
global.config = config;

/* Init logger */

const { config: log4jsConfig } = require('./utils/logging');

try {
  log4js.configure(log4jsConfig);
} catch (err) {
  console.error(`E: ${err.stack}`);
  process.exit(1);
}

/* Init app */

const app = express();

/* ALERT!
   Don't enable express.json() middleware globally, add it only in those routes where it is needed
   Why? The json body parser middleware breaks the service provider cortext, it needs a raw body parser
   More info: https://github.com/request/request/issues/2391
   app.use(express.json()); */

app.use(bearerToken());
app.use(helmet());
app.use(require('./routes'));

module.exports = app;
