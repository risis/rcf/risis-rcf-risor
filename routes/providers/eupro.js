'use strict';

const express = require('express');
const router = express.Router();

const euproController = require('../../controllers/providers/eupro');

router.use(express.json());

router.post('/fetch', euproController.fetch);
router.post('/queryBuilder', euproController.queryBuilder);
router.post('/transform', euproController.transform);
router.all('*', euproController.handleRequest);

module.exports = router;
