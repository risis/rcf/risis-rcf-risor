'use strict';

const express = require('express');
const router = express.Router();

const patentsExplorerController = require('../../controllers/providers/patents-explorer');

// middleware to parser json in body
router.use(express.json());

router.post('/fetch/:action', patentsExplorerController.fetch);
router.post('/search', patentsExplorerController.search);

module.exports = router;
