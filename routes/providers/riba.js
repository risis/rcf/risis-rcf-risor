'use strict';

const express = require('express');
const router = express.Router();

const ribaController = require('../../controllers/providers/riba');

router.all('*', ribaController.handleRequest);

module.exports = router;
