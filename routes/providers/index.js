'use strict';

const express = require('express');
const router = express.Router();

const providerController = require('../../controllers/providers');

router.get('/', providerController.listProviders);
router.use('/d4science', require('./d4science'));
router.use('/cortext', require('./cortext'));
router.use('/datastore', require('./datastore'));
router.use('/riba', require('./riba'));
router.use('/gatecloud', require('./gatecloud'));
router.use('/patents-explorer', require('./patents-explorer'));
router.use('/knowmak', require('./knowmak'));
router.use('/risis-datasets', require('./risis-datasets'));
router.use('/builtins', require('./builtins'));
router.use('/eupro', require('./eupro'));

module.exports = router;
