'use strict';

const express = require('express');
const cortextController = require('../../../controllers/providers/cortext');

const router = express.Router();

router.use(express.json());

router.get('/files_url/:script_name/:hash', cortextController.FilesUrlResults);

module.exports = router;
