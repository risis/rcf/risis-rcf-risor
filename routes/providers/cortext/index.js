'use strict';

const express = require('express');
const cortextController = require('../../../controllers/providers/cortext');

const router = express.Router();

router.use('/builtins', require('./builtins'));
router.all('/:resource', cortextController.handleRequest);
router.all('/:resource/:id', cortextController.handleRequest);

module.exports = router;
