'use strict';

const express = require('express');
const router = express.Router();

const builtinsController = require('../../controllers/providers/builtins');

router.use(express.json());

router.post('/fetch', builtinsController.fetch);
router.post('/transform', builtinsController.transform);
router.post('/transform-csv-to-cortext-format', builtinsController.transformCsvToCortextFormat);
router.post('/join-csv', builtinsController.joinCsv);
router.post('/index-csv', builtinsController.indexCsv);
router.post('/alter-csv', builtinsController.alterCsv);
router.post('/post', builtinsController.post);
router.post('/transfer', builtinsController.transfer);
router.post('/report-generator', builtinsController.reportGenerator);
router.post('/get-one-column-values-as-string', builtinsController.getOneColumnValuesAsString);
router.post('/download-files', builtinsController.downloadFiles);
router.post('/concatenate-csv-columns', builtinsController.concatenateCsvColumns);
router.get('/test/sleep', builtinsController.sleep);
router.get('/test/ping', builtinsController.ping);
router.get('/test/job', builtinsController.job);

module.exports = router;
