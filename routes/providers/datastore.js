'use strict';

const express = require('express');
const datastoreController = require('../../controllers/providers/datastore');
const setDatastoreClient = require('../../middlewares/datastore/init-rcf-datastore-client')(datastoreController);

const router = express.Router();

// middleware to parser json in body
router.use(express.json());

// middleware to init RcfDatastoreClient taking user token into account
router.use(setDatastoreClient);

// files
router.post('/datasets/files', datastoreController.uploadFiles);
router.delete('/datasets/files/:fileId', datastoreController.deleteFile);
router.put('/datasets/files/:fileId', datastoreController.replaceFile);
router.get('/datasets/files/download', datastoreController.downloadFiles);
router.get('/datasets/files/:fileId/import', datastoreController.importFile);
router.get('/datasets/files/import', datastoreController.importFiles);
router.get('/datasets/files/:fileId', datastoreController.downloadFile);
router.get('/datasets/download', datastoreController.downloadDatasetFiles);
router.get('/datasets/files', datastoreController.listFiles);

// datasets
router.get('/datasets', datastoreController.listDatasets);
router.get('/datasets/search', datastoreController.searchDatasets);
router.post('/datasets', datastoreController.createDataset);
router.get('/datasets/info', datastoreController.getDatasetInformation);
router.delete('/datasets', datastoreController.deleteDataset);
router.patch('/datasets/publish', datastoreController.publishDataset);
router.patch('/datasets/edit/:field', datastoreController.editDataset);
router.put('/datasets', datastoreController.replaceDataset);

// users
router.post('/admin/roles/assign', datastoreController.assignRole);

module.exports = router;
