'use strict';

const express = require('express');
const router = express.Router();

router.use('/vico', require('./vico'));
router.use('/cheetah', require('./cheetah'));
router.use('/cib', require('./cib'));
router.use('/nanopub', require('./nanopub'));
router.use('/cinnob', require('./cinnob'));
router.use('/isitm', require('./isitm'));
router.use('/efil', require('./efil'));

module.exports = router;
