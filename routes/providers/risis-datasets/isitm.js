'use strict';

const express = require('express');
const router = express.Router();

const risisDatasetsIsitmController = require('../../../controllers/providers/risis-datasets/isitm');

router.use(express.json());

router.post('/v1.1/transform', risisDatasetsIsitmController.transform);
router.post('/v1.2/search', risisDatasetsIsitmController.search);
router.all('*', risisDatasetsIsitmController.handleRequest);

module.exports = router;
