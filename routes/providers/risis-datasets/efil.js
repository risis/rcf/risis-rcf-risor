'use strict';

const express = require('express');
const router = express.Router();

const risisDatasetsEfilController = require('../../../controllers/providers/risis-datasets/efil');

router.use(express.json());

router.post('/v1.1/transform', risisDatasetsEfilController.transform);
router.post('/v1.2/search', risisDatasetsEfilController.search);
router.all('*', risisDatasetsEfilController.handleRequest);

module.exports = router;
