'use strict';

const express = require('express');
const router = express.Router();

const risisDatasetsCibController = require('../../../controllers/providers/risis-datasets/cib');

router.use(express.json());

router.post('/v1.1/transform', risisDatasetsCibController.transform);
router.post('/v1.2/search', risisDatasetsCibController.search);
router.all('*', risisDatasetsCibController.handleRequest);

module.exports = router;
