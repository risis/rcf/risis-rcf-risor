'use strict';

const express = require('express');
const router = express.Router();

const risisDatasetsVicoController = require('../../../controllers/providers/risis-datasets/vico');

router.use(express.json());

router.post('/v1.1/transform', risisDatasetsVicoController.transform);
router.post('/v1.2/search', risisDatasetsVicoController.search);
router.all('*', risisDatasetsVicoController.handleRequest);

module.exports = router;
