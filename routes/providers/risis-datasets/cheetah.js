'use strict';

const express = require('express');
const router = express.Router();

const risisDatasetsCheetahController = require('../../../controllers/providers/risis-datasets/cheetah');

router.use(express.json());

router.post('/v1.1/transform', risisDatasetsCheetahController.transform);
router.post('/v1.2/search', risisDatasetsCheetahController.search);
router.all('*', risisDatasetsCheetahController.handleRequest);

module.exports = router;
