'use strict';

const express = require('express');
const router = express.Router();

const risisDatasetsNanopubController = require('../../../controllers/providers/risis-datasets/nanopub');

router.use(express.json());

router.post('/fetch', risisDatasetsNanopubController.fetch);
router.post('/transform', risisDatasetsNanopubController.transform);
router.all('*', risisDatasetsNanopubController.handleRequest);

module.exports = router;
