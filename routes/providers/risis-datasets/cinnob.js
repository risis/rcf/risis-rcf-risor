'use strict';

const express = require('express');
const router = express.Router();

const risisDatasetsCinnobController = require('../../../controllers/providers/risis-datasets/cinnob');

router.use(express.json());

router.post('/v1.1/fetch', risisDatasetsCinnobController.fetch);
router.post('/transform', risisDatasetsCinnobController.transform);
router.all('*', risisDatasetsCinnobController.handleRequest);

module.exports = router;
