'use strict';

const express = require('express');
const router = express.Router();

const annieController = require('../../../controllers/providers/gatecloud/annie');

router.use(express.json());

router.post('/process', annieController.processAnnie);

module.exports = router;
