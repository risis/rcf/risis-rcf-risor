'use strict';

const express = require('express');
const router = express.Router();

const knowmakController = require('../../../controllers/providers/gatecloud/knowmak');

router.use(express.json());

router.all('*', knowmakController.knowmakRequest);

module.exports = router;
