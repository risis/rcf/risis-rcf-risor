'use strict';

const express = require('express');
const router = express.Router();

const bridgeAnnieController = require('../../../controllers/providers/gatecloud/bridge-annie');

router.use(express.json());

router.post('/process/submit', bridgeAnnieController.processSubmitBridge);
router.post('/process/job/:job_id/output', bridgeAnnieController.processJobOutputBridge);
router.all('*', bridgeAnnieController.handleRequest);

module.exports = router;
