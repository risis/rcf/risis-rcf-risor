'use strict';

const express = require('express');
const router = express.Router();

router.use('/annie', require('./annie'));
router.use('/twitie', require('./twitie'));
router.use('/measurements', require('./measurements'));
router.use('/knowmak', require('./knowmak'));
router.use('/bridge-annie', require('./bridge-annie'));
router.use('/bridge-twitie', require('./bridge-twitie'));
router.use('/bridge-measurements', require('./bridge-measurements'));
router.use('/translate', require('./translate'));
router.use('/bridge-translate', require('./bridge-translate'));

module.exports = router;
