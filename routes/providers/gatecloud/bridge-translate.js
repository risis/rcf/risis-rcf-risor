'use strict';

const express = require('express');
const router = express.Router();

const bridgeTranslateController = require('../../../controllers/providers/gatecloud/bridge-translate');

router.use(express.json());

router.post('/process/submit', bridgeTranslateController.processSubmitBridge);
router.post('/process/job/:job_id/output', bridgeTranslateController.processJobOutputBridge);
router.post('/transform', bridgeTranslateController.transform);
router.all('*', bridgeTranslateController.handleRequest);

module.exports = router;
