'use strict';

const express = require('express');
const router = express.Router();

const twitieController = require('../../../controllers/providers/gatecloud/twitie');

router.use(express.json());

router.post('/process', twitieController.processTwitie);

module.exports = router;
