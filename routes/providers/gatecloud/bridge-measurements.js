'use strict';

const express = require('express');
const router = express.Router();

const bridgeMeasurementsController = require('../../../controllers/providers/gatecloud/bridge-measurements');

router.use(express.json());

router.post('/process/submit', bridgeMeasurementsController.processSubmitBridge);
router.post('/process/job/:job_id/output', bridgeMeasurementsController.processJobOutputBridge);
router.all('*', bridgeMeasurementsController.handleRequest);

module.exports = router;
