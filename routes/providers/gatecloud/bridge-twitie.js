'use strict';

const express = require('express');
const router = express.Router();

const bridgeTwitieController = require('../../../controllers/providers/gatecloud/bridge-twitie');

router.use(express.json());

router.post('/process/submit', bridgeTwitieController.processSubmitBridge);
router.post('/process/job/:job_id/output', bridgeTwitieController.processJobOutputBridge);
router.all('*', bridgeTwitieController.handleRequest);

module.exports = router;
