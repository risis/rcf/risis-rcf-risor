'use strict';

const express = require('express');
const router = express.Router();

const measurementsController = require('../../../controllers/providers/gatecloud/measurements');

router.use(express.json());

router.post('/process', measurementsController.processMeasurements);

module.exports = router;
