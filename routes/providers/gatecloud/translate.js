'use strict';

const express = require('express');
const router = express.Router();

const translateController = require('../../../controllers/providers/gatecloud/translate');

router.use(express.json());

router.post('/:from/:to', translateController.processTranslate);

module.exports = router;
