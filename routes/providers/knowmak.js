'use strict';

const express = require('express');
const router = express.Router();

const knowmakController = require('../../controllers/providers/knowmak');

// middleware to parser json in body
router.use(express.json());

router.post('/fetch', knowmakController.fetch);
router.post('/transform', knowmakController.transform);

module.exports = router;
