const express = require('express');

const { d4scienceSearch } = require('../../controllers/providers/d4science');

const router = express.Router();

router.use(express.json());

router.post('/search', d4scienceSearch());

module.exports = router;
