'use strict';

const express = require('express');
const router = express.Router();

router.use(express.json());

const { name, description, version, author, homepage } = require('../package.json');

router.all('', function (req, res) {
  const welcomeInfos = { name, description, version, author, homepage };
  res.json(welcomeInfos);
});

module.exports = router;
