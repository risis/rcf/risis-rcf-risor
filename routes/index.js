'use strict';

const express = require('express');

const router = express.Router();

router.use(require('../middlewares/context'));
router.use(require('../middlewares/log'));

if (process.env.NODE_ENV.trim() !== 'test') {
  router.use(require('../middlewares/auth'));
  router.use(require('../middlewares/openapi-validator'));
}

router.all('/', require('./welcome'));
router.use('/providers', require('./providers'));
router.use('/scenarios', require('./scenarios'));

module.exports = router;
