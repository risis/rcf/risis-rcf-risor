'use strict';

const express = require('express');
const router = express.Router();

const scenarioController = require('../controllers/scenarios');

// middleware to parser json in body
router.use(express.json());

router.get('/', scenarioController.listScenarios);

router.post('/instances/:instanceId/actions', scenarioController.processInstanceAction);

router.get('/:scenarioId', scenarioController.getScenario);
router.post('/:scenarioId', scenarioController.runScenario);
router.get('/:scenarioId/bundle', scenarioController.getScenarioBundle);

module.exports = router;
