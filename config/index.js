if (process.env.NODE_ENV !== 'test') {
  console.info(`I: load config file config/${process.env.NODE_ENV}.yaml`);
}

const appRoot = require('app-root-path').toString();
const path = require('path');
const fs = require('fs-extra');

const config_ = require('config-lite')({
  config_basedir: appRoot,
  config_dir: 'config'
});

const tmpRoot = path.join(appRoot, 'data', 'temp');
fs.ensureDirSync(tmpRoot);

module.exports = {
  ...{
    node_port: process.env.RISOR_PORT,
    appRoot,
    tmpRoot,
    datastoreRoot: path.join(appRoot, 'data', 'datastore')
  },
  ...config_
};
