#!/bin/sh
set -e

CMD="$@"

./bootstrap.sh config
./bootstrap.sh dotenv

exec $CMD
