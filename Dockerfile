# Check https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md for reference

FROM node:18-alpine AS base

WORKDIR /home/node/app

#take the advantages in reusing the docker cache layers
#reference http://bitjudo.com/blog/2014/03/13/building-efficient-dockerfiles-node-dot-js
RUN printf '@cortext:registry=https://lib.cortext.net\n//lib.cortext.net/:_authToken=${NPM_TOKEN}' >> .npmrc
COPY package*.json ./

#it is recomended to run container as non-root 'node' user
#but when container run as 'node' user it is impossible to
#write files under app root dir, like logs, data, etc, and
#in development environment running with docker-compose we
#need to map . to /home/node/app
#RUN chown -R node:node /home/node/app
#USER node

ENTRYPOINT ["./entrypoint.sh"]
CMD ["npm", "start"]

# production image

FROM base AS prod

ARG NPM_TOKEN

RUN npm ci --only=production && npm cache clean --force && rm -f .npmrc

COPY . .

# development image

FROM base AS dev

ARG NPM_TOKEN

RUN npm install && rm .npmrc

COPY . .
