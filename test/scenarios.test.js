const app = require('../app');
const scenarios = require('../scenarios');

describe('Scenarios', () => {
  const storage = require('../service_providers/storage');
  storage.uploadFile = jest.fn();
  let server;
  beforeAll(() => {
    server = app.listen(global.config.node_port);
  });
  afterAll(() => {
    server.close();
  });

  it('should returns JSON response if using `json` filter on RSML', async done => {
    const context = {};
    const params = {
      inputs: { 'workspace-uri': 'm4://users-workspace/fc8cae85-c0d2-4c62-b383-dc11fec39cf8/projects/LnLGLG6nXNT/scenarios/json-response.test/RfscLUOFcN5' }
    };
    const response = await scenarios.runScenario('json-response.test', context, params);
    expect(response.result.outputs[0].jsonResponse).toMatchObject(
      [
        { desc: expect.any(String), title: expect.any(String) },
        expect.any(Object),
        expect.any(Object),
        expect.any(Object)
      ]
    );
    done();
  });
});
