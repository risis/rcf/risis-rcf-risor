const request = require('supertest');
const app = require('../app');

describe('Get /providers endpoint', () => {
  it('should services providers list not be empty', async done => {
    const res = await request(app).get('/providers');
    expect(res.status).toBe(200);
    expect(res.body.length).toBeGreaterThan(0);
    done();
  });
});
