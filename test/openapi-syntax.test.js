const path = require('path');
const Enforcer = require('openapi-enforcer');

describe('OpenAPI document', () => {
  test('openapi.yaml file syntax validation', (done) => {
    expect(() =>
      Enforcer(path.join(__dirname, '../openapi.yaml'), { fullResult: true })
        .then(({ error, warning }) => {
          // FIXME the regex below is a workaround to deal with the issue https://gitlab.com/risis/rcf/risis-rcf-risor/-/issues/67
          const regex = /BearerAuth > BearerAuth\s*Security requirement for http value must be an empty array\s*$/m;
          if (error && !regex.test(error)) throw new Error(error);
          else {
            // curently openapi.yaml is using some non-standard format keys,
            // like format:slug, format:uri, and so on, but openapi specs states
            // that the format key is a freeform field but for some reason openapi-enforcer
            // is firing some warnings, then it is needed to fix these warnings and
            // after that un-commend line below to be notified about some eventually
            // warn about it
            // if (warning) console.warn(warning)
            done();
          }
        })
    ).not.toThrow();
  });
});
