const request = require('supertest');
const app = require('../app');

describe('Get root / endpoint', () => {
  it('should get the app version from welcome endpoint', async done => {
    const res = await request(app).get('/');
    expect(res.status).toBe(200);
    expect(res.body.version).toBe(process.env.npm_package_version);
    done();
  });

  it('should get the same response as POST or GET on welcome', async done => {
    const resGET = await request(app).get('/');
    expect(resGET.status).toBe(200);
    const resPOST = await request(app).post('/');
    expect(resPOST.status).toBe(200);
    expect(resGET.body).toStrictEqual(resPOST.body);
    done();
  });
});
