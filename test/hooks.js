'use strict';

require('dotenv').config();
const hooks = require('hooks');
const url = require('url');
const appRoot = require('app-root-path').toString();
const path = require('path');
const config = require(path.join(appRoot, 'config'));
const RcfDatastoreClient = require('@cortext/rcf-datastore-client');
const faker = require('faker');

const datastore = new RcfDatastoreClient(url.format(config.providers.datastore.uri), config.providers.datastore.token);
const responseStash = {};
// const stash = {};

function newDataverseInformation (alias) {
  const datasetInformation = {
    alias: `${alias}`,
    name: `${faker.random.words()}`,
    affiliation: 'RISIS',
    permissionRoot: false,
    description: 'RISIS scenarios',
    dataverseContacts: [
      {
        contactEmail: 'it@risis.io'
      }
    ],
    dataverseSubjects: ['Research Group']
  };
  return datasetInformation;
}

function newDatasetInformation (title) {
  const datasetInformation = {
    title: `${title}`,
    descriptions: [
      {
        text: 'Data about habitants in mars',
        date: '2030-12-24'
      }
    ],
    authors: [
      {
        fullname: 'Martian Lennon Smith Jr.'
      }
    ],
    contact: [
      {
        email: 'lennon.smith@martian.universe.com',
        fullname: 'Martian Lennon Smith'
      }
    ],
    subject: [
      'Other'
    ]
  };
  return datasetInformation;
}

// Reference about Dredd hooks:
// https://dredd.org/en/latest/hooks/js.html

hooks.beforeAll((transactions) => {
  // enable test for only some RISOR endpoints
  const transactionsEnabled = {
    '/ > Get application metadata. > 200 > application/json; charset=utf-8': true,
    // openapi3 description of /providers makes use of keyword `anyOf` unsupported on dredd until now
    // see the status of openapi3 support at https://github.com/apiaryio/api-elements.js/blob/master/packages/openapi3-parser/STATUS.md
    // '/providers > List services providers. > 200 > application/json; charset=utf-8': true,
    // '/scenarios > List scenarios. > 200 > application/json; charset=utf-8': true,
    // '/scenarios/{scenarioId} > Get scenario metadata. > 200 > application/json; charset=utf-8': true,
    '/scenarios/{scenarioId}/bundle > Download scenario bundle. > 200 > application/zip': true,
    // '/providers/datastore/datasets{?service_provider_key} > List datasets. > 200 > application/json; charset=utf-8': true,
    '/providers/datastore/datasets{?service_provider_key} > Create dataset. > 201 > application/json; charset=utf-8': true,
    '/providers/datastore/datasets/info{?service_provider_key} > Get dataset metadata. > 200 > application/json; charset=utf-8': true,
    '/providers/datastore/datasets/download{?service_provider_key} > Download dataset bundle. > 200 > application/zip;charset=utf-8': true,
    '/builtins/transform > Transform data format. > 200 > application/json; charset=utf-8': true
  };
  for (const transaction of transactions) {
    transaction.skip = !transactionsEnabled[transaction.name];
  }
});

hooks.beforeEach(function (transaction) {
  // add service_provider_key query parameter for datastore endpoints
  if (transaction.fullPath.indexOf('/providers/datastore') === 0) {
    if (transaction.fullPath.indexOf('?') > -1) {
      transaction.fullPath += `&service_provider_key=${config.providers.datastore.token}`;
    } else {
      transaction.fullPath += `?service_provider_key=${config.providers.datastore.token}`;
    }
  }
});

// // /providers/datastore/datasets > List datasets
//
// hooks.before('/providers/datastore/datasets{?service_provider_key} > List datasets. > 200 > application/json; charset=utf-8', async (transaction, done) => {
//   try {
//     const dataverseAlias = 'risor-scenarios';
//     hooks.log(`create dataverse '${dataverseAlias}'`);
//     const dataverseInformation = newDataverseInformation(dataverseAlias);
//     await datastore.createDataverse('root', dataverseInformation);
//     hooks.log("create dataset 'Demographic data of planet Mars'");
//     const datasetInformation = newDatasetInformation('Demographic data of planet Mars');
//     const res = await datastore.createDataset(dataverseAlias, datasetInformation);
//     stash[transaction.name] = {};
//     stash[transaction.name]['persistentId'] = res.data.data.persistentId;
//     stash[transaction.name]['dataverseAlias'] = dataverseAlias;
//   } catch(err) {
//     hooks.log(err);
//   }
//   done();
// });
//
// hooks.after('/providers/datastore/datasets{?service_provider_key} > List datasets. > 200 > application/json; charset=utf-8', async (transaction, done) => {
//   try {
//     hooks.log("delete dataset 'Demographic data of planet Mars'");
//     const datasetId = stash[transaction.name]['persistentId'];
//     const dataverseAlias = stash[transaction.name]['dataverseAlias'];
//     await datastore.deleteDataset(datasetId);
//     hooks.log(`delete dataverse '${dataverseAlias}'`);
//     await datastore.deleteDataverse(dataverseAlias);
//     const responseBody = JSON.parse(transaction.real.body);
//     if (responseBody[0].latestVersion.datasetPersistentId !== datasetId) {
//       transaction.fail = `datasetId ${responseBody[0].latestVersion.datasetPersistentId} expected to be equal to ${datasetId}`;
//     }
//   } catch(err) {
//     hooks.log(err);
//   }
//   done();
// });

// /providers/datastore/datasets > Create dataset

hooks.before('/providers/datastore/datasets{?service_provider_key} > Create dataset. > 201 > application/json; charset=utf-8', async (transaction, done) => {
  try {
    hooks.log("create dataverse 'risor-scenarios'");
    const dataverseInformation = newDataverseInformation('risor-scenarios');
    await datastore.createDataverse('root', dataverseInformation);
  } catch (err) {
    hooks.log(err);
  }
  done();
});

hooks.after('/providers/datastore/datasets{?service_provider_key} > Create dataset. > 201 > application/json; charset=utf-8', async (transaction, done) => {
  try {
    const responseBody = JSON.parse(transaction.real.body);
    const datasetId = responseBody.data.persistentId;
    hooks.log(`delete dataset persistentId '${datasetId}'`);
    await datastore.deleteDataset(datasetId);
    hooks.log("delete dataverse 'risor-scenarios'");
    await datastore.deleteDataverse('risor-scenarios');
  } catch (err) {
    hooks.log(err);
  }
  done();
});

// /providers/datastore/datasets > Get dataset metadata

hooks.before('/providers/datastore/datasets/info{?service_provider_key} > Get dataset metadata. > 200 > application/json; charset=utf-8', async (transaction, done) => {
  try {
    hooks.log("create dataverse 'risor-scenarios'");
    const dataverseInformation = newDataverseInformation('risor-scenarios');
    await datastore.createDataverse('root', dataverseInformation);
    hooks.log("create dataset 'Demographic data of planet Mars'");
    const datasetInformation = newDatasetInformation('Demographic data of planet Mars');
    const res = await datastore.createDataset('risor-scenarios', datasetInformation);
    const datasetId = res.data.data.persistentId;
    responseStash[transaction.name] = datasetId;
    transaction.fullPath = transaction.fullPath.replace(/datasetId=doi[^&]+/, `datasetId=${encodeURIComponent(datasetId)}`);
  } catch (err) {
    hooks.log(err);
  }
  done();
});

hooks.after('/providers/datastore/datasets/info{?service_provider_key} > Get dataset metadata. > 200 > application/json; charset=utf-8', async (transaction, done) => {
  try {
    const datasetId = responseStash[transaction.name];
    hooks.log(`delete dataset persistentId '${datasetId}'`);
    await datastore.deleteDataset(datasetId);
    hooks.log("delete dataverse 'risor-scenarios'");
    await datastore.deleteDataverse('risor-scenarios');
    const responseBody = JSON.parse(transaction.real.body);
    const datasetTitle = responseBody.data.latestVersion.metadataBlocks.citation.fields[0].value;
    if (datasetTitle !== 'Demographic data of planet Mars') {
      transaction.fail = `dataset title '${datasetTitle}' expected to be equal to 'Demographic data of planet Mars'`;
    }
  } catch (err) {
    hooks.log(err);
  }
  done();
});

// /providers/datastore/datasets/download > Download dataset bundle

hooks.before('/providers/datastore/datasets/download{?service_provider_key} > Download dataset bundle. > 200 > application/zip;charset=utf-8', async (transaction, done) => {
  try {
    hooks.log("create dataverse 'risor-scenarios'");
    const dataverseInformation = newDataverseInformation('risor-scenarios');
    await datastore.createDataverse('root', dataverseInformation);
    hooks.log("create dataset 'Demographic data of planet Mars'");
    const datasetInformation = newDatasetInformation('Demographic data of planet Mars');
    const res = await datastore.createDataset('risor-scenarios', datasetInformation);
    const datasetId = res.data.data.persistentId;
    hooks.log("upload file 'greentech_dataset_sample100.csv'");
    const sampleFilePath = path.join(appRoot, 'test', 'samples', 'greentech_dataset_sample100.csv');
    await datastore.uploadFile(datasetId, sampleFilePath, 'greentech_dataset_sample100.csv');
    responseStash[transaction.name] = datasetId;
    transaction.fullPath = transaction.fullPath.replace(/datasetId=doi[^&]+/, `datasetId=${encodeURIComponent(datasetId)}`);
  } catch (err) {
    hooks.log(err);
  }
  done();
});

hooks.after('/providers/datastore/datasets/download{?service_provider_key} > Download dataset bundle. > 200 > application/zip;charset=utf-8', async (transaction, done) => {
  try {
    const datasetId = responseStash[transaction.name];
    hooks.log(`delete dataset persistentId '${datasetId}'`);
    await datastore.deleteDataset(datasetId);
    hooks.log("delete dataverse 'risor-scenarios'");
    await datastore.deleteDataverse('risor-scenarios');
  } catch (err) {
    hooks.log(err);
  }
  done();
});
