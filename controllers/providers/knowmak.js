'use strict';

const url = require('url');
const unirest = require('unirest');
const faker = require('faker');
const fs = require('fs');
const urljoin = require('url-join');
const { transformBuiltins } = require('../../utils/builtins');

const config = global.config;

module.exports = {
  fetch: async (req, res) => {
    const { context: { logger } } = req;
    const body = req.body;
    const { endpoint } = body.api;
    const { indicators, groupBy } = body.params;
    const knowmakUrl = urljoin(url.format(config.providers.eupro.uri), endpoint, '/');
    try {
      const response = await unirest.get(knowmakUrl);
      logger.debug('fetch api response.body = %o', (response ? response.body : null));
      if (response.status !== 200 & response.body.error) {
        throw new Error('Error getting indicators.');
      }
      const indicatorsSplitted = indicators.split(',');
      logger.debug('Indicators selected:', indicatorsSplitted);
      const folder = config.tmpRoot;
      const files = [];
      for (let i = 0; i < indicatorsSplitted.length; i++) {
        const indicator = indicatorsSplitted[i];
        // eslint-disable-next-line camelcase
        const resp = response.body.filter(obj => Object.values(obj).some(indicator_label => indicator_label === indicator));
        if (!resp) {
          throw new Error('Indicator selected not found.');
        }
        const indicatorSelectedUri = urljoin(url.format(config.providers.eupro.uri), `/v5/${resp[0].URI}`, `?groupby=${groupBy},year`);
        const resFetch = await unirest.get(indicatorSelectedUri);
        if (resFetch.status !== 200 & resFetch.body.error) {
          throw new Error('Error getting indicator URI.');
        }
        const data = resFetch.body.data;
        const filename = `${faker.random.alphaNumeric(10)}.json`;
        const pathFile = `${folder}/${filename}`;
        logger.debug(`creating temporary file ${pathFile} to save fetch api results`);
        fs.writeFileSync(pathFile, JSON.stringify(data));
        files.push(pathFile);
      }
      res.status(200).send({ files });
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  },
  transform: async (req, res) => {
    const { context: { logger } } = req;
    const { indicators, groupBy, files } = req.body;
    const filenames = indicators.split(',').map(indicator => `${indicator}-groupBy-${groupBy}`);
    const options = [];
    filenames.forEach(() => {
      options.push({
        delimiter: '\t',
        fields: ['value', groupBy, 'year']
      });
    });
    const body = {
      filename: filenames,
      type: 'file',
      from: 'json',
      to: 'tsv',
      options,
      file: files.split(',')
    };
    const response = await transformBuiltins(res, body, logger);
    const filesConverted = [];
    if (response.result) {
      response.result.forEach(result => {
        filesConverted.push(`${result.folder}/${result.filename}`);
      });
    } else if (response.filename) {
      filesConverted.push(`${response.folder}/${response.filename}`);
    }
    res.status(200).send(filesConverted);
  }
};
