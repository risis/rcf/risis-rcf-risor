'use strict';

const request = require('request');
const keycloak = require('../../utils/keycloak');
const url = require('url');
const urljoin = require('url-join');

const config = global.config;

module.exports = {
  handleRequest: async (req, res) => {
    const { context: { logger } } = req;
    const ribaUrl = url.format(config.providers.riba.uri);
    logger.info('proxying request to RIBA, url %o', ribaUrl);
    const token = await keycloak.getToken();
    logger.debug('keycloak access-token = %o', token);
    logger.debug('request path = %o', req.path);
    const options = {
      url: urljoin(ribaUrl, req.path),
      headers: {
        Authorization: `Bearer ${token}`
      },
      method: req.method.toLowerCase()
    };
    logger.debug('proxying request to RIBA, options = %o', options);
    req.pipe(request(options)).pipe(res);
  }
};
