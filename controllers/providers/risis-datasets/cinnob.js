'use strict';

const request = require('request');
const url = require('url');
const unirest = require('unirest');
const faker = require('faker');
const urljoin = require('url-join');
const fs = require('fs');
const { transformBuiltins } = require('../../../utils/builtins');

const config = global.config;

function formatValueSectorNace (value) {
  // Convert to number and round to 1 decimal place
  const roundedValue = parseFloat(value).toFixed(1);

  // Split into integer and decimal parts
  const [integerPart, decimalPart] = roundedValue.split('.');

  // Pad the integer part with leading zeros if needed
  const paddedIntegerPart = integerPart.padStart(2, '0');

  // Combine the formatted parts and return
  return `${paddedIntegerPart}.${decimalPart}`;
}

module.exports = {
  handleRequest: async (req, res) => {
    const { context: { logger } } = req;
    const options = {
      url: urljoin(url.format(config.providers['risis-datasets/cinnob'].uri), req.path),
      qs: req.query,
      headers: req.headers,
      method: req.method.toLowerCase()
    };
    logger.info('proxying request to RISIS DATASETS CinnoB API');
    logger.debug('options = %o', options);
    req.pipe(request(options)).pipe(res);
  },
  transform: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const response = await transformBuiltins(res, req.body, logger);
      const filesConverted = [];
      if (response.result) {
        response.result.forEach(result => {
          filesConverted.push(`${result.folder}/${result.filename}`);
        });
      } else if (response.filename) {
        filesConverted.push(`${response.folder}/${response.filename}`);
      }
      res.status(200).send(filesConverted);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  fetch: async (req, res) => {
    const { context: { logger } } = req;
    const body = req.body;
    const { endpoint, query } = body.api;
    const cinnobUrl = urljoin(url.format(config.providers['risis-datasets/cinnob'].uri), 'v1.1', endpoint, '/');

    try {
      const headers = { Accept: 'application/json', 'Content-Type': 'application/json' };
      logger.info('fetch cinnob api, url = %o', cinnobUrl);
      logger.debug('query params = %o', query);

      if (query['firm_sector_nace_3digits.min'] >= 0 && query['firm_sector_nace_3digits.max'] >= 0) {
        const formattedMin = formatValueSectorNace(query['firm_sector_nace_3digits.min']);
        const formattedMax = formatValueSectorNace(query['firm_sector_nace_3digits.max']);
        query['firm_sector_nace_3digits.between'] = `${formattedMin},${formattedMax}`;
        delete query['firm_sector_nace_3digits.min'];
        delete query['firm_sector_nace_3digits.max'];
      }
      let results = [];
      let firmsExtraction = query['firm_register_id.in'];
      const queryFiltered = Object.fromEntries(
        Object.entries(query).filter(([key, value]) => value !== '')
      );
      if (Object.keys(queryFiltered).length || Object.values(queryFiltered).every(val => val === '')) {
        let continueFetching = true;
        const userLimit = queryFiltered.limit || null;
        let remainingRecords = userLimit;
        let perPage = 100;
        for (let offset = 0; continueFetching; offset += perPage) {
          if (remainingRecords && remainingRecords < perPage) {
            perPage = remainingRecords;
          }
          const response = await unirest
            .get(cinnobUrl)
            .query(queryFiltered)
            .query({
              offset,
              limit: perPage
            })
            .headers(headers);

          if ((response.status >= 400 && offset === 0) || (response.status >= 400 && response.status !== 404)) {
            throw new Error(JSON.stringify(response));
          }
          if (!response.body.instances || response.body.instances.length === 0) {
            continueFetching = false;
            break;
          }
          results = [...results, ...response.body.instances];

          if (userLimit == null) {
            continue;
          }
          remainingRecords -= perPage;
          if (remainingRecords <= 0) {
            break;
          }
        }
        if (endpoint === '/entities/firms' && queryFiltered['firm_country.in']) {
          firmsExtraction = [...new Set(results.map(obj => obj.firm_register_id))].join(',');
        }
      }

      const folder = config.tmpRoot;
      const filename = `${faker.random.alphaNumeric(10)}.json`;
      const filePath = `${folder}/${filename}`;
      logger.debug(`creating temporary file ${filePath} to save fetch api results`);

      await fs.promises.writeFile(filePath, JSON.stringify(results.flat()));

      res.status(200).send({ folder, filename, count: results.flat().length, firmsExtraction });
    } catch (e) {
      const response = JSON.parse(e.message);
      logger.error(response.body);
      res.status(response.statusCode).send({ error: response.body });
    }
  }
};
