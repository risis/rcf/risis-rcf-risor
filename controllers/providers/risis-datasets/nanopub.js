'use strict';

const request = require('request');
const url = require('url');
const unirest = require('unirest');
const urljoin = require('url-join');
const faker = require('faker');
const fs = require('fs');

const config = global.config;
const { transformBuiltins } = require('../../../utils/builtins');

module.exports = {
  handleRequest: async (req, res) => {
    const { context: { logger } } = req;
    const options = {
      url: urljoin(url.format(config.providers['risis-datasets/nanopub'].uri), req.path),
      qs: req.query,
      headers: req.headers,
      method: req.method.toLowerCase()
    };
    logger.info('proxying request to RISIS DATASETS NANO API');
    logger.debug('options = %o', options);
    req.pipe(request(options)).pipe(res);
  },
  transform: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const response = await transformBuiltins(res, req.body, logger);
      const filesConverted = [];
      if (response.result) {
        response.result.forEach(result => {
          filesConverted.push(`${result.folder}/${result.filename}`);
        });
      } else if (response.filename) {
        filesConverted.push(`${response.folder}/${response.filename}`);
      }
      res.status(200).send(filesConverted);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  fetch: async (req, res) => {
    const { context: { logger } } = req;
    const body = req.body;
    const { endpoint, query, pagination } = body.api;
    const nanoUrl = urljoin(url.format(config.providers['risis-datasets/nanopub'].uri), endpoint, '/');
    const maxResults = body['max-results'];
    const resultsKey = body.results;
    const pageSize = pagination.page_size;
    const limitParam = pagination.limit_param;
    const offsetParam = pagination.offset_param;

    Object.keys(query).forEach(key => {
      if (query[key] === '') {
        delete query[key];
      }
    });

    try {
      let continueFetching = true;
      const results = [];
      for (let offset = 0; continueFetching; offset = parseInt(offset) + parseInt(pageSize)) {
        logger.debug('fetch api, url = %o, offset = %o, query params = %o', nanoUrl, offset, query);
        const response = await unirest
          .get(nanoUrl)
          .query(query)
          .query(`${limitParam}=${pageSize}`)
          .query(`${offsetParam}=${offset}`);
        logger.debug('fetch api response.body = %o', (response ? response.body : null));
        let totalCount = null;
        let responseResults = null;
        try {
          if (maxResults !== '') {
            // eslint-disable-next-line no-eval
            totalCount = isNaN(maxResults) ? eval(`response.body.${maxResults}`) : parseInt(maxResults);
          }
          if (resultsKey) {
            // eslint-disable-next-line no-eval
            responseResults = eval(`response.body.${resultsKey}`);
          } else {
            responseResults = response.body instanceof Array ? response.body : [response.body];
          }
        } catch (err) {
          logger.error(err);
        }
        if (response.status !== 400 & response.body.code === 3 & offset === 0) {
          throw new Error('Results not found for the query.');
        } else if (response.body.code === 3 || (totalCount !== null && (totalCount === 0 || offset >= totalCount))) {
          continueFetching = false;
        } else if (responseResults) {
          results.push(responseResults);
        } else {
          throw new Error('Empty results. Specify the key where the results are placed as "results" when sending the request.');
        }
      }
      const folder = config.tmpRoot;
      const filename = `${faker.random.alphaNumeric(10)}.json`;
      logger.debug(`creating temporary file  ${folder}/${filename} to save fetch api results`);
      const stream = await fs.createWriteStream(`${folder}/${filename}`);
      stream.on('open', (fd) => {
        stream.write(JSON.stringify(results.flat()));
        stream.end();
      });
      stream.on('close', (fd) => {
        res.status(200).send({ folder, filename, count: results.flat().length });
      });
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  }
};
