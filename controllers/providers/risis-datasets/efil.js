'use strict';

const request = require('request');
const url = require('url');
const unirest = require('unirest');
const urljoin = require('url-join');
const faker = require('faker');
const fs = require('fs');
const config = global.config;
const { transformBuiltins } = require('../../../utils/builtins');

module.exports = {
  handleRequest: async (req, res) => {
    const { context: { logger } } = req;
    const options = {
      url: urljoin(url.format(config.providers['risis-datasets/efil'].uri), req.path),
      qs: req.query,
      headers: req.headers,
      method: req.method.toLowerCase()
    };
    logger.info('proxying request to RISIS DATASETS EFIL API');
    logger.debug('options = %o', options);
    req.pipe(request(options)).pipe(res);
  },
  transform: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const response = await transformBuiltins(res, req.body, logger);
      const filesConverted = [];
      if (response.result) {
        response.result.forEach(result => {
          filesConverted.push(`${result.folder}/${result.filename}`);
        });
      } else if (response.filename) {
        filesConverted.push(`${response.folder}/${response.filename}`);
      }
      res.status(200).send(filesConverted);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  search: async (req, res) => {
    const { body, context: { logger } } = req;
    const efilUrl = urljoin(url.format(config.providers['risis-datasets/efil'].uri), req.path);
    try {
      logger.info('fetch efil api, url = %o', efilUrl);
      logger.debug('query params = %o', body);
      const headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      };
      let results = [];
      let continueFetching = true;
      const userLimit = body.limit || null;
      let remainingRecords = userLimit;
      let perPage = 100;
      for (let offset = 0; continueFetching; offset += perPage) {
        if (remainingRecords && remainingRecords < perPage) {
          perPage = remainingRecords;
        }
        const response = await unirest
          .post(efilUrl)
          .query({
            offset,
            limit: perPage
          })
          .headers(headers)
          .send(body);

        if ((response.status >= 400 && offset === 0) || (response.status >= 400 && response.status !== 404)) {
          throw new Error(JSON.stringify(response));
        }
        if ((response.body.error && response.body.error.context && response.body.error.context.code === 404) || response.body.results.length === 0) {
          continueFetching = false;
          break;
        }
        results = [...results, ...response.body.results];

        if (userLimit == null) {
          continue;
        }
        remainingRecords -= perPage;
        if (remainingRecords <= 0) {
          break;
        }
      }
      const folder = config.tmpRoot;
      const filename = `${faker.random.alphaNumeric(10)}.json`;
      logger.debug(`creating temporary file  ${folder}/${filename} to save fetch api results`);
      const stream = await fs.createWriteStream(`${folder}/${filename}`);
      stream.on('open', (fd) => {
        stream.write(JSON.stringify(results.flat()));
        stream.end();
      });
      stream.on('close', (fd) => {
        res.status(200).send({ folder, filename, count: results.flat().length });
      });
    } catch (e) {
      const response = JSON.parse(e.message);
      logger.error(response.body);
      res.status(response.statusCode).send({ error: response.body });
    }
  }
};
