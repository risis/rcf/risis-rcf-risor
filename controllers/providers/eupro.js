'use strict';

const url = require('url');
const unirest = require('unirest');
const faker = require('faker');
const fs = require('fs');
const urljoin = require('url-join');
const request = require('request');
const storage = require('../../service_providers/storage');
const { transformBuiltins } = require('../../utils/builtins');
const tmp = require('tmp');
const path = require('path');

const config = global.config;

module.exports = {
  fetch: async (req, res) => {
    const { context: { logger } } = req;
    const body = req.body;
    const { endpoint, query, pagination } = body.api;
    const euproUrl = urljoin(url.format(config.providers.eupro.uri), endpoint, '/');
    const maxResults = body['max-results'];
    const resultsKey = body.results;
    const pageSize = pagination.page_size;
    const limitParam = pagination.limit_param;
    const offsetParam = pagination.offset_param;
    try {
      let continueFetching = true;
      const results = [];
      for (let offset = 0; continueFetching; offset = parseInt(offset) + parseInt(pageSize)) {
        logger.debug('fetch api, url = %o, offset = %o, query params = %o', euproUrl, offset, query);
        const response = await unirest
          .get(euproUrl)
          .query(query)
          .query(`${limitParam}=${pageSize}`)
          .query(`${offsetParam}=${offset}`);
        logger.debug('fetch api response.body = %o', (response ? response.body : null));
        let totalCount = null;
        let responseResults = null;
        try {
          if (maxResults !== '') {
            // eslint-disable-next-line no-eval
            totalCount = isNaN(maxResults) ? eval(`response.body.${maxResults}`) : parseInt(maxResults);
          }
          // eslint-disable-next-line no-eval
          responseResults = response.body.data ? response.body.data : eval(`response.body.${resultsKey}`);
        } catch (err) {
          logger.error(err);
        }
        if (response.status !== 400 & response.body.error & offset === 0) {
          throw new Error('Results not found for the query.');
        } else if (response.body.error || (totalCount && (totalCount === 0 || offset >= totalCount))) {
          continueFetching = false;
        } else if (responseResults) {
          results.push(responseResults);
        } else {
          throw new Error('Empty results. Specify the key where the results are placed as "results" when sending the request.');
        }
      }
      const folder = config.tmpRoot;
      const filename = `${faker.random.alphaNumeric(10)}.json`;
      logger.debug(`creating temporary file  ${folder}/${filename} to save fetch api results`);
      const stream = await fs.createWriteStream(`${folder}/${filename}`);
      stream.on('open', (fd) => {
        stream.write(JSON.stringify(results.flat()));
        stream.end();
      });
      stream.on('close', (fd) => {
        res.status(200).send({ folder, filename, count: results.flat().length });
      });
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  },
  queryBuilder: async (req, res) => {
    const { body: { dest, workspaceUri }, context: { logger } } = req;
    const euproUrl = url.format(config.providers.eupro.uri);
    try {
      const tmpFile = tmp.fileSync({ tmpdir: config.tmpRoot, prefix: 'eupro-query-builder-response' });
      const options = {
        url: urljoin(euproUrl, req.path),
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body.queryBuilder),
        method: 'get'
      };
      logger.debug('proxying request to EUPRO: %o', options);
      const requestPromise = new Promise((resolve, reject) => {
        request(options, async (error, response, body) => {
          if (!error && response.statusCode === 200) {
            fs.writeSync(tmpFile.fd, body);
            resolve(`temporary file saved at ${tmpFile.name}`);
            resolve(response);
          } else {
            reject(body);
          }
        });
      });
      await requestPromise;
      logger.info('uploading eupro-query-builder-response to storage at = %o', dest);
      const storageResponse = await storage.uploadFile(tmpFile.name, dest);
      // keep the tmpFile, it is needed by eupro-query.rsml scenario
      // tmpFile.removeCallback();
      const viewUri = urljoin(storage.workspaceUriToPublicUri(workspaceUri), path.basename(dest));
      res.status(200).json({ viewUri, storageResponse, tmpFile });
    } catch (error) {
      res.status(500).send({ error });
    }
  },
  transform: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await transformBuiltins(res, req.body, logger);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  handleRequest: async (req, res) => {
    const { context: { logger } } = req;
    const euproUrl = url.format(config.providers.eupro.uri);
    const options = {
      url: urljoin(euproUrl, req.path),
      method: req.method.toLowerCase()
    };
    logger.debug('proxying request to EUPRO, url = %o, options = %o', euproUrl, options);
    req.pipe(request(options)).pipe(res);
  }
};
