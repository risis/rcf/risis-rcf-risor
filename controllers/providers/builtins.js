'use strict';

const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const storage = require('../../service_providers/storage');
const fetch = require('node-fetch');
const urljoin = require('url-join');
const tmp = require('tmp-promise');
const Url = require('url-parse');
const faker = require('faker');
const unirest = require('unirest');
const got = require('got');
const { createWriteStream } = require('fs');
const stream = require('stream');
const { promisify } = require('util');
const pipeline = promisify(stream.pipeline);
const nunjucks = require('nunjucks');
const _ = require('lodash');
const { parse } = require('csv-parse');
const { transformBuiltins, transformCsvToCortextFormatBuiltins, joinCsvFilesBuiltins, downloadFilesBuiltins, concatenateCsvColumnsBuiltins, indexCsv, alterCsv } = require('../../utils/builtins');

const config = global.config;

function md5FileSync (filepath) {
  const buffer = fs.readFileSync(filepath);
  const sum = crypto.createHash('md5');
  sum.update(buffer);
  return sum.digest('hex');
}

async function detectResourceFilename (uri) {
  const response = await fetch(uri, { method: 'HEAD', follow: 10 });
  const contentDisposition = response.headers.get('content-disposition');
  let filename = null;
  if (contentDisposition) {
    let attachment;
    [attachment, filename] = contentDisposition.split(';').map(i => i.trim());
    if (attachment === 'attachment') {
      filename = filename.split('=')[1].replace(/^"|"$/g, '');
    }
  } else {
    const url = new Url(uri);
    filename = path.basename(url.pathname);
  }
  if (filename) {
    return filename;
  } else {
    throw new Error(`no filename detected on uri ${uri}`);
  }
}

async function fetchUri (req, res) {
  const { context: { logger } } = req;
  if (!req.body.uri) {
    throw new Error('body.uri param missing');
  }
  const uris = (typeof req.body.uri === 'string') ? [req.body.uri] : req.body.uri;
  if (!req.body.dest) {
    throw new Error('body.dest param missing');
  };
  const dest = req.body.dest;
  try {
    const files = await Promise.all(uris.map(async uri => {
      const fileName = await detectResourceFilename(uri);
      return {
        uri,
        fileName
      };
    }));

    // keep track of tmp folders
    const tmpDirs = [];
    const result = await Promise.all(files.map(async file => {
      // tmp folder creation
      const tmpDir = await tmp.dir({ tmpdir: config.tmpRoot, prefix: 'fetch', unsafeCleanup: true });
      tmpDirs.push(tmpDir);

      // path of file in tmp folder
      const filePath = path.join(tmpDir.path, file.fileName);

      // download file to tmp path
      const downloadStream = got.stream(file.uri);
      const fileWriteStream = createWriteStream(filePath);
      await pipeline(downloadStream, fileWriteStream);

      // upload to storage
      await storage.uploadFiles(tmpDir.path, dest);
      return {
        uri: urljoin(dest, file.fileName),
        filename: file.fileName,
        size: fs.statSync(filePath).size,
        md5sum: md5FileSync(filePath)
      };
    }));
    // delete all tmp dirs
    await Promise.all(tmpDirs.map(tmpDir => tmpDir.cleanup()));

    res.status(200).send({ uri: dest, files: result });
  } catch (err) {
    logger.error(err);
    res.status(400).send({ error: err.message });
  }
}

async function fetchApi (req, res) {
  const { context: { logger } } = req;
  const body = req.body;
  const { url, query, pagination } = body.api;
  const maxResults = body['max-results'];
  const resultsKey = body.results;
  const pageSize = pagination.page_size;
  const limitParam = pagination.limit_param;
  const offsetParam = pagination.offset_param;
  try {
    let continueFetching = true;
    const results = [];
    for (let offset = 0; continueFetching; offset = parseInt(offset) + parseInt(pageSize)) {
      logger.debug('fetch api, url = %o, offset = %o, query params = %o', url, offset, query);
      const response = await unirest
        .get(url)
        .query(query)
        .query(`${limitParam}=${pageSize}`)
        .query(`${offsetParam}=${offset}`);
      logger.debug('fetch api response.body = %o', (response ? response.body : null));
      let totalCount = null;
      let responseResults = null;
      try {
        if (maxResults !== '') {
          // eslint-disable-next-line no-eval
          totalCount = isNaN(maxResults) ? eval(`response.body.${maxResults}`) : parseInt(maxResults);
        }
        // eslint-disable-next-line no-eval
        responseResults = response.body.data ? response.body.data : eval(`response.body.${resultsKey}`);
      } catch (err) {
        logger.error(err);
      }
      if (response.body.error & offset === 0) {
        throw new Error('Results not found for the query.');
      } else if (response.body.error || (totalCount && (totalCount === 0 || offset >= totalCount))) {
        continueFetching = false;
      } else if (responseResults) {
        results.push(responseResults);
      } else {
        throw new Error('Empty results. Specify the key where the results are placed as "results" when sending the request.');
      }
    }
    const folder = config.tmpRoot;
    const filename = `${faker.random.alphaNumeric(10)}.json`;
    logger.debug(`creating temporary file  ${folder}/${filename} to save fetch api results`);
    const stream = await fs.createWriteStream(`${folder}/${filename}`);
    stream.on('open', (fd) => {
      stream.write(JSON.stringify(results.flat()));
      stream.end();
    });
    stream.on('close', (fd) => {
      res.status(200).send({ folder, filename, count: results.flat().length });
    });
  } catch (err) {
    logger.error(err);
    res.status(400).send({ error: err.message });
  }
}

module.exports = {
  // builtins test functions
  sleep: async (req, res) => {
    const milliseconds = req.query.milliseconds || 1000;
    await new Promise(resolve => setTimeout(resolve, milliseconds));
    res.status(200).send({ message: `ZZzzz... slept for ${milliseconds} milliseconds` });
  },
  ping: async (req, res) => {
    res.status(200).send({
      pong: req.body.ping
    });
  },
  job: async (req, res) => {
    const initialTimestamp = req.query.initialTimestamp;
    const now = Date.now();
    res.status(200).send({
      now,
      status: (now - initialTimestamp < 5000 ? 'running' : 'finished'),
      // statusCode = 1 (running), 2 (finished)
      statusCode: (now - initialTimestamp < 5000 ? 1 : 2)
    });
  },
  // builtins production functions
  transfer: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const resultItems = [];
      let files = [];
      if (Array.isArray(req.body.from)) {
        files = req.body.from.map(function (file) {
          return {
            filepath: file.folder + '/' + file.filename,
            dest: req.body.to + '' + file.filename
          };
        });
      } else {
        files = [{ filepath: req.body.from, dest: req.body.to }];
      }
      for (let i = 0; i < files.length; i++) {
        const fileInfo = files[i];
        // TODO detect protocol in {dest} instead of hardcoding storage upload
        // see https://gitlab.com/risis/rcf/risis-rcf-risor/-/issues/38
        const responseUpload = await storage.uploadFiles(fileInfo.filepath, fileInfo.dest);
        resultItems.push({ uri: fileInfo.dest, response: responseUpload });
      }
      const result = resultItems && resultItems.length === 1 ? resultItems[0] : { result: resultItems };
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  },
  fetch: async (req, res) => {
    const { context: { logger } } = req;
    try {
      if (req.body && req.body.uri) {
        fetchUri(req, res);
      } else if (req.body && req.body.api) {
        fetchApi(req, res);
      } else {
        throw new Error('Missing params');
      }
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  },
  post: async (req, res) => {
    const { context: { logger } } = req;
    try {
      if (req.body) {
        const { url, headers, query } = req.body.api;
        await unirest
          .post(url)
          .headers(headers)
          .send(query)
          .then(async (response) => {
            // TODO remove enclosing brackets, find a way to make this work for OrgReg scenario without these brackets
            const results = [response.body];
            const folder = config.tmpRoot;
            const filename = `${faker.random.alphaNumeric(10)}.json`;
            logger.debug(`creating temporary file  ${folder}/${filename} to save fetch api results`);
            const stream = await fs.createWriteStream(`${folder}/${filename}`);
            stream.on('open', (fd) => {
              stream.write(JSON.stringify(results.flat()[0]));
              stream.end();
            });
            stream.on('close', (fd) => {
              res.status(200).send({ folder, filename, count: results.flat()[0].length });
            });
          });
      } else {
        throw new Error('Missing params');
      }
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  },
  transform: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await transformBuiltins(res, req.body, logger);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  transformCsvToCortextFormat: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await transformCsvToCortextFormatBuiltins(res, req.body, logger);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  joinCsv: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await joinCsvFilesBuiltins(req.body, logger);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  indexCsv: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await indexCsv(req.body, logger);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  alterCsv: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await alterCsv(req.body, logger);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  getOneColumnValuesAsString: async (req, res) => {
    const { context: { logger } } = req;
    const { file, fieldname } = req.body;
    try {
      const columnValuesResult = [];
      const dataRows = [];
      fs.createReadStream(file)
        .pipe(parse({ delimiter: '\t' }))
        .on('data', (row) => {
          dataRows.push(row);
        })
        .on('end', () => {
          const indexColumn = dataRows[0].indexOf(fieldname);
          dataRows.shift();

          dataRows.forEach((row) => {
            const columnValue = row[indexColumn];
            columnValuesResult.push(columnValue);
          });
          const columnValuesResultStr = columnValuesResult.join(',');
          res.status(200).send({ result: columnValuesResultStr });
        });
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  },
  downloadFiles: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await downloadFilesBuiltins(req, res);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  concatenateCsvColumns: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await concatenateCsvColumnsBuiltins(req.body, logger);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  },
  reportGenerator: async (req, res) => {
    const { context: { logger } } = req;
    logger.info('generating report');
    try {
      const workspaceUri = req.body.workspaceUri;
      const viewBaseUrl = config.providers.storage.public_uri;
      const reportTemplate = path.join(config.appRoot, req.body.reportTemplate);
      const reportTitle = req.body.reportTitle || 'RISIS RCF Report Generator';
      const datafiles = req.body.datafiles;
      const outputUri = req.body.outputUri;
      const datafilesUri = req.body.datafilesUri;
      const env = nunjucks.configure(reportTemplate);
      env.addFilter('basename', path.basename);
      env.addFilter('lowerCase', _.lowerCase);
      env.addFilter('upperFirst', _.upperFirst);
      const tmpFile = tmp.fileSync({ tmpdir: config.tmpRoot, prefix: 'report-generator-index', postfix: 'html' });
      const datafilesDir = path.basename(datafilesUri);
      fs.write(tmpFile.fd,
        env.render('index.html.njk', { reportTitle, datafiles: datafiles.map(d => path.join(datafilesDir, path.basename(d))) }),
        (err) => { err && logger.error(err); });
      fs.close(tmpFile.fd, (err) => { err && logger.error(err); });
      const response = await Promise.all(
        [storage.uploadFiles(reportTemplate, outputUri),
          storage.uploadFiles(tmpFile.name, urljoin(outputUri, 'index.html'))
        ].concat(datafiles.map(datafile => {
          const datafileUrl = new Url(datafile);
          if (datafileUrl.protocol === undefined || !datafileUrl.protocol) {
            logger.debug('uploading datafile %o to %o, datafile url = %O', datafile, datafilesUri, datafileUrl);
            return storage.uploadFiles(datafile, urljoin(datafilesUri, path.basename(datafile)));
          } else {
            logger.debug('[SKIP] uploading datafile %o to %o, datafile url = %O', datafile, datafilesUri, datafileUrl);
            return new Promise((resolve) => resolve(datafile));
          }
        }))
      );
      tmpFile.removeCallback();
      logger.debug('report saved at %o', outputUri);
      const viewUri = urljoin(storage.workspaceUriToPublicUri(workspaceUri), path.basename(outputUri));
      const info = [{ title: 'Introduction', uri: `${viewUri}/#/introduction` }];
      for (const [i, datafile] of datafiles.entries()) {
        info.push({ title: _.upperFirst(_.lowerCase(path.basename(datafile, '.csv'))), uri: `${viewUri}/#/${i + 1}` });
      }
      info.push({ title: 'Credits', uri: `${viewUri}/#/credits` });
      const infoSerialized = info.reduce((result, item) => (result ? `${result};` : '') + `${item.title}=${item.uri}`, false);
      res.status(200).send({ reportTemplate, datafiles, outputUri, datafilesUri, workspaceUri, viewBaseUrl, viewUri: `${viewUri}/#/introduction`, info: infoSerialized, response });
    } catch (err) {
      logger.error('error generating report!');
      logger.error(err);
      logger.error('stack trace: %o', err.stack);
      res.status(400).send({ error: err.message });
    }
  }
};
