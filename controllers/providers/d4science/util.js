/**
 * @module controller/providers/d4science/util
 * @description This module provides some utilities for working with streams in the d4science provider.
 */

'use strict';

const { promisify } = require('util');
const { pipeline, Transform } = require('stream');

/**
 * A custom Transform stream that trims bytes from the beginning and end of a stream.
 * @extends Transform
 */
class ByteTrimmer extends Transform {
  /**
   * Create a ByteTrimmer instance.
   * @param {number} skipStart - The number of bytes to trim from the beginning of the stream.
   * @param {number} skipEnd - The number of bytes to trim from the end of the stream.
   */
  constructor (skipStart, skipEnd) {
    super();
    this.skipStart = skipStart;
    this.skipEnd = skipEnd;
    /**
     * The total number of bytes processed in the stream.
     * @type {number}
     * @private
     */
    this.seen = 0;

    /**
     * A buffer to store the remaining bytes from the previous chunk.
     * @type {Buffer}
     * @private
     */
    this.lastBuff = Buffer.alloc(0);
  }

  /**
   * Transform function for the ByteTrimmer stream.
   * @param {Buffer|string} chunk - The chunk of data to be transformed.
   * @param {string} encoding - The encoding of the chunk (e.g., 'utf8', 'binary').
   * @param {Function} callback - A callback function to signal the completion of the transformation.
   */
  _transform (chunk, encoding, callback) {
    let len = Buffer.byteLength(chunk);

    if (this.seen <= this.skipStart) {
      const diff = this.skipStart - this.seen;
      if (len > diff) { // chunk has bytes after skipStart
        chunk = chunk.slice(diff, len);
      } else { // discard everything
        chunk = undefined;
      }
    }

    this.seen += len;

    if (!chunk) {
      return callback(); // nothing to push, exit
    }

    // concat the stored bytes before the chunk bytes
    chunk = Buffer.concat([this.lastBuff, chunk]);

    len = Buffer.byteLength(chunk);
    if (len < this.skipEnd) { // not enough bytes stored to know whether to push
      this.lastBuff = chunk;
      return callback();
    }

    this.lastBuff = chunk.slice(len - this.skipEnd, len); // keep the last skipEnd bytes
    this.push(chunk.slice(0, len - this.skipEnd)); // push the ones before
    callback();
  }
}

/**
 * Concatenates multiple readable streams into a single asynchronous generator stream.
 *
 * @param {Array<ReadableStream>} readableStreams - An array of readable streams to concatenate.
 * @param {Object} options - Additional options for customization.
 * @param {any} options.wrapStart - An optional value to yield before concatenating streams.
 * @param {any} options.joinWith - An optional value to yield between concatenated streams.
 * @param {any} options.wrapEnd - An optional value to yield after concatenating streams.
 * @returns {AsyncGenerator} An asynchronous generator that yields the concatenated stream.
 */
async function * concatStreams (readableStreams, { wrapStart, joinWith, wrapEnd }) {
  if (wrapStart) yield wrapStart;
  for (const [index, rs] of readableStreams.entries()) {
    if (index > 0) yield joinWith;
    for await (const chunk of rs) {
      yield chunk;
    }
  }
  if (wrapEnd) yield wrapEnd;
}

module.exports = {
  ByteTrimmer,
  concatStreams,
  streamPipeline: promisify(pipeline)
};
