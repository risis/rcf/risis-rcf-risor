const path = require('path');
const fs = require('fs');

const controller = require('./index');

const {
  removePrefix,
  parseQuery,
  mergeJSONArrayFiles
} = controller.exportedForTesting;

describe('D4science provider', () => {
  const mocksDirectory = path.join(global.config.appRoot, 'test', 'mocks', 'd4science');
  const tmpResultsDirectory = global.config.tmpRoot;
  describe('utility functions', () => {
    describe('remove prefix', () => {
      it('should remove prefix', () => {
        // arrange
        const input = {
          text: 'helloworld',
          prefix: 'hello'
        };
        const expected = 'world';
        // act
        const actual = removePrefix(input.text, input.prefix);
        // assert
        expect(actual).toEqual(expected);
      });
      it('should not change the input if it does not start with the prefix', () => {
        // arrange
        const input = {
          text: 'helloworld',
          prefix: 'goodbye'
        };
        const expected = 'helloworld';
        // act
        const actual = removePrefix(input.text, input.prefix);
        // assert
        expect(actual).toEqual(expected);
      });
    });
    describe('parse query', () => {
      it("should parse a 'like' query", () => {
        // arrange
        const query = {
          children: [
            {
              identifier: 'd4science.items.title',
              value: {
                condition: {
                  id: 'like'
                },
                data: 'sociology'
              }
            }
          ]
        };
        const expected = 'title:sociology';
        // act
        const actual = parseQuery(query);
        // assert
        expect(actual).toEqual(expected);
      });
      it('should not support other queries', () => {
        // arrange
        const query = {
          children: [
            {
              identifier: 'd4science.items.title',
              value: {
                condition: {
                  id: 'contain'
                },
                data: 'sociology'
              }
            }
          ]
        };
        // act
        const actual = () => parseQuery(query);
        // assert
        expect(actual).toThrow();
      });
    });
    describe('merge json arrays', () => {
      let page1Path;
      let page1;
      let page2Path;
      let page2;
      const resultFiles = [];
      beforeAll(() => {
        page1Path = path.join(mocksDirectory, 'page1.json');
        page2Path = path.join(mocksDirectory, 'page2.json');
        page1 = require(page1Path);
        page2 = require(page2Path);
      });
      it('should concatenate zero arrays', async () => {
        // arrange
        const input = {
          filePaths: [],
          folder: tmpResultsDirectory
        };
        const expected = [];
        // act
        const resultPath = await mergeJSONArrayFiles(input.filePaths, input.folder);
        const fullResultPath = path.join(input.folder, resultPath);
        resultFiles.push(fullResultPath); // for cleanup
        const actual = require(fullResultPath);
        // assert
        expect(actual).toEqual(expected);
      });
      it('should concatenate one array', async () => {
        // arrange
        const input = {
          filePaths: [page1Path],
          folder: tmpResultsDirectory
        };
        const expected = page1;
        // act
        const resultPath = await mergeJSONArrayFiles(input.filePaths, input.folder);
        const fullResultPath = path.join(input.folder, resultPath);
        resultFiles.push(fullResultPath); // for cleanup
        const actual = require(fullResultPath);
        // assert
        expect(actual).toEqual(expected);
      });
      it('should concatenate several arrays', async () => {
        // arrange
        const input = {
          filePaths: [page1Path, page2Path],
          folder: tmpResultsDirectory
        };
        const expected = page1.concat(page2);
        // act
        const resultPath = await mergeJSONArrayFiles(input.filePaths, input.folder);
        const fullResultPath = path.join(input.folder, resultPath);
        resultFiles.push(fullResultPath); // for cleanup
        const actual = require(fullResultPath);
        // assert
        expect(actual).toEqual(expected);
      });
      afterAll(() => {
        for (const path of resultFiles) {
          fs.unlinkSync(path);
        }
      });
    });
  });
});
