'use strict';

const faker = require('faker');
const path = require('path');
const { Readable } = require('stream');
const { createWriteStream, createReadStream } = require('fs');
const { getd4scienceService } = require('./service/inject');

// Import stream utility functions
const { ByteTrimmer, concatStreams, streamPipeline } = require('./util.js');

// Global config
const config = global.config;

/**
 * Creates a byte trimmer for removing wrapping brackets from a data stream.
 * @returns {ByteTrimmer} The byte trimmer instance.
 */
function bracketTrimmer () {
  return new ByteTrimmer(
    Buffer.from('[', 'utf8').length,
    Buffer.from(']', 'utf8').length
  );
}

/**
 * Removes a specified prefix from a string. Utility function.
 * @param {string} text - The input text.
 * @param {string} prefix - The prefix to remove.
 * @returns {string} The modified text with the prefix removed.
 */
function removePrefix (text, prefix) {
  if (text.startsWith(prefix)) {
    return text.slice(prefix.length);
  }
  return text;
}

/**
 * Parses a queryfactory payload and transforms it into an array of filter operations in the D4science query search syntax.
 * @param {Object} queryPayload - The query payload to parse.
 * @param {Object[]} queryPayload.children - An array of rules representing filter conditions.
 * @returns {string} An array of filter operations in the format "field:value".
 * @throws {Error} Throws an error if an unsupported operator is encountered.
 */
function parseQuery (queryPayload) {
  const prefix = 'd4science.items.';
  const ops = queryPayload.children.map((rule) => {
    const field = removePrefix(rule.identifier, prefix);
    const op = rule.value.condition.id;
    const value = rule.value.data;
    if (op !== 'like') {
      throw new Error(`unsupported operation ${op}`);
    }
    return {
      field,
      value
    };
  });
  return ops.map(({ field, value }) => `${field}:${value}`).join(' OR ');
}

/**
 * Merges and concatenates JSON arrays stored in multiple files into a single JSON file.
 * @async
 * @param {string[]} filePaths - An array of file paths to the JSON files to merge.
 * @param {string} folder - The folder where the merged JSON file will be saved.
 * @returns {Promise<string>} The filename of the merged JSON file.
 */
async function mergeJSONArrayFiles (filePaths, folder) {
  const fileStream = concatStreams(
    filePaths.map((filePath) => createReadStream(filePath).pipe(bracketTrimmer())),
    { wrapStart: '[', joinWith: ',', wrapEnd: ']' }
  );

  const outFilename = `${faker.random.alphaNumeric(10)}.json`;

  await streamPipeline(
    Readable.from(fileStream),
    createWriteStream(path.join(folder, outFilename))
  );

  return outFilename;
}

/**
 * Factory function for creating a D4science search handler.
 * @param {D4ScienceService} [d4scienceService=getd4scienceService()] - The D4Science service instance to use for searching.
 * @returns {Function} A search handler function that handles D4science search requests.
 */
function d4scienceSearch (d4scienceService = getd4scienceService()) {
  /**
   * Handles a D4science search request, saving the results as a JSON file and returning a local path to the file.
   * @async
   * @param {Request} req - The request object containing the search criteria, represented as a queryfactory payload.
   * @param {Response} res - The response object for sending the folder and filename of the file containing the results.
   */
  return async function search (req, res) {
    const { body, context: { logger } } = req;

    let query;
    try {
      query = parseQuery(body.query);
    } catch (e) {
      res.status(400).send({ error: e.message });
    }
    logger.info(`d4science query ${query}`);

    const folder = config.tmpRoot;
    logger.debug(`creating temporary folder ${folder} to save search results`);

    let filePaths;
    try {
      filePaths = await d4scienceService.fetchAllItems(query, folder, { logger });
    } catch (e) {
      logger.error('Error fetching items');
      if (e.name === 'AbortError') {
        logger.error('Request aborted', e);
      }
      if (e.name === 'FetchError') {
        logger.error('Request operational error', e);
      }
      res.status(500).send({ error: e.message });
    }

    let resultFilename;
    try {
      resultFilename = await mergeJSONArrayFiles(filePaths, folder);
    } catch (e) {
      logger.error('Error merging paginated responses', e);
      res.status(500).send({ error: e.message });
    }

    res.status(200).send({
      folder,
      filename: resultFilename
    });
  };
}

const toExport = { d4scienceSearch };

if (process.env.NODE_ENV === 'test') {
  toExport.exportedForTesting = {
    removePrefix,
    parseQuery,
    mergeJSONArrayFiles
  };
}

module.exports = toExport;
