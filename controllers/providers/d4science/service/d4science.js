'use strict';

const url = require('url');
const path = require('path');
const pLimit = require('p-limit');
const fetch = require('node-fetch');
const faker = require('faker');
const log4js = require('log4js');
const fs = require('fs').promises;
const { Issuer } = require('openid-client');
const { createWriteStream } = require('fs');
const { streamPipeline } = require('../util.js');

// Default logger
const defaultLogger = log4js.getLogger('default');

// Concurrency limiter with 10 concurrent operation max.
const limit = pLimit(10);

// D4science provider config
const d4scienceConfig = global.config.providers.d4science;

/**
 * Creates a base URL object for the items endpoint of D4Science with the given search string set as a query parameter. This object is created using the WHATWG URL standard-compliant node library `url`.
 * @param {string} query - The search query.
 * @returns {URL} The URL for the search query.
 */
function makeSearchURL (query) {
  const baseUrl = new URL(url.format(d4scienceConfig.uri));
  baseUrl.searchParams.append('q', query);
  return baseUrl;
}

/**
 * Creates a URL object for the items endpoint of D4Science with the given search string and `all_fields` as query parameters. `all_fields=true` causes the API to return full items instead of only identifiers. This object is created using the WHATWG URL standard-compliant node library `url`.
 * @param {string} query - The search query.
 * @returns {URL} The URL for the search query.
 */
function makeItemsURL (query) {
  const baseUrl = makeSearchURL(query);
  baseUrl.searchParams.append('all_fields', 'true');
  return baseUrl;
}

/**
 * Calculates and returns an array of offsets based on page size and total count.
 * @param {number} pageSize - The number of items per page.
 * @param {number} totalCount - The total number of items.
 * @returns {number[]} An array of offsets for pagination.
 */
function calculateOffsets (pageSize, totalCount) {
  if (pageSize < 1 || totalCount < 0) throw new Error('pageSize must be positiveand totalCount must be non negative');
  const offsets = [];
  for (let currentPage = 0; currentPage * pageSize < totalCount; currentPage++) {
    offsets.push(currentPage * pageSize);
  }
  return offsets;
}

/**
 * A class for authenticating with the D4Science platform using OpenID Connect (OIDC).
 */
class D4ScienceAuthProvider {
  static UNINITIALIZED = {};
  /**
   * Initializes a new instance of the D4ScienceAuthProvider class.
   * This class is responsible for authenticating with the D4Science platform.
   */
  constructor () {
    // D4sscience authentication
    this._d4scienceAuthClient = D4ScienceAuthProvider.UNINITIALIZED;
    this._d4scienceTokenSet = D4ScienceAuthProvider.UNINITIALIZED;
  }

  /**
 * Initializes the D4Science authentication client and retrieves a token set. This uses the client credentials flow to authenticate the RISOR application on behalf of itself, not of a user.
 * @async
 * @private
 */
  async _initClient () {
    const d4scienceIssuer = await Issuer.discover(d4scienceConfig.oidc.base_url);
    this._d4scienceAuthClient = new d4scienceIssuer.Client({
      client_id: d4scienceConfig.oidc.client_id,
      client_secret: d4scienceConfig.oidc.client_secret
    });
    this._d4scienceTokenSet = await this._getToken();
  }

  /**
   * Retrieves a TokenSet from the D4Science authentication client.
   * @async
   * @private
   * @returns {Promise<TokenSet>} The access token.
   */
  async _getToken () {
    return this._d4scienceAuthClient.grant({
      grant_type: d4scienceConfig.oidc.grant_type,
      audience: d4scienceConfig.oidc.audience
    });
  }

  /**
   * Ensures that the current access token is not expired. Refreshes the token if necessary.
   * @async
   * @returns {Promise<string>} A valid D4science access token.
   */
  async getAccessToken () {
    if (this._d4scienceTokenSet === D4ScienceAuthProvider.UNINITIALIZED) {
      await this._initClient();// lazy initialization
    }
    if (this._d4scienceTokenSet.expired()) {
      this._d4scienceTokenSet = await this._getToken();
    }
    return this._d4scienceTokenSet.access_token;
  }
}

/**
 * A class representing a service for interacting with D4Science APIs, including authentication and data retrieval.
 */
class D4scienceService {
  /**
   * Creates an instance of the D4ScienceService class.
   * Initializes the D4Science authentication client and retrieves a token set using the client credentials flow.
   * This token set is used for making authenticated requests to D4Science APIs.
   * @async
   */
  constructor (authProvider = new D4ScienceAuthProvider()) {
    // D4sscience authentication
    this._authProvider = authProvider;
  }

  /**
   * Fetches all items from D4Science, depending on a given query. The D4Science API is a paginated API. This function stores the items on each page as a JSON file in a specified folder and returns a list with the paths of all files.
   * @async
   * @param {string} query - The search query to retrieve items.
   * @param {string} folder - The folder where JSON files will be stored
   * @param {Object} options - Additional options for the operation.
   * @param {Logger} options.logger - The logger to record information about the operation.
   * @returns {Promise<string[]>} An array of file paths where the fetched JSON files are stored.
   * @throws {Error} If there is an unexpected API response or other errors during the fetch operation.
   */
  async fetchAllItems (query, folder, { logger = defaultLogger } = {}) {
    const accessToken = await this._authProvider.getAccessToken();

    const itemRequestOptions = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    };

    const countUrl = makeSearchURL(query);
    countUrl.searchParams.append('count', 'true');
    const response = await fetch(countUrl, itemRequestOptions);
    if (!response.ok) {
      throw new Error(`unexpected item count API response ${response.statusText}`);
    }
    const { count } = await response.json();
    logger.info(`${count} items to fetch`);

    // Calculatet the offsets for each request. D4Science does pageSize/offset pagination.
    const offsets = calculateOffsets(10, count);
    logger.info(`offsets ${offsets}`);

    // Fetch pages concurrently and store them in files. Limited to 10 concurrent requests.
    const results = await Promise.allSettled(offsets.map((offset) => {
      return limit(async (currentOffset) => {
        const itemPageURL = makeItemsURL(query);
        itemPageURL.searchParams.append('offset', currentOffset);

        const filename = `${faker.random.alphaNumeric(10)}.json`;
        const filePath = path.join(folder, filename);

        const response = await fetch(itemPageURL, itemRequestOptions);

        if (!response.ok) {
          throw new Error(`unexpected item page API response ${response.statusText}`);
        }

        // Stream the response body and write to file.
        await streamPipeline(
          response.body,
          createWriteStream(filePath)
        );

        return filePath;
      }, offset);
    }));

    // Clean up in case of fetch failure
    let error;
    const filePaths = [];
    for (const res of results) {
      if (res.status === 'fulfilled') {
        filePaths.push(res.value);
      } else { // clean up flag
        error = res.reason;
      }
    };
    if (error) {
      await Promise.all(filePaths.map(fs.unlink));
      throw error;
    }

    return filePaths;
  }
};

const toExport = { D4scienceService };

if (process.env.NODE_ENV === 'test') {
  toExport.exportedForTesting = {
    makeSearchURL,
    makeItemsURL,
    calculateOffsets
  };
}

module.exports = toExport;
