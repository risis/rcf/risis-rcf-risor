const path = require('path');
const fs = require('fs').promises;
const nock = require('nock');

const service = require('./d4science');

const { D4scienceService } = service;

const {
  makeSearchURL,
  makeItemsURL,
  calculateOffsets
} = service.exportedForTesting;

const fakeAuthProvider = {
  getAccessToken: jest.fn()
};

describe('D4Science service', () => {
  const mocksDirectory = path.join(global.config.appRoot, 'test', 'mocks', 'd4science');
  const tmpResultsDirectory = global.config.tmpRoot;
  describe('utility functions', () => {
    it('should build the base search url', () => {
      // arrange
      const query = 'title:any_title';
      const expected = 'https://api.d4science.org/gcat/items?q=' + encodeURIComponent(query);
      // act
      const actual = makeSearchURL(query).toString();
      // assert
      expect(actual).toEqual(expected);
    });
    it('should build the base items url', () => {
      // arrange
      const query = 'title:any_title';
      const expected = `https://api.d4science.org/gcat/items?q=${encodeURIComponent(query)}&all_fields=true`;
      // act
      const actual = makeItemsURL(query).toString();
      // assert
      expect(actual).toEqual(expected);
    });
    it('should calculate offsets for zero', () => {
      // arrange
      const input = { pageSize: 5, totalCount: 0 };
      const expected = [];
      // act
      const actual = calculateOffsets(input.pageSize, input.totalCount);
      // assert
      expect(actual).toEqual(expected);
    });
    it('should not support non positive pageSizes', () => {
      // arrange
      const input = { pageSize: 0, totalCount: 1000 };
      // act
      const call = () => calculateOffsets(input.pageSize, input.totalCount);
      // assert
      expect(call).toThrowError();
    });
    it('should not support negative counts', () => {
      // arrange
      const input = { pageSize: 1, totalCount: -1 };
      // act
      const call = () => calculateOffsets(input.pageSize, input.totalCount);
      // assert
      expect(call).toThrowError();
    });
    it('should calculate offsets', () => {
      // arrange
      const input = { pageSize: 5, totalCount: 20 };
      const expected = [0, 5, 10, 15];
      // act
      const actual = calculateOffsets(input.pageSize, input.totalCount);
      // assert
      expect(actual).toEqual(expected);
    });
  });
  describe('D4science client', () => {
    describe('no downloads', () => {
      it('should throw when count fails', async () => {
        // arrange
        const scope = nock('https://api.d4science.org')
          .get('/gcat/items')
          .query(q => q.count === 'true') // match count query
          .reply(400);
        const client = new D4scienceService(fakeAuthProvider);
        // act
        try {
          await client.fetchAllItems('unused_query', 'unused_folder');
        } catch (e) {
          // assert
          expect(scope.isDone()).toBeTruthy();
          expect(e.message).toMatch('unexpected');
        }
      });
      it('should throw when all page fetches fail', async () => {
        // arrange
        const scope = nock('https://api.d4science.org')
          .get('/gcat/items')
          .query(q => q.count === 'true') // match count query
          .reply(200, { count: 20 })
          .get('/gcat/items')
          .query(q => q.all_fields === 'true')
          .twice()
          .reply(400);
        const client = new D4scienceService(fakeAuthProvider);
        // act
        try {
          await client.fetchAllItems('unused_query', 'unused_folder');
        } catch (e) {
          // assert
          expect(scope.isDone()).toBeTruthy();
          expect(e.message).toMatch('unexpected');
        }
      });
    });
    describe('downloads', () => {
      let page1Path;
      let page1;
      let page2Path;
      let page2;
      beforeAll(() => {
        page1Path = path.join(mocksDirectory, 'page1.json');
        page2Path = path.join(mocksDirectory, 'page2.json');
        page1 = require(page1Path);
        page2 = require(page2Path);
      });
      afterAll(async () => {
        for (const file of await fs.readdir(tmpResultsDirectory)) {
          await fs.unlink(path.join(tmpResultsDirectory, file));
        }
      });
      it('should throw when one page fetch fails', async () => {
        const scope = nock('https://api.d4science.org')
          .get('/gcat/items')
          .query(q => q.count === 'true') // match count query
          .reply(200, { count: 20 })
          .get('/gcat/items')
          .query(q => q.all_fields === 'true')
          .reply(400)
          .get('/gcat/items')
          .query(q => q.all_fields === 'true')
          .reply(200, page1);
        const client = new D4scienceService(fakeAuthProvider);
        // act
        try {
          await client.fetchAllItems('unused_query', tmpResultsDirectory);
        } catch (e) {
          // assert
          expect(scope.isDone()).toBeTruthy();
          expect(e.message).toMatch('unexpected');
        }
      });
      it('should fetch pages', async () => {
        const scope = nock('https://api.d4science.org')
          .get('/gcat/items')
          .query(q => q.count === 'true') // match count query
          .reply(200, { count: 20 })
          .get('/gcat/items')
          .query(q => q.all_fields === 'true')
          .reply(200, page1)
          .get('/gcat/items')
          .query(q => q.all_fields === 'true')
          .reply(200, page2);
        const client = new D4scienceService(fakeAuthProvider);
        // act
        const pagePaths = await client.fetchAllItems('unused_query', tmpResultsDirectory);
        // assert
        expect(scope.isDone()).toBeTruthy();
        expect(pagePaths).toHaveLength(2);
        const [fetchedPage1, fetchedPage2] = pagePaths.map(require);
        expect(fetchedPage1).toEqual(page1);
        expect(fetchedPage2).toEqual(page2);
      });
    });
  });
});
