const { D4scienceService } = require('./d4science');

module.exports = {
  getd4scienceService: () => new D4scienceService()
};
