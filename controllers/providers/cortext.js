'use strict';

const cortextService = require('../../service_providers/cortext/cortext');

function handleRequest (req, res) {
  cortextService.handleRequest(req, res, (err) => {
    if (err) {
      res.send(err);
    }
    res.json('request Cortext handled');
  });
}

function FilesUrlResults (req, res) {
  cortextService.FilesUrlResults(req, res);
}

module.exports = {
  handleRequest,
  FilesUrlResults
};
