'use strict';

const datastoreService = require('../../service_providers/datastore');
const fs = require('fs-extra');
const path = require('path');

const config = global.config;

module.exports = {
  datastoreService,
  searchDatasets: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.searchDatasets(
        req,
        {
          dataverseAlias: req.query.dataverseId,
          query: req.query.q || '*',
          type: 'dataset',
          startPosition: req.query.startPosition || 0,
          itemsPerPage: req.query.itemsPerPage || 10
        });
      res.status(result.status).send(result.data.data.items);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode).send({ error: err.message });
    }
  },
  createDataset: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.createDataset(
        req,
        req.query.dataverseId,
        req.body
      );
      res.status(result.status).send(result.data);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode).send({ error: err.message });
    }
  },
  deleteDataset: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.deleteDataset(req, req.query.datasetId);
      res.status(result.status).send(result.data);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode).send({ error: err.message });
    }
  },
  publishDataset: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.publishDataset(req, req.query.datasetId);
      res.status(result.status).send(result.data);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode).send({ error: err.message });
    }
  },
  replaceDataset: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.replaceDataset(req, req.query.datasetId, req.body);
      res.status(result.status).send(result.data);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode).send({ error: err.message });
    }
  },
  uploadFiles: async (req, res) => {
    const { context: { logger } } = req;
    const datasetId = req.query.datasetId || req.body.datasetId;
    const results = [];
    if (req.files || req.body.files) {
      const files = req.files || (Array.isArray(req.body.files) ? req.body.files : req.body.files.split(','));
      for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const pathFile = file.path || file;
        const fileName = file.originalname || pathFile.split('/')[pathFile.split('/').length - 1];
        try {
          let result = await datastoreService.uploadFile(req, datasetId, pathFile, fileName);
          result = await result.json();
          results.push(result);
        } catch (err) {
          logger.error(err);
          res.status(err.errorCode || 500).send({ error: err.message });
        } finally {
          fs.removeSync(pathFile);
        }
      }
      if (results.length === 1) {
        return res.status(200).send(results[0]); // Uploaded just one file
      }
      res.status(200).send({ results });
    } else {
      res.status(400).send({ error: 'file missing' });
    }
  },
  deleteFile: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.deleteFile(req, req.params.fileId);
      res.status(result.status).send({ message: result.statusText });
    } catch (err) {
      logger.error(err);
      res.status(400).send(err);
    }
  },
  replaceFile: async (req, res) => {
    const { context: { logger } } = req;
    if (req.files) {
      const file = req.files[0];
      try {
        const result = await datastoreService.replaceFile(req, req.params.fileId, file.path, file.originalname);
        res.status(result.statusCode).send(result.body);
      } catch (err) {
        logger.error(err);
        res.status(err.errorCode).send({ error: err.message });
      } finally {
        fs.removeSync(file.path);
      }
    } else {
      res.status(400).send({ error: 'file missing' });
    }
  },
  downloadFile: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.downloadFile(req, req.params.fileId);
      res.set(result.headers).status(result.status).send(result.data);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode).send({ error: `file id ${req.params.fileId || undefined} not found` });
    }
  },
  downloadDatasetFiles: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.downloadDatasetFiles(req, req.query.datasetId);
      res.set(result.headers).status(result.status).send(result.data);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode);
      switch (err.errorCode) {
        case 400:
          res.send({ error: `no files found for datasetId ${req.query.datasetId}` });
          break;
        case 404:
          res.send({ error: `datasetId ${req.query.datasetId} not found` });
          break;
        default:
          res.send({ error: `error downloading dataset id ${req.query.datasetId}` });
          break;
      }
    }
  },
  editDataset: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.editDataset(req, req.query.datasetId, req.params.field, req.body);
      res.status(result.status).send({ message: result.statusText });
    } catch (err) {
      logger.error(err);
      res.send(err);
    }
  },
  getDatasetInformation: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.getDatasetInformation(req, req.query.datasetId);
      res.status(result.status).send(result.data);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode).send({ error: err.message });
    }
  },
  listDatasets: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const datasetList = await datastoreService.listDatasets(req, req.query.dataverseId);
      const datasetIds = datasetList.map(ds => ds.id);
      const datasetsResponses = await datastoreService.getDatasetsInformation(req, datasetIds);
      const datasetsResponsesJson = await Promise.all(datasetsResponses.map(response => response.json()));
      const datasetsMeta = datasetsResponsesJson.map(response => response.data);
      res.status(200).send(datasetsMeta);
    } catch (err) {
      logger.error(err.stack);
      res.status(err.errorCode || 500).send({ error: err.message });
    }
  },
  listFiles: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.getDatasetInformation(req, req.query.datasetId);
      res.status(result.status).send(result.data.data.latestVersion.files);
    } catch (err) {
      logger.error(err);
      res.status(err.errorCode).send({ error: err.message });
    }
  },
  downloadFiles: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await datastoreService.getDatasetInformation(req, req.query.datasetId);
      const files = result.data.data.latestVersion.files;
      const ids = files.map(file => file.dataFile.id);
      const downloads = await datastoreService.downloadFiles(req, ids);
      if (downloads.headers) {
        Object.keys(downloads.headers.raw()).forEach(header => {
          logger.debug(`set header ${header} to value ${downloads.headers.raw()[header][0]}`);
          res.set(header, downloads.headers.raw()[header][0]);
        });
        res.status(downloads.status);
        downloads.body.pipe(res);
      } else {
        throw new Error(`files not found for datasetId ${req.query.datasetId}`);
      }
    } catch (err) {
      logger.error(err.stack);
      res.status(err.statusCode ? err.statusCode : 500).send({ error: err.message });
    }
  },
  importFiles: async (req, res) => {
    const { context: { logger } } = req;
    const datasetId = req.query.datasetId;
    const outputFolder = path.join(config.datastoreRoot, datasetId);
    fs.ensureDirSync(outputFolder);
    try {
      const response = await datastoreService.importFiles(req, datasetId, outputFolder);
      res.status(200).send({ ...{ folder: outputFolder }, ...{ data: response.data.data } });
    } catch (err) {
      logger.error(err);
      res.status(500).send(err);
    }
  },
  importFile: async (req, res) => {
    const { context: { logger } } = req;
    const datasetId = req.query.datasetId;
    const fileId = req.params.fileId;
    const outputFolder = path.join(config.datastoreRoot, datasetId);
    fs.ensureDirSync(outputFolder);
    try {
      const response = await datastoreService.importFile(req, datasetId, fileId, outputFolder);
      res.status(200).send({ ...{ folder: outputFolder }, ...{ data: response.data } });
    } catch (err) {
      logger.error(err);
      res.status(500).send(err);
    }
  },
  assignRole: async (req, res) => {
    const { context: { logger } } = req;
    const { email, role, datasetId } = req.body;
    try {
      if (email) {
        const result = await datastoreService.assignRole(req, email, role, datasetId);
        if (result === true) {
          res.status(200).send({});
        } else {
          const json = await result.json();
          res.status(result.status).send(json);
        }
      } else {
        res.status(200).send({});
      }
    } catch (err) {
      logger.error(err);
      res.status(500).send({ error: err.message });
    }
  }
};
