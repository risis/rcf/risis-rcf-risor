'use strict';

const url = require('url');
const unirest = require('unirest');
const faker = require('faker');
const fs = require('fs');
const urljoin = require('url-join');

const config = global.config;

module.exports = {
  // FIXME the function `fetchPatents` is a copy of `builtins.fetchApi`
  //       at some point it is important to refactor to avoid duplication
  fetch: async (req, res) => {
    const { body: { query }, context: { logger } } = req;
    const patentsExplorerUrl = urljoin(url.format(config.providers['patents-explorer'].uri), req.params.action, '/');
    const totalCountKey = '_metadata.total_count';

    try {
      logger.debug('fetch patents-explorer api, url = %o, query params = %o', patentsExplorerUrl, query);
      const response = await unirest
        .get(patentsExplorerUrl)
        .query(query)
        .query('pagination=0');
      logger.debug('fetch api response.body = %o', (response ? response.body : null));

      const totalCount = totalCountKey ? response.body[totalCountKey] : null;
      const results = response.body.records;

      if (response.body.error || (totalCount && totalCount === 0)) {
        throw new Error('Results not found for the query.');
      }
      if (!results) {
        throw new Error('Empty results. Specify the key where the results are placed as "results" when sending the request.');
      }

      const folder = config.tmpRoot;
      const filename = `${faker.random.alphaNumeric(10)}.json`;
      logger.debug(`creating temporary file  ${folder}/${filename} to save fetch api results`);
      const stream = await fs.createWriteStream(`${folder}/${filename}`);
      stream.on('open', (fd) => {
        stream.write(JSON.stringify(results.flat()));
        stream.end();
      });
      stream.on('close', (fd) => {
        res.status(200).send({ folder, filename, count: results.flat().length });
      });
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  },

  search: async (req, res) => {
    const { body, context: { logger } } = req;
    const patentsExplorerUrl = urljoin(url.format(config.providers['patents-explorer'].uri), '/search');
    logger.debug('fetch patents-explorer api, url = %o, query params = %o', patentsExplorerUrl, body);
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    try {
      let results = [];
      let continueFetching = true;
      for (let page = 1; continueFetching; page++) {
        const response = await unirest
          .post(patentsExplorerUrl)
          .query({
            page
          })
          .headers(headers)
          .send(body);
        logger.debug('fetch api response.body = %o', (response ? response.body : null));
        if ((response.status >= 400 && page === 1) || (response.status >= 400 && response.status !== 404)) {
          throw new Error(JSON.stringify(response));
        }
        if (response.body.error && response.body.error.context && response.body.error.context.code === 404) {
          continueFetching = false;
          break;
        }
        results = [...results, ...response.body.records];
      }

      const folder = config.tmpRoot;
      const filename = `${faker.random.alphaNumeric(10)}.json`;
      logger.debug(`creating temporary file  ${folder}/${filename} to save fetch api results`);
      const stream = await fs.createWriteStream(`${folder}/${filename}`);
      stream.on('open', (fd) => {
        stream.write(JSON.stringify(results.flat()));
        stream.end();
      });
      stream.on('close', (fd) => {
        res.status(200).send({ folder, filename, count: results.flat().length });
      });
    } catch (e) {
      const response = JSON.parse(e.message);
      logger.error(response.body);
      res.status(response.statusCode).send({ error: response.body });
    }
  }
};
