'use strict';

const util = require('../../utils');

module.exports = {
  listProviders: (req, res) => {
    res.status(200).send(util.listProviders());
  }
};
