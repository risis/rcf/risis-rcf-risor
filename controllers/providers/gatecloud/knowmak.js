'use strict';

const url = require('url');
const urljoin = require('url-join');
const request = require('request');
const tmp = require('tmp');
const fs = require('fs');
const storage = require('../../../service_providers/storage');
const path = require('path');

const config = global.config;

module.exports = {
  knowmakRequest: async (req, res) => {
    const { body: { dest, workspaceUri }, context: { logger } } = req;
    try {
      const tmpFile = tmp.fileSync({ tmpdir: config.tmpRoot, prefix: 'gatecloud-knowmak-output' });
      const options = {
        url: urljoin(url.format(config.providers['gatecloud/knowmak'].uri), req.path),
        headers: {
          'Content-Type': 'text/plain'
        },
        body: req.body.text,
        method: 'post'
      };
      logger.info('sending request to Gatecloud Knowmak');
      logger.debug('options = %o', options);
      const requestPromise = new Promise((resolve, reject) => {
        request(options, async (error, response, body) => {
          if (!error && response.statusCode === 200) {
            fs.writeSync(tmpFile.fd, body);
            resolve(`temporary file saved at ${tmpFile.name}`);
          } else {
            reject(response.body);
          }
        });
      });
      const requestResponse = await requestPromise;
      logger.debug(requestResponse);
      logger.info('uploading Gatecloud Knowmak output to storage at = %o', dest);
      const storageResponse = await storage.uploadFile(tmpFile.name, dest);
      tmpFile.removeCallback();
      const viewUri = urljoin(storage.workspaceUriToPublicUri(workspaceUri), path.basename(dest));
      res.status(200).json({ viewUri, storageResponse });
    } catch (error) {
      logger.error(error);
      res.status(500).json({ error: error.message });
    }
  }
};
