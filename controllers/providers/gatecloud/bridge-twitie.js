'use strict';

const request = require('request');
const url = require('url');
const urljoin = require('url-join');
const fs = require('fs');
const tmp = require('tmp');
const storage = require('../../../service_providers/storage');
const path = require('path');

const config = global.config;

module.exports = {
  handleRequest: async (req, res) => {
    const { context: { logger } } = req;
    const options = {
      url: urljoin(url.format(config.providers['gatecloud/bridge-twitie'].uri), req.path),
      headers: req.headers,
      method: req.method.toLowerCase()
    };
    logger.info('proxying request to Gatecloud Bridge Twitie');
    logger.debug('options = %o', options);
    req.pipe(request(options)).pipe(res);
  },
  processSubmitBridge: async (req, res) => {
    const { context: { logger } } = req;
    const options = {
      url: urljoin(url.format(config.providers['gatecloud/bridge-twitie'].uri), req.path),
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      formData: {
        id_column: req.body.id_column,
        text_column: req.body.text_column,
        data: fs.createReadStream(req.body.filename)
      },
      method: req.method.toLowerCase()
    };
    logger.info('proxying request to Gatecloud Bridge Twitie, url = %o', options.url);
    request(options, (error, response, body) => {
      logger.debug('body = %o', body);
      if (!error && response.statusCode === 200) {
        const result = JSON.parse(body);
        res.status(200).json(result);
      } else {
        logger.error(error);
        res.status(response.statusCode).send(body);
      }
    });
  },
  processJobOutputBridge: async (req, res) => {
    const { body: { dest, workspaceUri }, context: { logger } } = req;
    try {
      const tmpFile = tmp.fileSync({ tmpdir: config.tmpRoot, prefix: 'gatecloud-bridge-twitie-process-output' });
      const options = {
        url: urljoin(url.format(config.providers['gatecloud/bridge-twitie'].uri), req.path),
        method: 'get'
      };
      logger.info('sending request to Gatecloud Bridge Twitie');
      logger.debug('options = %o', options);
      const requestPromise = new Promise((resolve, reject) => {
        request(options, async (error, response, body) => {
          if (!error && response.statusCode === 200) {
            fs.writeSync(tmpFile.fd, body);
            resolve(`temporary file saved at ${tmpFile.name}`);
          } else {
            reject(response.body);
          }
        });
      });
      const requestResponse = await requestPromise;
      logger.debug(requestResponse);
      logger.info('uploading Gatecloud Bridge Twitie output to storage at = %o', dest);
      const storageResponse = await storage.uploadFile(tmpFile.name, dest);
      tmpFile.removeCallback();
      const viewUri = urljoin(storage.workspaceUriToPublicUri(workspaceUri), path.basename(dest));
      res.status(200).json({ viewUri, storageResponse });
    } catch (error) {
      logger.error(error);
      res.status(500).json({ error: error.message });
    }
  }
};
