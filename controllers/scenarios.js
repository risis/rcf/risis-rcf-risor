'use strict';

const scenarios = require('../scenarios');
const storage = require('../service_providers/storage');
const fs = require('fs');
const path = require('path');
const yaml = require('yaml');
const unirest = require('unirest');
const shortid = require('shortid');
const keycloak = require('../utils/keycloak');
const tmp = require('tmp');

const Url = require('url-parse');
const urljoin = require('url-join');

const config = global.config;

const { RsmlEngine } = require('../scenarios/engine');

const scenarioEngine = new RsmlEngine();

function listScenarioFiles (dir) {
  return fs.readdirSync(dir, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => fs.readdirSync(path.join(dir, dirent.name)).map(f => path.join(dirent.name, f)))
    .flat()
    .filter(file => path.extname(file).toLowerCase() === '.rsml')
    .map(file => path.join(dir, file));
}

function getScenarioId (filename) {
  return path.basename(filename, '.rsml');
}

function rsmlToJSON (filepath, logger) {
  try {
    const file = yaml.parse(fs.readFileSync(filepath, 'utf8'));
    scenarios.expandObjectVariables(file.inputs, null, '');
    return file;
  } catch (err) {
    logger.error(err);
    return { error: `[RSML syntax error] ${err.message}` };
  }
}

async function listScenarios (req) {
  const { context: { userAuthorizations, logger } } = req;
  const scenariosDir = path.join(config.appRoot, config.scenarios.path);
  // Using object.keys because we are not using granular permissions on the scopes
  const userScenarios = userAuthorizations && userAuthorizations.scenarios ? Object.keys(userAuthorizations.scenarios) : [];
  const protectedScenarios =
    config.authorization && config.authorization.scenarios && config.authorization.scenarios.protected
      ? config.authorization.scenarios.protected
      : [];
  return listScenarioFiles(scenariosDir)
    .filter(filename => {
      const id = getScenarioId(filename);
      return !protectedScenarios.includes(id) || userScenarios.includes(id);
    })
    .map(filename => {
      return {
        ...{ id: getScenarioId(filename) },
        ...rsmlToJSON(filename, logger)
      };
    });
}

async function getScenario (scenarioId, logger) {
  // FIXME() Take into account scenario versions
  const scenarioPath = path.join(config.scenarios.path, scenarioId, `${scenarioId}.rsml`);
  return {
    ...{ id: getScenarioId(scenarioPath) },
    ...rsmlToJSON(scenarioPath, logger)
  };
}

// FIXME() Don't hard coded Authorization Bearer here
async function notifyState (state, response, context, callbackUrl, logger) {
  if (callbackUrl) {
    const body = {
      state,
      context
    };
    if (response) {
      body.response = response;
    }
    try {
      const token = await keycloak.getToken();
      logger.debug('access-token = ', token);
      const headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      };
      logger.info('Updating state at', callbackUrl);
      logger.debug('body payload sent on updating state', JSON.stringify(body));
      const updateResponse = await unirest.patch(callbackUrl).headers(headers).send(body);
      if (updateResponse.status === 200) {
        logger.info('Successfully updated state');
      } else {
        logger.error('Error updating state', updateResponse.body);
        logger.info('Response status', updateResponse.status);
      }
    } catch (error) {
      logger.error('Error calling riba to update state');
      logger.error(error);
    }
  }
}

module.exports = {
  listScenarios: (req, res) => {
    const { context: { logger } } = req;
    listScenarios(req)
      .then(result => res.status(200).send(result))
      .catch((err) => {
        logger.error(err);
        res.status(400).send({ error: err.message });
      });
  },
  getScenario: async (req, res) => {
    const { context: { logger } } = req;
    try {
      const result = await getScenario(req.params.scenarioId, logger);
      res.status(200).send(result);
    } catch (err) {
      logger.error(err);
      res.status(400).send({ error: err.message });
    }
  },
  getScenarioBundle: async (req, res) => {
    const { context: { logger } } = req;
    try {
      // scenario path is same as name
      const url = new Url(config.scenarios.uri);
      const bucket = url.hostname;
      const minioPath = urljoin(url.pathname.substring(1), req.params.scenarioId);
      const archive = await storage.getPathAsZip(bucket, minioPath, req.params.scenarioId);
      res.attachment(minioPath + '.zip').type('zip');
      archive.on('end', () => res.end());
      archive.pipe(res);
    } catch (err) {
      logger.error('Get scenario bundle error');
      logger.error(err);
      res.status(400).send({ error: err.message, stack: err.stack });
    }
  },
  runScenario: async (req, res) => {
    const { context: { logger } } = req;
    // the scenario id MUST be always equal to the scenario filename
    const callbackUrl = req.body.context ? req.body.context.callback_url : null;
    logger.info("scenario '%s' callback url = '%s'", req.params.scenarioId, callbackUrl);
    // FIXME() Update scenario version with the right number
    res.setHeader('Content-Type', 'application/vnd.rsml.v1.0.0+json');
    const instanceId = shortid.generate();
    const notificationContext = { instanceId };
    let runningUpdate;
    try {
      res.status(200).send({ scenarioId: req.params.scenarioId, callbackUrl: `${callbackUrl}`, state: 'running' });
      runningUpdate = notifyState('running', null, notificationContext, callbackUrl, logger);
      req.body.params.datastore_public_uri = config.providers.datastore.public_uri;
      const response = await scenarioEngine.runScenario(req.params.scenarioId, instanceId, req.body.context, req.body.params);
      const ctx = response.context || response;
      if (response && response.result) {
        const result = response.result;
        if (req.body.params && req.body.params.inputs && req.body.params.inputs['workspace-uri']) {
          const dest = urljoin(req.body.params.inputs['workspace-uri'], 'outputs', 'result.json');
          logger.info('saving scenario result on storage at ', dest);
          const tmpFile = tmp.fileSync({ tmpdir: config.tmpRoot, prefix: 'scenario-result' });
          fs.writeSync(tmpFile.fd, JSON.stringify(result));
          await storage.uploadFile(tmpFile.name, dest);
          tmpFile.removeCallback();
        }
      }
      await runningUpdate;
      // note that we avoid to return the whole response object as payload
      // to notify the finish state. The response is either the result of
      // the rsml execution or empty. There are two main reasons for this:
      // reponse.params can contain secret tokens that were passed as input
      // reponse.context can being to large and raises a 'Payload Too Large'
      // error
      if (ctx && ctx.stopped) {
        notifyState('stopped', { result: 'scenario stopped' }, notificationContext, callbackUrl, logger);
      } else if (response && response.result) {
        notifyState('finished', { result: response.result }, notificationContext, callbackUrl, logger);
      } else {
        notifyState('finished', {}, notificationContext, callbackUrl, logger);
      }
    } catch (err) {
      await runningUpdate;
      if (typeof err.toJSON === 'function') {
        logger.error(err.toJSON());
        notifyState('finished', { error: err.toJSON() }, notificationContext, callbackUrl, logger);
      } else if (err.message !== undefined) {
        logger.error(err);
        notifyState('finished', { error: { message: err.message } }, notificationContext, callbackUrl, logger);
      } else {
        logger.error(err);
        notifyState('finished', { error: { message: 'Unknown error' } }, notificationContext, callbackUrl, logger);
      }
    }
  },
  processInstanceAction: async (req, res) => {
    const instanceId = req.params.instanceId;
    const command = req.body.command;
    try {
      const commandResponse = await scenarioEngine.processCommand(instanceId, command);
      res.status(200).send({ engineResponse: commandResponse });
    } catch (e) {
      res.status(400).send(e.message);
    }
  }
};
